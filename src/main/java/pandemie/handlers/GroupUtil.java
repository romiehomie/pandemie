package pandemie.handlers;

import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.transform.Scale;
import javafx.scene.transform.Translate;

/**
 *  Skalierungs- und Translatierungswerkzeuge für den Umgang mit Gruppen (Group)
 *
 */
public class GroupUtil {
    public static void scale(Node surroundingNode, Group group, double factor, double pivotX, double pivotY) {
        // calculate transformation between window coordinates and group coordinates
        //double transfromScaleX = surroundingNode.getLayoutBounds().getWidth()/group.getLayoutBounds().getWidth();
        //double transfromScaleY = surroundingNode.getLayoutBounds().getHeight()/group.getLayoutBounds().getHeight();

        //Creating new scale transformation
        Scale rescaling = new Scale();

        //Setting the dimensions for the transformation
        rescaling.setX(group.getScaleX()*factor);
        rescaling.setY(group.getScaleY()*factor);

        //Setting the pivot point for the transformation
        rescaling.setPivotX(pivotX);
        rescaling.setPivotY(pivotY);

        //Transform every node
        for (Node node : group.getChildren()) {
            node.getTransforms().add(rescaling);
        }
    }

    public static void scaleInverse(Node scene, Group group, double factor, double pivotX, double pivotY) {
        if(factor>0){
            scale(scene, group, 1/factor, pivotX, pivotY);
        }
    }

    public static void translate(Group group, double deltaX, double deltaY){
        // create the translation
        Translate translation = new Translate(deltaX, deltaY);

        //Transform every node
        for (Node node : group.getChildren()) {
            node.getTransforms().add(translation);
        }
    }
}
