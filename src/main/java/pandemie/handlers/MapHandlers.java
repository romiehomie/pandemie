package pandemie.handlers;

import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.image.ImageView;

import static java.lang.Math.abs;
import static java.lang.Math.exp;

public class MapHandlers {
    private static double lastWindowHeight, lastWindowWidth, newWindowHeight, newWindowWidth;
    private static double x0, y0, x1, y1;
    private static double scrollConst = 0.0005;
    private static double onePlusEpsilon = 1.01;


    public static void ladeMapHandler(AnchorPane anchorPane,
                                      Group gameMap,
                                      ImageView centerMap){
        // ===================
        // == Resize Events ==
        // ===================

        // event handler for window resize height
        lastWindowHeight = anchorPane.getHeight();
        anchorPane.heightProperty().addListener(heightResizeEvent ->{
            newWindowHeight = anchorPane.getHeight();
            if( lastWindowHeight!=0 && newWindowWidth!=0){
                System.out.println("New window height is " + newWindowHeight + ".");
                System.out.println("Last window height was " + lastWindowHeight + ".");
                System.out.println("Zoom by "+ newWindowHeight/lastWindowHeight);
            }
            lastWindowHeight = newWindowHeight;
        });

        // event handler for window resize width
        lastWindowWidth = anchorPane.getHeight();
        anchorPane.heightProperty().addListener(heightResizeEvent ->{
            newWindowWidth = anchorPane.getHeight();
            if (lastWindowWidth!=0. && newWindowWidth!=0.){
                System.out.println("New window height is " + newWindowWidth + ".");
                System.out.println("Last window height was " + lastWindowWidth + ".");
                System.out.println("Zoom by "+ newWindowWidth/lastWindowWidth);
            }
            lastWindowHeight = newWindowHeight;
        });


        // ===================
        // === Drag Events ===
        // ===================

        // event handler for drag initialized
        anchorPane.setOnMousePressed(mouseEvent -> {
            // prepare cursor position for first drag event
            x0 = mouseEvent.getX();
            y0 = mouseEvent.getY();

            // make no other event handler responding to this event
            mouseEvent.consume();
        });

        // event handler for further dragging
        anchorPane.setOnMouseDragged(mouseEvent -> {
            // get cursor position at this time
            x1 = mouseEvent.getX();
            y1 = mouseEvent.getY();

            // skipp map dragging if north or south border would be breached
            if(!isToFarNorth(gameMap, y1-y0) && !isToFarSouth(anchorPane, gameMap, y1-y0)){
                // translate gameMap by tiny mouse position difference
                GroupUtil.translate(gameMap, x1-x0, y1-y0);
            } else if(isToFarNorth(gameMap, y1-y0)) {
                gameMap.setLayoutY(0.);
                GroupUtil.translate(gameMap, x1-x0, 0.);
            } else if(isToFarSouth(anchorPane, gameMap, y1-y0)){
                // todo: why does this works like this??
                // took  0.  instead of  gameMap.getLayoutBounds().getHeight()-anchorPane.getHeight()
                gameMap.setLayoutY(0.);
                GroupUtil.translate(gameMap, x1-x0, 0.);
            }

            if (isToFarEast(anchorPane, gameMap)){
                //GroupUtil.translate(gameMap, centerMap.getLayoutBounds().getWidth(),0);
                //x1 += centerMap.getLayoutBounds().getWidth();
                //System.out.println("to far east");
            }

            if (isToFarWest(anchorPane, gameMap)){
                //GroupUtil.translate(gameMap, -centerMap.getLayoutBounds().getWidth(),0);
                //x1 -= centerMap.getLayoutBounds().getWidth();
                //System.out.println("to far west");
            }

            // save last cursor position for next drag event
            x0 = x1;
            y0 = y1;

            // make no other event handler respond to this event
            mouseEvent.consume();
        });

        // ===================
        // == Scroll Events ==
        // ===================


        // todo: find out why the event handler for scrolling is not working when used on nodes different from imageViews
        //westMap.setOnScroll(scrollEvent -> {handleScroll(scrollEvent, anchorPane, gameMap, centerMap);});
        centerMap.setOnScroll(scrollEvent -> {handleScroll(scrollEvent, anchorPane, gameMap, centerMap);});
        //eastMap.setOnScroll(scrollEvent -> {handleScroll(scrollEvent, anchorPane, gameMap, centerMap);});


    }

    /**
     *
     *
     * @param scrollEvent
     * @param anchorPane
     * @param gameMap
     */
    private static void handleScroll(ScrollEvent scrollEvent,
                                     AnchorPane anchorPane,
                                     Group gameMap,
                                     ImageView centerMap) {
        // calculate zoom from scroll intensity
        double zoom = exp(scrollConst*scrollEvent.getDeltaY());

        // get mouse position
        double mouseX = scrollEvent.getX();
        double mouseY = scrollEvent.getY();

        // make no other event handler respond to this event
        scrollEvent.consume();

        // apply scaling by factor zoom with pivot point (fix point) at mouse cursor position
        GroupUtil.scale(anchorPane,
                gameMap,
                zoom,
                mouseX,
                mouseY);

        int loopBreakCounter = 0;
        while(isMapSmallerThanSurroundingNode(anchorPane, gameMap)
                || isToFarNorth(gameMap, 0.)
                || isToFarSouth(anchorPane, gameMap, 0. )) {
            // check if map is to small for scene, then translate scene onto map if boarders where breached
            if (isMapSmallerThanSurroundingNode(anchorPane, gameMap)) {
                double zoomToFitSize = anchorPane.getLayoutBounds().getHeight() / gameMap.getLayoutBounds().getHeight();
                GroupUtil.scale(anchorPane,
                        gameMap,
                        zoomToFitSize * onePlusEpsilon,
                        mouseX,
                        mouseY);
            }

            if (isToFarNorth(centerMap, 0.)) {
                System.out.println("to far north");
                GroupUtil.translate(gameMap,
                        0.,
                        -gameMap.getLayoutBounds().getMinY());
            }

            if (isToFarSouth(anchorPane, centerMap, 0.)) {
                GroupUtil.translate(gameMap,
                        0.,
                        anchorPane.getLayoutBounds().getMaxY() - gameMap.getLayoutBounds().getMaxY());
            }

            if (loopBreakCounter>50){
                System.err.println("Problem beim reskalieren der Karte nachdem durch ein Scrolling der Rand der Karte durchbrochen wurde.");
                break;
            }

            loopBreakCounter++;
        }
    }

    /**
     * Diese Funktion gibt genau dann true zurück, wenn der sichtbare Ausschnitt nach
     * Verschiebung um deltaY über den nördlichen Rand des Spielbrettes hinausragt.
     * 
     * @param gameMap - das Spielbrett
     * @param deltaY - Verschiebung
     * @return
     */
    private static boolean isToFarNorth(Node gameMap, double deltaY){
        return gameMap.getLayoutBounds().getMinY() + deltaY > 0;
    }

    /**
     * Diese Funktion gibt genau dann true zurück, wenn der sichtbare Ausschnitt nach
     * Verschiebung um deltaY über den südlichen Rand des Spielbrettes hinausragt.
     *
     * @param surroundingNode - der Fensterausschnitt in dem sich die Karte befindet
     * @param gameMap - das Spielbrett
     * @param deltaY - Verschiebung
     *
     * @return ob die Karte über den Südlichen Rand des Spielbrettes hinausragt
     */
    private static boolean isToFarSouth(Node surroundingNode, Node gameMap, double deltaY){
        return gameMap.getLayoutBounds().getMaxY() - surroundingNode.getLayoutBounds().getMaxY() + deltaY < 0;
    }

    /**
     * Diese Funktion gibt genau dann true zurück, wenn die Karte zu klein ist um den sichtbaren Ausschnitt
     * ganz auszufüllen.
     *
     * @param surroundingNode - der Fensterausschnitt in dem sich die Karte befindet
     * @param gameMap - das Spielbrett
     * @return Ob die Karte zu klein ist um den sichtbaren Ausschnitt ganz auszufüllen
     */
    private static boolean isMapSmallerThanSurroundingNode(Node surroundingNode, Group gameMap){
        return gameMap.getLayoutBounds().getHeight() <= surroundingNode.getLayoutBounds().getHeight();
    }

    /**
     * Diese Funktion gibt genau dann true zurück, wenn die Karte den maximalen Zoom überschritten hat.
     *
     * @return
     */
    private static boolean isTooMuchZoom(){
        //todo: choose some maximum zoom
        return false;
    }

    /**
     *
     * @param map
     * @return
     */
    private static boolean isToFarEast(Node surroundingNode, Node map){
        //todo: korrigieren
        return false; // map.getLayoutBounds().getMaxX() < surroundingNode.getLayoutBounds().getMinX();
    }


    /**
     * Die Weltkarte besteht intern aus drei aneinander geklebten Karten. Eine zentrale Karte, eine westliche Karte
     * und eine östliche Karte. Die Funktion gibt genau dann true zurück, wenn der sichtbare Bereich die zentrale
     * Karte nach Westen hin verlassen hat.
     *
     * @param surroundingNode - der Fensterausschnitt in dem sich die Karte befindet
     * @param map - das Spielbrett
     * @return
     */
    private static boolean isToFarWest(Node surroundingNode, Node map){
        return map.getLayoutBounds().getMinX() > 0.;
    }



}