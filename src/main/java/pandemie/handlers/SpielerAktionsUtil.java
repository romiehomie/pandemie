package pandemie.handlers;

import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import pandemie.game.rollen.Rolle;

public class SpielerAktionsUtil {
    AnchorPane aktionsKarteStandard;
    AnchorPane getAktionsKarteLogistikexperte;
    AnchorPane centerPane;
    AnchorPane fragePane;

    public SpielerAktionsUtil(AnchorPane centerPane, AnchorPane aktionsKarteStandard, AnchorPane aktionskarteLogistikexperte, AnchorPane fragePane){
        this.centerPane = centerPane;
        this.aktionsKarteStandard = aktionsKarteStandard;
        this.getAktionsKarteLogistikexperte = aktionskarteLogistikexperte;
    }

    public void aktionsKarteZeigen(Rolle rolle){
        centerPane.toBack();
        fragePane.toBack();
        if(rolle.equals(Rolle.LOGISTIKER)){
            getAktionsKarteLogistikexperte.toFront();

        } else{
            aktionsKarteStandard.toFront();
        }
    }

    public void spielBrettZeigen(){
        centerPane.toFront();
    }

    public void frageBoxZeigen(){ fragePane.toFront(); }

}


