package pandemie.draws;

import javafx.scene.canvas.Canvas;
import javafx.scene.effect.DropShadow;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import pandemie.game.ErregerTyp;
import pandemie.game.spielablauf.Spieler;
import pandemie.game.spielkarten.Stadt;

import java.util.ArrayList;

// zeichnet eine Stadt und alle zuhörigen Objekte darum
public class DrawableStadt {
    private double stadtRadius = 25;
    private double erregerSymbol = 15;
    private double erregerSymbolLinienBreite = 5;
    private double erregerInnenRadius = 2*stadtRadius/5;
    private double erregerAussenRadius = stadtRadius-stadtRadius/5;
    private double stadtCanvasWidth = 200.;
    private double stadtCanvasHight = 200.;
    private double rcHeight = stadtRadius*4/5;
    private double rcWidth = stadtRadius;
    private double rcxPos = stadtCanvasWidth/2-stadtCanvasWidth*1/7;
    private double rcyPos = stadtCanvasHight/2-rcHeight-stadtRadius*2.5;
    private double distSatRadius = stadtRadius*7/5;
    private double satRadius = stadtRadius/5;
    private double meepleRadius=stadtRadius*2/5.;
    private double meepleWidth =stadtRadius*4/5.;
    private double meepleHight =stadtRadius;
    private double firstMeepleDX = stadtRadius/2.;
    private double meepleVerschub = stadtRadius/2.7;
    private double meepleDY = stadtRadius*2.7;

    private Paint labelColor = Color.BLACK;
    private Paint labelShadowColor = Color.WHITE;

    private Stadt stadt;
    private Canvas canvas;

    public Canvas getCanvas() {
        return canvas;
    }

    public DrawableStadt(Stadt stadt) {
        this.stadt = stadt;
        this.createStadtCanvas();
    }

    public void createStadtCanvas(){
        canvas = new Canvas(stadtCanvasWidth, stadtCanvasHight);
        canvas.setLayoutX(stadt.getSymbolX() - stadtCanvasWidth/2.);
        canvas.setLayoutY(stadt.getSymbolY() - stadtCanvasHight/2.);
    }

    public void drawStadt(ArrayList<Spieler> spielerListe){
        // legt weißen Schlagschatten auf das Canvas
        DropShadow dropShadow = new DropShadow();
        dropShadow.setRadius(3.0);
        dropShadow.setOffsetX(0.);
        dropShadow.setOffsetY(0.);
        dropShadow.setSpread(.9);
        dropShadow.setColor(Color.color(1., 1., 1.));
        canvas.setEffect(dropShadow);

        // zeichnet der Stadtzentren
        Color color = stadt.getErreger().getFarbe();
        canvas.getGraphicsContext2D().setFill(color);
        CanvasUtil.fillCircle(canvas, stadtCanvasWidth/2, stadtCanvasHight/2, stadtRadius);
        // zeichnen der Erregersymbole in den Stadtzentren
        drawErregerSymbole();
        // zeichnen der Erregerskalen um die Stadtzentren
        drawErregerSkalen();
        // zeichnen der Forschungslabore
        drawLabel();
        // zeichnen der Forschungslabore
        drawLabor();
        // zeichnen der Stadtnamen in einem Label
        drawLabel();
        // zeichnen der Spielfiguren
        drawSpieler(spielerListe);
    }

    private void drawErregerSymbole() {
        Color color = stadt.getErreger().getFarbe();

        // zeichnet Erreger Symbol
        canvas.getGraphicsContext2D().setStroke(Color.GREY);
        canvas.getGraphicsContext2D().setLineWidth(erregerSymbolLinienBreite);
        if (color == ErregerTyp.BLAU.getFarbe()) {
            CanvasUtil.stroke5Star(canvas, stadtCanvasWidth/2., stadtCanvasHight/2., erregerInnenRadius, erregerAussenRadius );
        } else if (color == ErregerTyp.SCHWARZ.getFarbe()) {
            CanvasUtil.stroke5gon(canvas, stadtCanvasWidth/2, stadtCanvasHight/2, erregerAussenRadius);
        } else if (color == ErregerTyp.ROT.getFarbe()) {
            CanvasUtil.strokeRect(canvas, stadtCanvasWidth/2, stadtCanvasHight/2, erregerAussenRadius);
        } else {
            CanvasUtil.strokeCircle(canvas, stadtCanvasWidth/2, stadtCanvasHight/2, erregerAussenRadius);
        }
    }

    public void drawLabel(){
        double mittlereBuchstabenBreite = 3;
        String label = stadt.getName();
        // Schattierung des Labeltexteszeichnen
        canvas.getGraphicsContext2D().setLineWidth(2);
        canvas.getGraphicsContext2D().setStroke(labelShadowColor);
        canvas.getGraphicsContext2D().strokeText(label, stadtCanvasWidth/2-mittlereBuchstabenBreite*label.length(), stadtCanvasHight/2 + stadtRadius*2);
        // Labeltext zeichnen
        canvas.getGraphicsContext2D().setLineWidth(1);
        canvas.getGraphicsContext2D().setStroke(labelColor);
        canvas.getGraphicsContext2D().strokeText(label, stadtCanvasWidth/2-mittlereBuchstabenBreite*label.length(), stadtCanvasHight/2 + stadtRadius*2);
    }

    public void drawErregerSkalen(){
        int erregerStadtfarbeSkala = stadt.getSkala(stadt.getErreger());
        canvas.getGraphicsContext2D().setFill(stadt.getErreger().getFarbe());
        drawErregerSkala(stadt.getErreger(), 0);
        int i=3;
        for(ErregerTyp erreger: ErregerTyp.values()){
            if(!erreger.equals(stadt.getErreger())){
                canvas.getGraphicsContext2D().setFill(erreger.getFarbe());
                drawErregerSkala(erreger, i);
                i+=3;
            }
        }
    }

    private void drawErregerSkala(ErregerTyp erreger, int centerSlot){
        int skala = stadt.getSkala(erreger);
        if(skala==1){
            CanvasUtil.satelliteCircle(canvas,
                    stadtCanvasWidth/2,
                    stadtCanvasHight/2,
                    distSatRadius,
                    satRadius,
                    0);
        } else {
            for(int i=1; i<=skala; i++){
                CanvasUtil.satelliteCircle(canvas,
                        stadtCanvasWidth/2,
                        stadtCanvasHight/2,
                        distSatRadius,
                        satRadius,
                        centerSlot+i-2);
            }

        }
    }

    public void drawLabor(){
        canvas.getGraphicsContext2D().setFill(Color.YELLOWGREEN);
        if (stadt.isHatForschungsLabor()) {
            CanvasUtil.fillResearchCenter(canvas, rcxPos, rcyPos, rcWidth, rcHeight);
        }
    }

    public void drawSpieler(ArrayList<Spieler> spielerListe){
        int i=0;
        for(Spieler spieler: spielerListe){
            canvas.getGraphicsContext2D().setFill(spieler.getRolle().getFarbe());

            if(spieler.getStadt().equals(stadt)){
                CanvasUtil.fillMeeple(canvas, stadtCanvasHight/2-firstMeepleDX+i*meepleVerschub,stadtCanvasWidth/2-meepleDY, meepleRadius, meepleWidth, meepleHight);
            }
            i++;
        }
    }
}
