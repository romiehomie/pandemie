package pandemie.draws;

import javafx.scene.canvas.Canvas;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;
import pandemie.game.spielkarten.Stadt;

import java.util.ArrayList;

// ermittelt alle hinterlegten Nachbarstädte einer Stadt und zeichnet die Verbindungen auf den Spielplan
public class DrawableVerbindungen {
    Canvas mapCanvas;

    public DrawableVerbindungen(ImageView centerMap){
        mapCanvas = new Canvas(
                centerMap.getLayoutBounds().getWidth(),
                centerMap.getLayoutBounds().getHeight());
    }

    public void draw() {
        for(Stadt stadt: Stadt.values()){
            ArrayList<Stadt> nachbarStaedte = stadt.getNachbarstaedte();
            for(Stadt nachbarStadt: nachbarStaedte){
                this.drawVerbindung(stadt, nachbarStadt);
            }
        }
    }

    public void drawVerbindung(Stadt stadt1, Stadt stadt2){
        mapCanvas.getGraphicsContext2D().setLineWidth(5);
        mapCanvas.getGraphicsContext2D().setStroke(Color.WHITE);

        // wenn die Stadt am Rand ist, zeichne auf die jeweils nächste Karte (östlich bzw. westlich der mittleren Karte)
        if(stadt1.distanceTo(stadt2)>mapCanvas.getWidth()/2.){
            if(stadt1.getSymbolX() < mapCanvas.getLayoutBounds().getCenterX()){
                CanvasUtil.strokeLine(mapCanvas,
                        stadt1.getSymbolX(),
                        stadt1.getSymbolY(),
                        stadt2.getSymbolX()-mapCanvas.getWidth(),
                        stadt2.getSymbolY());
            } else{
                CanvasUtil.strokeLine(mapCanvas,
                        stadt1.getSymbolX(),
                        stadt1.getSymbolY(),
                        stadt2.getSymbolX()+mapCanvas.getWidth(),
                        stadt2.getSymbolY());
            }
        } else {
            CanvasUtil.strokeLine(mapCanvas,
                    stadt1.getSymbolX(),
                    stadt1.getSymbolY(),
                    stadt2.getSymbolX(),
                    stadt2.getSymbolY());
        }

    }

    public Canvas getCanvas() {
        return mapCanvas;
    }
}
