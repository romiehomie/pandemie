package pandemie.draws;

import javafx.scene.canvas.Canvas;
import javafx.scene.effect.Bloom;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;
import pandemie.game.spielkarten.Stadt;

import static java.lang.Math.cos;
import static java.lang.Math.sin;

// ermittelt die hinterlegten Koordinatenpaare von gezeichneter und Kartenposition der Städte
// und zeichnet die Verbindung mittels eines Zeigers von der gezeichneten Position zur Kartenposition
public class DrawableStadtZeiger {
    private static double stadtPunktRadius = 4;
    private static double stadtSymbolRadius = 25;
    private static double lineWidth = 1;

    private Canvas mapCanvas;

    public DrawableStadtZeiger(ImageView centerMap){
        mapCanvas = new Canvas(
                centerMap.getLayoutBounds().getWidth(),
                centerMap.getLayoutBounds().getHeight());
        mapCanvas.setEffect(new Bloom());
        mapCanvas.getGraphicsContext2D().setGlobalAlpha(0.2);
    }

    public Canvas getCanvas() {
        return mapCanvas;
    }

    public void zeichne(){
        for(Stadt stadt: Stadt.values()){
            zeichneZeiger(stadt);
        }
    }

    private void zeichneZeiger(Stadt stadt){
        Color farbe = stadt.getErreger().getFarbe();
        mapCanvas.getGraphicsContext2D().setFill(farbe.darker().desaturate());
        mapCanvas.getGraphicsContext2D().setStroke(farbe.darker().desaturate());
        mapCanvas.getGraphicsContext2D().setLineWidth(lineWidth);
        zeichneLinien(
                stadt.getPosX(),
                stadt.getPosY(),
                stadt.getSymbolX(),
                stadt.getSymbolY(),
                stadt.getErreger().getFarbe());
        zeichnePunkt(
                stadt.getPosX(),
                stadt.getPosY());
    }

    private void zeichneLinien(double posX, double posY, double symbolX, double symbolY, Color farbe) {
        int iMax = 64;
        double lineStartX = posX;
        double lineStartY = posY;
        for(int i=1; i<=iMax; i++){
            double lineEndX = symbolX + stadtSymbolRadius*sin(2*Math.PI/iMax * i);
            double lineEndY = symbolY + stadtSymbolRadius*cos(2*Math.PI/iMax * i);
            CanvasUtil.strokeLine(mapCanvas, lineStartX, lineStartY, lineEndX, lineEndY);
        }
    }

    private void zeichnePunkt(double posX, double posY) {
        CanvasUtil.fillCircle(mapCanvas, posX, posY, stadtPunktRadius);
    }
}
