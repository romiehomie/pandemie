package pandemie.draws;

import javafx.scene.Group;
import javafx.scene.canvas.Canvas;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import pandemie.game.KartenStapel;
import pandemie.game.spielablauf.Spieler;
import pandemie.handlers.MapHandlers;

import java.util.ArrayList;

public class Spielbrett {
    private static double mapInitialX = 0.;
    private static double mapInitialY = 0.;
    private static int spieleranzahl = 4;
    private static int schwierigkeitsgrad = 1;
    private static ArrayList<KartenStapel> spielkarten;
    private static ArrayList<KartenStapel> infektionskarten;
    private static ArrayList<Spieler> spielerListe;

    /** Funktion die das Spielbrett mit Hintergrundbild und zusätzlichen Grafiken befüllt.
     *
     * @param anchorPane - Pane auf das gezeichnet wird.
     */
    public static void starte(AnchorPane anchorPane) {
        // Creating an image
        Image image = new Image(Spielbrett.class.getResourceAsStream("pics/blueMarbleDayDownscaled.jpg"));
        ImageView centerMap = new ImageView(image);

        //ImageView eastMap = new ImageView(image);
        //eastMap.setX(+centerMap.getLayoutBounds().getWidth());
        //ImageView westMap = new ImageView(image);
        //westMap.setX(-centerMap.getLayoutBounds().getWidth());

        // Create Cities and Stuff
        //pandemie.draws.Stadt.draw_AllCityPositionsAndSymbolConnection();
        //pandemie.draws.Stadt.draw_AllVerbindungen();
        //pandemie.draws.Stadt.draw_AllCitySymbols();

        // drop map on group-pane, then drop shapes on group-pane
        Group gameMap = new Group(centerMap);

        // Zeichen Stadt-Punkt-Markierungen und Verbindungslinien zum Stadt Symbol
        DrawableStadtZeiger stadtZeiger = new DrawableStadtZeiger(centerMap);
        stadtZeiger.zeichne();
        Canvas stadtZeigerCanvas = stadtZeiger.getCanvas();
        gameMap.getChildren().add(stadtZeigerCanvas);

        // Zeichne Verbindungen der Nachbarstädte
        DrawableVerbindungen verbindungen = new DrawableVerbindungen(centerMap);
        Canvas verbindungsLinienCanvas = verbindungen.getCanvas();
        verbindungsLinienCanvas.getGraphicsContext2D().setLineWidth(5);
        verbindungsLinienCanvas.getGraphicsContext2D().setStroke(Color.WHITE);
        verbindungen.draw();
        gameMap.getChildren().add(verbindungsLinienCanvas);

        // zeichne Städte
        DrawableStaedte drawableStaedte = new DrawableStaedte();
        drawableStaedte.draw(spielerListe);
        gameMap.getChildren().addAll(drawableStaedte.getCanvasliste());

        // Setzt clipping Region auf Bereich vom anchorPane
        Rectangle clip = new Rectangle();
        clip.widthProperty().bind(anchorPane.widthProperty());
        clip.heightProperty().bind(anchorPane.heightProperty());
        anchorPane.setClip(clip);

        // Setzt game map auf anchor pane
        anchorPane.getChildren().add(gameMap);

        // Setzt anfangs position der Karte
        gameMap.setLayoutX(mapInitialX);
        gameMap.setLayoutY(mapInitialY);

        // Läd event handler für dragging, scrolling, window resizing
        MapHandlers.ladeMapHandler(anchorPane, gameMap, centerMap);
    }


    /**
     * Setzt die Spielerliste.
     *
     * @param spielerListe - die gesetzt wird.
     */
    public static void setSpielerListe(ArrayList<Spieler> spielerListe) {
        Spielbrett.spielerListe = spielerListe;
    }
}
