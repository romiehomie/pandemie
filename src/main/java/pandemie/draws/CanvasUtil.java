package pandemie.draws;

import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import static java.lang.Math.cos;
import static java.lang.Math.sin;

/**
 *  Darstellungshilfen um bestimmte Spielobjekte auf dem Spielplan darzustellen
 */
public class CanvasUtil {

    /**
     * Die Methode strokeLine zeichnet die möglichen Verbindungslinien zwischen 2 Städten
     *
     * @param canvas Zeichenfläche
     * @param xStart x-Koordinate der 1. Stadt
     * @param yStart y-Koordinate der 1. Stadt
     * @param xEnd x-Koordinate der 2. Stadt
     * @param yEnd y-Koordinate der 2. Stadt
     */
    public static void strokeLine(Canvas canvas, double xStart, double yStart, double xEnd, double yEnd) {
        GraphicsContext gc = canvas.getGraphicsContext2D();
        gc.strokeLine(xStart, yStart, xEnd, yEnd);
        gc.stroke();
    }

    /**
     * Die Methode zeichnet mit einem ausgefüllten Kreis den Standort einer Stadt auf die Karte
     *
     * @param canvas Zeichenfläche
     * @param xCenter x-Koordinate des Zentrums eines Standortes einer Stadt
     * @param yCenter y-Koordinate des Zentrums eines Standortes einer Stadt
     * @param radius Radius der zu zeichnenden Stadt
     */
    public static void fillCircle(Canvas canvas, double xCenter, double yCenter, double radius){
        canvas.getGraphicsContext2D().fillOval(xCenter-radius, yCenter-radius, 2*radius, 2*radius);
    }

    /**
     * Die Methode zeichnet ein ausgefülltes Dreieck, welches zur Gestaltung eines Spielsteines beötigt wird
     *
     * @param canvas Zeichenfläche
     * @param xTop x-Koordinate des Startpunktes um das Dreieck zu zeichnen
     * @param yTop y-Koordinate des Startpunktes um das Dreieck zu zeichnen
     * @param width Breite des Dreieckes
     * @param height Höhe des Dreieckes
     */
    public static void fillTriangle(Canvas canvas, double xTop, double yTop, double width, double height){
        GraphicsContext gc = canvas.getGraphicsContext2D();
        gc.beginPath();
        gc.moveTo(xTop, yTop);
        gc.lineTo(xTop+width/2, yTop+height);
        gc.lineTo(xTop-width/2, yTop+height);
        gc.lineTo(xTop, yTop);
        gc.closePath();
        gc.fill();
    }

    /**
     * Die Methode zeichnet einen ausgefüllten Spielstein, welcher aus einem ausgefüllten Dreicheck und einem
     * ausgefüllten Kreis zusammengesetzt ist
     *
     * @param canvas Zeichenfläche
     * @param xStart x-Koordinate des Kopfmittelpunktes der Spielfigur und der Spitze des Körperdreiecks
     * @param yStart y-Koordinate des Kopfmittelpunktes der Spielfigur und der Spitze des Körperdreiecks
     * @param radius Radius des Spielfigurkopfes
     * @param width Breite des Körperdreicks
     * @param height Höhe  des Körperdreicks
     */
    public static void fillMeeple(Canvas canvas, double xStart, double yStart, double radius, double width, double height) {
        GraphicsContext gc = canvas.getGraphicsContext2D();
        fillCircle(canvas, xStart, yStart, radius);
        fillTriangle(canvas, xStart, yStart, width, height);
    }

    /**
     * Die Methode zeichnet die Erregerskalen in Form von kleinen ausgefüllten Kreisen um die Städte.
     * Pro Erreger sind 1-3 Kreise in ihrer jeweiligen Erregerfarbe möglich
     *
     * @param canvas Zeichenfläche
     * @param xCityCenter x-Koordinate des Standortzentrums
     * @param yCityCenter y-Koordinate des Standortzentrums
     * @param dist Distanz zwischen dem Standortzentrum und den Kreisen zur Darstellung der Erreger
     * @param satRadius Radius der Erregerkreise
     * @param slot Anzahl der Erregerkreise; maximal 12, jeweils 3 für jeden Erregertypen
     */
    public static void satelliteCircle(Canvas canvas,
                                       double xCityCenter,
                                       double yCityCenter,
                                       double dist,
                                       double satRadius,
                                       int slot){
        GraphicsContext gc = canvas.getGraphicsContext2D();
        //gc.setGlobalAlpha(0.5);
        double xSatCenter = xCityCenter + dist*sin(2*Math.PI/12*slot);
        double ySatCenter = yCityCenter - dist*cos(2*Math.PI/12*slot);

        fillCircle(canvas, xSatCenter, ySatCenter, satRadius);
    }

    /**
     * Die Methode zeichnet ein Forschungslabor in ausgefüllter Darstellung an die Standorte, wo eines entstanden ist
     * bzw. das erste zu Anfang des Spieles in der Stadt Atlanta
     *
     * @param canvas Zeichenfläche
     * @param xTop x-Koordinate des Startpunktes um das Symbol des Forschungslabors zu zeichnen
     * @param yTop y-Koordinate des Startpunktes um das Symbol des Forschungslabors zu zeichnen
     * @param width Breite des Symbols des Forschungslabors
     * @param height Höhe  des Symbols des Forschungslabors
     */
    public static void fillResearchCenter(Canvas canvas, double xTop, double yTop, double width, double height) {
        GraphicsContext gc = canvas.getGraphicsContext2D();
        gc.beginPath();
        gc.moveTo(xTop, yTop);
        gc.lineTo(xTop+width/2, yTop+height);
        gc.lineTo(xTop+width/2, yTop+2*height);
        gc.lineTo(xTop-width/2, yTop+2*height);
        gc.lineTo(xTop-width/2, yTop+height);
        gc.lineTo(xTop, yTop);
        gc.closePath();
        gc.fill();
    }

    // Die nächsten 4 Methoden zeichnen die Erregersymbole der Städte in die Standortmuster

    /**
     * Methode für das Zeichnen des Erregersymbols Stern
     *
     * @param canvas Zeichenfläche
     * @param xCenter x-Koordinate des Startpunktes am obersten Punkt des Stadtsymbols
     * @param yCenter y-Koordinate des Startpunktes am obersten Punkt des Stadtsymbols
     * @param innerRadius Innenradius des Sterns
     * @param outerRadius Aussenradius des Sterns
     */
    public static void stroke5Star(Canvas canvas,
                                   double xCenter,
                                   double yCenter,
                                   double innerRadius,
                                   double outerRadius){
        GraphicsContext gc = canvas.getGraphicsContext2D();
        gc.beginPath();
        // Hilsvariablen
        double x, y, arg;

        // Startpunkt am obersten Punkt
        x = xCenter; //Sinus-Term ist 0 für den obersten Punkt
        y = yCenter - outerRadius; //Cosinus-Term ist 1 für den obersten Punkt
        gc.moveTo(x,y);
        // zeichnet die Linien
        for(int i=1; i<=10; i++){
            arg = 2*Math.PI/10. * i;
            x = xCenter + ((i%2==0)?outerRadius:innerRadius)*sin(arg);
            y = yCenter - ((i%2==0)?outerRadius:innerRadius)*cos(arg);
            gc.lineTo(x,y);
        }
        gc.closePath();
        gc.stroke();
    }

    /**
     * Methode für das Zeichnen des Erregersymbols Fünfeck
     * @param canvas Zeichenfläche
     * @param xCenter x-Koordinate des Startpunktes am obersten Punkt des Stadtsymbols
     * @param yCenter y-Koordinate des Startpunktes am obersten Punkt des Stadtsymbols
     * @param radius Radius
     */
    public static void stroke5gon(Canvas canvas,
                                   double xCenter,
                                   double yCenter,
                                   double radius){
        GraphicsContext gc = canvas.getGraphicsContext2D();
        gc.beginPath();
        // Hilsvariablen
        double x, y, arg;

        // Startpunkt am obersten Punkt
        x = xCenter; //Sinus-Term ist 0 für den obersten Punkt
        y = yCenter - radius; //Cosinus-Term ist 1 für den obersten Punkt
        gc.moveTo(x,y);
        // zeichnet die Linien
        for(int i=1; i<=5; i++){
            arg = 2*Math.PI/5. * i;
            x = xCenter + radius*sin(arg);
            y = yCenter - radius*cos(arg);
            gc.lineTo(x,y);
        }
        gc.closePath();
        gc.stroke();
    }

    /**
     * Methode für das Zeichnen des Erregersymbols Viereck
     * @param canvas Zeichenfläche
     * @param xCenter x-Koordinate des Startpunktes am obersten Punkt des Stadtsymbols
     * @param yCenter y-Koordinate des Startpunktes am obersten Punkt des Stadtsymbols
     * @param radius Radius
     */
    public static void strokeRect(Canvas canvas,
                                  double xCenter,
                                  double yCenter,
                                  double radius) {
        GraphicsContext gc = canvas.getGraphicsContext2D();
        gc.beginPath();
        // Hilsvariablen
        double x, y, arg;

        // Startpunkt am obersten Punkt
        x = xCenter; //Sinus-Term ist 0 für den obersten Punkt
        y = yCenter - radius; //Cosinus-Term ist 1 für den obersten Punkt
        gc.moveTo(x,y);
        // zeichnet die Linien
        for(int i=1; i<=4; i++) {
            arg = (2*Math.PI/4. * i);
            x = xCenter + radius*sin(arg);
            y = yCenter - radius*cos(arg);
            gc.lineTo(x,y);
        }
        gc.closePath();
        gc.stroke();
    }

    /**
     * Methode für das Zeichnen des Erregersymbols Kreis
     *
     * @param canvas Zeichenfläche
     * @param xCenter x-Koordinate vom Mittelpunkt des Kreises
     * @param yCenter y-Koordinate vom Mittelpunkt des Kreises
     * @param radius Radius des Kreises
     */
    public static void strokeCircle(Canvas canvas, double xCenter, double yCenter, double radius) {
        canvas.getGraphicsContext2D().strokeOval(xCenter-radius, yCenter-radius, 2*radius, 2*radius);
    }

}
