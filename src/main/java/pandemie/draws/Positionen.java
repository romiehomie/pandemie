package pandemie.draws;

import pandemie.game.ErregerTyp;

import static pandemie.game.spielkarten.Stadt.*;

public class Positionen {
    public static void load(){
        /**
         * Städte mit dem Erregertyp "BLAU"
         */
        SAN_FRANCISCO.setPosX(870);
        SAN_FRANCISCO.setPosY(782);
        SAN_FRANCISCO.setSymbolX(870);
        SAN_FRANCISCO.setSymbolY(782);

        CHICAGO.setPosX(1378);
        CHICAGO.setPosY(723);
        CHICAGO.setSymbolX(1233);
        CHICAGO.setSymbolY(685);

        ATLANTA.setPosX(1432);
        ATLANTA.setPosY(842);
        ATLANTA.setSymbolX(1432);
        ATLANTA.setSymbolY(842);

        TORONTO.setPosX(1505);
        TORONTO.setPosY(690);
        TORONTO.setSymbolX(1505);
        TORONTO.setSymbolY(690);

        NEW_YORK.setPosX(1590);
        NEW_YORK.setPosY(740);
        NEW_YORK.setSymbolX(1700);
        NEW_YORK.setSymbolY(700);

        WASHINGTON.setPosX(1545);
        WASHINGTON.setPosY(765);
        WASHINGTON.setSymbolX(1660);
        WASHINGTON.setSymbolY(840);

        LONDON.setPosX(2700);
        LONDON.setPosY(575);
        LONDON.setSymbolX(2500);
        LONDON.setSymbolY(484);

        ESSEN.setPosX(2810);
        ESSEN.setPosY(577);
        ESSEN.setSymbolX(2838);
        ESSEN.setSymbolY(466);

        PARIS.setPosX(2735);
        PARIS.setPosY(616);
        PARIS.setSymbolX(2735);
        PARIS.setSymbolY(616);

        MADRID.setPosX(2645);
        MADRID.setPosY(745);
        MADRID.setSymbolX(2490);
        MADRID.setSymbolY(745);

        MAILAND.setPosX(2840);
        MAILAND.setPosY(666);
        MAILAND.setSymbolX(2950);
        MAILAND.setSymbolY(622);

        ST_PETERSBURG.setPosX(3156);
        ST_PETERSBURG.setPosY(450);
        ST_PETERSBURG.setSymbolX(3156);
        ST_PETERSBURG.setSymbolY(450);

        /**
         * Städte mit dem ErregerTyp "GELB"
         */
        LOS_ANGELES.setPosX(928);
        LOS_ANGELES.setPosY(840);
        LOS_ANGELES.setSymbolX(928);
        LOS_ANGELES.setSymbolY(940);

        MEXICO_STADT.setPosX(1215);
        MEXICO_STADT.setPosY(1060);
        MEXICO_STADT.setSymbolX(1215);
        MEXICO_STADT.setSymbolY(1060);

        MIAMI.setPosX(1500);
        MIAMI.setPosY(960);
        MIAMI.setSymbolX(1700);
        MIAMI.setSymbolY(1070);

        BOGOTA.setPosX(1590);
        BOGOTA.setPosY(1275);
        BOGOTA.setSymbolX(1590);
        BOGOTA.setSymbolY(1275);

        LIMA.setPosX(1545);
        LIMA.setPosY(1530);
        LIMA.setSymbolX(1380);
        LIMA.setSymbolY(1575);

        SANTIAGO.setPosX(1640);
        SANTIAGO.setPosY(1852);
        SANTIAGO.setSymbolX(1465);
        SANTIAGO.setSymbolY(1895);

        SAN_PAULO.setPosX(2000);
        SAN_PAULO.setPosY(1700);
        SAN_PAULO.setSymbolX(2000);
        SAN_PAULO.setSymbolY(1700);

        BUENOS_AIRES.setPosX(1820);
        BUENOS_AIRES.setPosY(1870);
        BUENOS_AIRES.setSymbolX(1820);
        BUENOS_AIRES.setSymbolY(1870);

        LAGOS.setPosX(2755);
        LAGOS.setPosY(1250);
        LAGOS.setSymbolX(2755);
        LAGOS.setSymbolY(1250);

        KHARTUM.setPosX(3188);
        KHARTUM.setPosY(1117);
        KHARTUM.setSymbolX(3188);
        KHARTUM.setSymbolY(1196);

        KINSHASA.setPosX(2929);
        KINSHASA.setPosY(1415);
        KINSHASA.setSymbolX(2930);
        KINSHASA.setSymbolY(1515);

        JOHANNESBURG.setPosX(3122);
        JOHANNESBURG.setPosY(1742);
        JOHANNESBURG.setSymbolX(3122);
        JOHANNESBURG.setSymbolY(1742);

        /**
         * Städte mit dem ErregerTyp "SCHWARZ"
         */
        MOSKAU.setPosX(3265);
        MOSKAU.setPosY(515);
        MOSKAU.setSymbolX(3360);
        MOSKAU.setSymbolY(525);

        ISTANBUL.setPosX(3135);
        ISTANBUL.setPosY(735);
        ISTANBUL.setSymbolX(3135);
        ISTANBUL.setSymbolY(735);

        ALGIER.setPosX(2745);
        ALGIER.setPosY(800);
        ALGIER.setSymbolX(2735);
        ALGIER.setSymbolY(888);

        KAIRO.setPosX(3168);
        KAIRO.setPosY(900);
        KAIRO.setSymbolX(3080);
        KAIRO.setSymbolY(985);

        RIAD.setPosX(3400);
        RIAD.setPosY(980);
        RIAD.setSymbolX(3405);
        RIAD.setSymbolY(1105);

        BAGDAD.setPosX(3370);
        BAGDAD.setPosY(850);
        BAGDAD.setSymbolX(3370);
        BAGDAD.setSymbolY(850);

        TEHERAN.setPosX(3470);
        TEHERAN.setPosY(815);
        TEHERAN.setSymbolX(3555);
        TEHERAN.setSymbolY(690);

        KARATSCHI.setPosX(3705);
        KARATSCHI.setPosY(975);
        KARATSCHI.setSymbolX(3625);
        KARATSCHI.setSymbolY(910);

        DELHI.setPosX(3855);
        DELHI.setPosY(919);
        DELHI.setSymbolX(3865);
        DELHI.setSymbolY(830);

        MUMBAI.setPosX(3795);
        MUMBAI.setPosY(1065);
        MUMBAI.setSymbolX(3715);
        MUMBAI.setSymbolY(1125);

        KALKUTTA.setPosX(4025);
        KALKUTTA.setPosY(1010);
        KALKUTTA.setSymbolX(4075);
        KALKUTTA.setSymbolY(925);

        CHENNAI.setPosX(3900);
        CHENNAI.setPosY(1155);
        CHENNAI.setSymbolX(3900);
        CHENNAI.setSymbolY(1270);

        /**
         * Städte mit dem ErregerTyp "ROT"
         */
        PEKING.setPosX(4444);
        PEKING.setPosY(747);
        PEKING.setSymbolX(4362);
        PEKING.setSymbolY(575);

        SEOUL.setPosX(4605);
        SEOUL.setPosY(783);
        SEOUL.setSymbolX(4625);
        SEOUL.setSymbolY(580);

        SHANGHAI.setPosX(4520);
        SHANGHAI.setPosY(880);
        SHANGHAI.setSymbolX(4305);
        SHANGHAI.setSymbolY(855);

        TOKIO.setPosX(4795);
        TOKIO.setPosY(812);
        TOKIO.setSymbolX(4920);
        TOKIO.setSymbolY(626);

        OSAKA.setPosX(4730);
        OSAKA.setPosY(830);
        OSAKA.setSymbolX(4905);
        OSAKA.setSymbolY(920);

        HONGKONG.setPosX(4410);
        HONGKONG.setPosY(1010);
        HONGKONG.setSymbolX(4410);
        HONGKONG.setSymbolY(1010);

        TAIPEH.setPosX(4522);
        TAIPEH.setPosY(975);
        TAIPEH.setSymbolX(4656);
        TAIPEH.setSymbolY(1012);

        BANGKOK.setPosX(4208);
        BANGKOK.setPosY(1141);
        BANGKOK.setSymbolX(4110);
        BANGKOK.setSymbolY(1195);

        HO_CHI_MINH_STADT.setPosX(4300);
        HO_CHI_MINH_STADT.setPosY(1185);
        HO_CHI_MINH_STADT.setSymbolX(4400);
        HO_CHI_MINH_STADT.setSymbolY(1321);

        JAKARTA.setPosX(4300);
        JAKARTA.setPosY(1444);
        JAKARTA.setSymbolX(4155);
        JAKARTA.setSymbolY(1533);

        SYDNEY.setPosX(4965);
        SYDNEY.setPosY(1856);
        SYDNEY.setSymbolX(4965);
        SYDNEY.setSymbolY(1856);

        MANILA.setPosX(4515);
        MANILA.setPosY(1132);
        MANILA.setSymbolX(4694);
        MANILA.setSymbolY(1234);
    }
}
