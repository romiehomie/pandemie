package pandemie.draws;

import javafx.scene.canvas.Canvas;
import pandemie.game.spielablauf.Spieler;
import pandemie.game.spielkarten.Stadt;

import java.util.ArrayList;
import java.util.Calendar;

// ermittelt alle hinterlegten Städte, die auf den Spielplan gezeichnet werden
public class DrawableStaedte {
    ArrayList<DrawableStadt> staedteliste = new ArrayList<>();


    public DrawableStaedte() {
        for(Stadt stadt: Stadt.values()){
            staedteliste.add(new DrawableStadt(stadt));
            }
        }

    public void draw(ArrayList<Spieler> spielerListe) {
        for (DrawableStadt ds : staedteliste) {
            ds.drawStadt(spielerListe);
        }
    }

    public ArrayList<DrawableStadt> getStaedteliste() {
        return staedteliste;
    }

    public ArrayList<Canvas> getCanvasliste() {
        ArrayList<Canvas> canvasliste = new ArrayList<>();
        for (DrawableStadt ds : staedteliste) {
            canvasliste.add(ds.getCanvas());
        }
        return canvasliste;
    }

}


