package pandemie.view;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;
import javafx.stage.Modality;
import pandemie.MainHauptmenue;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Optional;

public class HauptmenueController {

    @FXML
    private Button singleplayer, load, beenden;

    @FXML
    private TextField spieler1tf, spieler2tf, spieler3tf, spieler4tf;

    /**
     * Beendet das Spiel vollständig. Vorher wird der Spieler aber noch gefragt,
     * ob derjenige das Spiel auch wirklich beenden möchte.
     */
    @FXML
    void spielBeenden() {
        // Es wird ein Alert-Objekt mit dem Titel "Spiel Beenden?!" erzeugt, welches den AlertType der Bestätigung aufweist.
        Alert a = new Alert(Alert.AlertType.CONFIRMATION);
        a.setTitle("Spiel Beenden?!");
        // Abfrage der Bestätigung, ob das Spiel wirklich beendet werden soll
        a.setHeaderText(null);
        a.setContentText("Möchten Sie das Spiel wirklich beenden?");
        // Alert-Fenster wird in die Mitte des Menü-Fensters gesetzt
        a.initModality(Modality.APPLICATION_MODAL);
        a.initOwner(beenden.getScene().getWindow());
        // Alert-Fenster wird angezeigt und es wird auf eine Bestätigung des Spielers gewartet
        Optional<ButtonType> op = a.showAndWait();
        // Das Spiel soll nur beendet werden, wenn der Spieler sein Ok dafür gegeben hat
        if (op.isPresent() && op.get() == ButtonType.OK) {
            Platform.exit();
        }
    }

    /**
     *
     * Ein vorher gespeicherter Spielstand wird geladen.
     *
     */
    @FXML
    void spielLaden() {

    }

    /**
     *
     * Bei Spielbeginn wechselt die Szene zum Spielfeld und fügt im Hauptmenue 4 Textfelder hinzu, wo man seinen Spielernamen eintragen kann.
     * Diese werden dann bei Spielbeginn in der jeweiligen Spielerbox als Label angezeigt, je nach dem wie viele Spielr mitspielen.
     *
     * @throws IOException
     */
    @FXML
    void starteSingleplayer() throws IOException {
        ArrayList<String> spielerNamen = new ArrayList<>();

        boolean keineEingabefehlerBeiDenNamen;
        // Ziehe die Spielernamen aus den Textfeldern
        keineEingabefehlerBeiDenNamen = zieheTextInListeWennMoeglich(spieler1tf, spielerNamen);
        keineEingabefehlerBeiDenNamen &= zieheTextInListeWennMoeglich(spieler2tf, spielerNamen);
        keineEingabefehlerBeiDenNamen &= zieheTextInListeWennMoeglich(spieler3tf, spielerNamen);
        keineEingabefehlerBeiDenNamen &= zieheTextInListeWennMoeglich(spieler4tf, spielerNamen);

        if(spielerNamen.size() > 1 && keineEingabefehlerBeiDenNamen){
            // Erstelle String aus Spielernamen
            String spielernamenstring = "";
            for(String name: spielerNamen) {
                spielernamenstring += name+";";
            }
            // Entferne letztes Semikolon
            spielernamenstring = spielernamenstring.substring(0, spielernamenstring.length()-1);
            // Speichere die Namensliste im Fensternamen
            MainHauptmenue.returnStage().setTitle(spielernamenstring);
            // Szenenwechsel zum Spielfeld wird durchgeführt
            MainHauptmenue.setRoot("Spielplan");
            // Resizeable - Methode des Spielfeldes wird auf true gesetzt
            MainHauptmenue.returnStage().setResizable(true);
        }
    }

    /**
     *
     * Hier findet eine Überprüfung des Spielernamens statt.
     * Falls der jeweilige Spielername gültig ist wird dieser in eine Liste von Spielernamen eingetragen
     * Falls dieser aber ungülig sein sollte, dann wird das jeweilige Textfeld gecleart und es werden die Gültigkeitskriterien
     * für einen gültigen Spielernamen im Textfeld abgebildet.
     *
     * @param tf Textfeld zum eintragen des Spielernamens
     * @param spielerNamen Liste von Spielernamen
     * @return true - eingetragener Spielername war gültig; false - eingetragener Spielername war ungültig und der Spieler wird darum gebeten einen gültigen Spielernamen einzutragen.
     */
    private boolean zieheTextInListeWennMoeglich(TextField tf, ArrayList<String> spielerNamen) {
        String name = tf.getCharacters().toString();
        if(gueltigerSpielername(name)){
            spielerNamen.add(name);
            return true;
        }else if(leeresFeld(name)){
            return true;
        } else{
            tf.clear();
            tf.setPromptText("Mindestens 3, maximal 10 Buchstaben!");
            return false;
        }
    }

    private boolean leeresFeld(String name) {
        return name.equals("");
    }

    /**
     * Hier werden die Gültigkeitkriterien für die jeweiligen Spielernamen festgestzt.
     * Beispielsweise muss ein gültiger Spielername 3 bis 10 Zeichen beinhalten und darf nur aus Groß- und Kleinbuchstaben bestehen
     *
     * @param string zu überprüfender Spielername
     * @return Gültigkeitskriterien für einen Spielernamen
     */
    private boolean gueltigerSpielername(String string) {
        return string.matches("[a-zA-Z]{3,10}");
    }

    @FXML
    void initialize() {
        assert singleplayer != null : "fx:id=\"singleplayer\" was not injected: check your FXML file 'Hauptmenue.fxml'.";
        assert load != null : "fx:id=\"load\" was not injected: check your FXML file 'Hauptmenue.fxml'.";
        assert beenden != null : "fx:id=\"beenden\" was not injected: check your FXML file 'Hauptmenue.fxml'.";
        assert spieler1tf != null : "fx:id=\"spieler1tf\" was not injected: check your FXML file 'Hauptmenue.fxml'.";
        assert spieler2tf != null : "fx:id=\"spieler1tf\" was not injected: check your FXML file 'Hauptmenue.fxml'.";
        assert spieler3tf != null : "fx:id=\"spieler1tf\" was not injected: check your FXML file 'Hauptmenue.fxml'.";
        assert spieler4tf != null : "fx:id=\"spieler1tf\" was not injected: check your FXML file 'Hauptmenue.fxml'.";

        spieler1tf.setText("Karl");
        spieler2tf.setText("Heinz");
        spieler3tf.setText("Otto");
        spieler4tf.setText("Fritz");
    }
}
