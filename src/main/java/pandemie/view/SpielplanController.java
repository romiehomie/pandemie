package pandemie.view;

import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.VBox;
import javafx.scene.effect.*;
import javafx.scene.paint.Color;
import javafx.scene.layout.GridPane;

import javafx.util.Duration;
import pandemie.MainHauptmenue;
import pandemie.draws.Spielbrett;
import pandemie.game.ErregerTyp;
import pandemie.game.rollen.Rolle;
import pandemie.game.spielablauf.Spiel;
import pandemie.game.spielablauf.Spieler;
import pandemie.game.spielkarten.ISpielkarte;
import pandemie.game.spielkarten.Stadt;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;

public class SpielplanController {

    private String[] spielerNamen;
    private String cssUeberSchrift = "-fx-text-fill: white; -fx-font-size: 16px; -fx-background-color: #000000;";
    private Spiel spiel;
    private ToggleGroup toggleGroup;
    private Timeline blinkEffekt;

    @FXML
    private MenuItem speichern;
    @FXML
    private MenuItem laden;
    @FXML
    private MenuItem beenden;

    @FXML
    private Label anzahlAusbrueche;
    @FXML
    private Label anzahlInfektionen;

    @FXML
    private Label anzahlWuerfel1;
    @FXML
    private Label anzahlWuerfel2;
    @FXML
    private Label anzahlWuerfel3;
    @FXML
    private Label anzahlWuerfel4;

    @FXML
    private Label anzahlTage;

    @FXML
    private Button rundeBeenden;

    @FXML
    private VBox spieler1vBox;
    @FXML
    private VBox spieler2vBox;

    @FXML
    private Label name1;

    @FXML
    private Label rolle1;

    @FXML
    private Label pos1;

    @FXML
    private FlowPane handkarten1;


    @FXML
    private Label name2;

    @FXML
    private Label rolle2;

    @FXML
    private Label pos2;

    @FXML
    private FlowPane handkarten2;

    @FXML
    private VBox spieler3vBox;

    @FXML
    private Label name3;

    @FXML
    private Label rolle3;

    @FXML
    private Label pos3;

    @FXML
    private FlowPane handkarten3;

    @FXML
    private VBox spieler4vBox;

    @FXML
    private Label name4;

    @FXML
    private Label rolle4;

    @FXML
    private Label pos4;

    @FXML
    private FlowPane handkarten4;

    @FXML
    private AnchorPane centerAnchorPane;

    @FXML
    private VBox boxNachrichten;

    @FXML
    private VBox boxHandkarten;

    @FXML
    private VBox boxAktionen;


    @FXML
    void initialize() {
        assert speichern != null : "fx:id=\"speichern\" was not injected: check your FXML file 'Spielplan.fxml'.";
        assert laden != null : "fx:id=\"laden\" was not injected: check your FXML file 'Spielplan.fxml'.";
        assert beenden != null : "fx:id=\"beenden\" was not injected: check your FXML file 'Spielplan.fxml'.";
        assert anzahlAusbrueche != null : "fx:id=\"anzahlAusbrueche\" was not injected: check your FXML file 'Spielplan.fxml'.";
        assert anzahlInfektionen != null : "fx:id=\"anzahlInfektionen\" was not injected: check your FXML file 'Spielplan.fxml'.";
        assert anzahlWuerfel1 != null : "fx:id=\"anzahlWuerfel1\" was not injected: check your FXML file 'Spielplan.fxml'.";
        assert anzahlWuerfel2 != null : "fx:id=\"anzahlWuerfel2\" was not injected: check your FXML file 'Spielplan.fxml'.";
        assert anzahlWuerfel3 != null : "fx:id=\"anzahlWuerfel3\" was not injected: check your FXML file 'Spielplan.fxml'.";
        assert anzahlWuerfel4 != null : "fx:id=\"anzahlWuerfel4\" was not injected: check your FXML file 'Spielplan.fxml'.";
        assert anzahlTage != null : "fx:id=\"anzahlTage\" was not injected: check your FXML file 'Spielplan.fxml'.";
        assert rundeBeenden != null : "fx:id=\"rundeBeenden\" was not injected: check your FXML file 'Spielplan.fxml'.";
        assert spieler1vBox != null : "fx:id=\"spieler1vBox\" was not injected: check your FXML file 'Spielplan.fxml'.";
        assert name1 != null : "fx:id=\"name1\" was not injected: check your FXML file 'Spielplan.fxml'.";
        assert rolle1 != null : "fx:id=\"rolle1\" was not injected: check your FXML file 'Spielplan.fxml'.";
        assert pos1 != null : "fx:id=\"pos1\" was not injected: check your FXML file 'Spielplan.fxml'.";
        assert handkarten1 != null : "fx:id=\"handkarten1\" was not injected: check your FXML file 'Spielplan.fxml'.";
        assert spieler2vBox != null : "fx:id=\"spieler2vBox\" was not injected: check your FXML file 'Spielplan.fxml'.";
        assert name2 != null : "fx:id=\"name2\" was not injected: check your FXML file 'Spielplan.fxml'.";
        assert rolle2 != null : "fx:id=\"rolle2\" was not injected: check your FXML file 'Spielplan.fxml'.";
        assert pos2 != null : "fx:id=\"pos2\" was not injected: check your FXML file 'Spielplan.fxml'.";
        assert handkarten2 != null : "fx:id=\"handkarten2\" was not injected: check your FXML file 'Spielplan.fxml'.";
        assert spieler3vBox != null : "fx:id=\"spieler3vBox\" was not injected: check your FXML file 'Spielplan.fxml'.";
        assert name3 != null : "fx:id=\"name3\" was not injected: check your FXML file 'Spielplan.fxml'.";
        assert rolle3 != null : "fx:id=\"rolle3\" was not injected: check your FXML file 'Spielplan.fxml'.";
        assert pos3 != null : "fx:id=\"pos3\" was not injected: check your FXML file 'Spielplan.fxml'.";
        assert handkarten3 != null : "fx:id=\"handkarten3\" was not injected: check your FXML file 'Spielplan.fxml'.";
        assert spieler4vBox != null : "fx:id=\"spieler4vBox\" was not injected: check your FXML file 'Spielplan.fxml'.";
        assert name4 != null : "fx:id=\"name4\" was not injected: check your FXML file 'Spielplan.fxml'.";
        assert rolle4 != null : "fx:id=\"rolle4\" was not injected: check your FXML file 'Spielplan.fxml'.";
        assert pos4 != null : "fx:id=\"pos4\" was not injected: check your FXML file 'Spielplan.fxml'.";
        assert handkarten4 != null : "fx:id=\"handkarten4\" was not injected: check your FXML file 'Spielplan.fxml'.";
        assert centerAnchorPane != null : "fx:id=\"centerAnchorPane\" was not injected: check your FXML file 'Spielplan.fxml'.";
        assert boxNachrichten != null : "fx:id=\"frageBox\" was not injected: check your FXML file 'Spielplan.fxml'.";
        assert boxHandkarten != null : "fx:id=\"boxHandkarten\" was not injected: check your FXML file 'Spielplan.fxml'.";
        assert boxAktionen != null : "fx:id=\"boxAktionen\" was not injected: check your FXML file 'Spielplan.fxml'.";

        startePandemie();
        // Beginne ersten Zug
        spiel.beginneErstenZug();
    }

    private void startePandemie() {
        // Ziehe Spielernamen aus dem Titel und erstelle spielerListe (der Namen)
        spielerNamen = MainHauptmenue.returnStage().getTitle().split(";");
        int spielerZahl = spielerNamen.length;
        MainHauptmenue.returnStage().setTitle("");


        // Initialisiere Spiel
        int schwierigKeitsgrad = 2;
        spiel = new Spiel(spielerZahl, schwierigKeitsgrad, this);

        // Initialisiere AktionsPanel


        // Weise zufällige Rollen zu
        setSpielerBoxenLabels(spiel.getSpielerListe());
        setCounterLeistenLabels();

        // Initialisiere Karte
        Spielbrett.setSpielerListe(spiel.getSpielerListe());
        Spielbrett.starte(centerAnchorPane);

        // Spieler 1 dran
        blinkEffekt = spielerAnzeigen(1);
        spielerAnzeigen(1);
    }

    public void loescheNachrichten() {
        boxNachrichten.getChildren().removeAll(boxNachrichten.getChildren());
        boxNachrichten.setVisible(false);
    }


    public void createBtnClearLog() {
        boxNachrichten.setVisible(true);
        Button btn = new Button();
        btn.setOnAction(event -> {
            loescheNachrichten();
        });
        btn.setText("Nachrichten löschen");
        boxNachrichten.getChildren().add(btn);
    }

    public void wurdeInfiziert(Stadt infektionskarte, ErregerTyp erreger, int wuerfel) {
        Label label = new Label();
        label.setStyle(cssUeberSchrift);
        label.setText("Die Stadt " + infektionskarte.getName() + " wurde mit dem Erreger " + erreger.getName()
                + " infiziert. Die Stadt hat nun " + wuerfel + " " + erreger.getName() + "punkte.");
        boxNachrichten.getChildren().add(label);
    }


    /**
     * Wechselt bei Beendigung des Spiels die Szene zurück ins Hauptmenü
     *
     * @throws IOException
     */
    @FXML
    void spielBeenden() throws IOException {
        MainHauptmenue.setRoot("Hauptmenue");
    }

    /**
     * Ein vorher gespeicherter Spielstand wird geladen.
     */
    @FXML
    void spielLaden() {

    }

    /**
     * Ein bisheriger Spielverlauf wird abgespeichert.
     */
    @FXML
    void spielSpeichern() {

    }

    @FXML
    void beendeZug() {
        blinkEffekt.stop();
        spiel.inkrementNummerSpielerDran();
        blinkEffekt = spielerAnzeigen(spiel.getNummerSpielerDran());
        aktualisiereKartenUndAktionsPanel(spiel.getSpielerListe().get(spiel.getNummerSpielerDran()));
        incrementLabelTage();
        spiel.erregerSpielen();
    }

    public String[] getSpielerNamen() {
        return spielerNamen;
    }

    public void setCounterLeistenLabels() {
        setAusbruchsCounter(0);
        setInfektionsRate(1);
        setDurchseuchungsCounter(ErregerTyp.BLAU, 0);
        setDurchseuchungsCounter(ErregerTyp.ROT, 0);
        setDurchseuchungsCounter(ErregerTyp.SCHWARZ, 0);
        setDurchseuchungsCounter(ErregerTyp.GELB, 0);
        setLabelTage(0);
    }

    public void setSpielerBoxenLabels(ArrayList<Spieler> spielerListe) {
        int spielerNummer = 1;
        for (Spieler spieler : spielerListe) {
            setLabelName(spielerNummer, spielerListe);
            setLabelRolle(spielerNummer, spieler.getRolle());
            setLabelPosition(spielerNummer, spieler.getStadt());
            spielerNummer++;
        }
        for (; spielerNummer < 5; spielerNummer++) {
            kill(spielerNummer);
        }
    }

    /**
     * Setzt auf das Namenslabel des jeweiligen Spielers seinen Spielernamen.
     *
     * @param spielerNummer - die Label-Nummer des Spielers
     * @param spielerListe  -
     */
    private void setLabelName(int spielerNummer, ArrayList<Spieler> spielerListe) {
        if (spielerNummer == 1) {
            name1.setText(spielerNamen[0]);
            name1.setTextFill(spielerListe.get(0).getRolle().getFarbe());
            addWhiteShadow(name1);
        } else if (spielerNummer == 2) {
            name2.setText(spielerNamen[1]);
            name2.setTextFill(spielerListe.get(1).getRolle().getFarbe());
            addWhiteShadow(name2);
        } else if (spielerNummer == 3) {
            name3.setText(spielerNamen[2]);
            name3.setTextFill(spielerListe.get(2).getRolle().getFarbe());
            addWhiteShadow(name3);
        } else if (spielerNummer == 4) {
            name4.setText(spielerNamen[3]);
            name4.setTextFill(spielerListe.get(3).getRolle().getFarbe());
            addWhiteShadow(name4);
        } else {
            System.err.println("Nicht genügend Spieler um so viele Namens-Label zu setzen!");
        }
    }

    /**
     * Entfernt überzählige Labeltexte vom Spieler mit der vorgegebenen spielerNummer.
     *
     * @param spielerNummer - die vorgegebene spielerNummer von der die Labels geleert werden.
     */
    private void kill(int spielerNummer) {
        if (spielerNummer == 3) {
            name3.setText("");
            rolle3.setText("");
            pos3.setText("");
        } else if (spielerNummer == 4) {
            name4.setText("");
            rolle4.setText("");
            pos4.setText("");
        } else {
            System.err.println("Ein Spieler mit der Nummer " + spielerNummer + " existieren nicht oder darf nicht entfernt werden.");
        }
    }

    /**
     * Setzt den Ausbruchscounter auf einen vorgegebenen Wert.
     *
     * @param count - int - neuer Wert des Ausbruchscounters
     */
    public void setAusbruchsCounter(Integer count) {
        // count wird in ein String umgewandelt und der String count wird als Text für das Label "anzahlAusbrüche" gesetzt
        anzahlAusbrueche.setText(count + " / 8");
        anzahlAusbrueche.setTextFill(Color.BLACK);
        // Abfrage count gleich 7 ist; wenn ja, dann soll sich die Schriftfarbe auf rot ändern, ansonsten soll die ursprüngliche Farbe beibehalten werden
        if (count == 7) {
            anzahlAusbrueche.setTextFill(Color.RED);
        } else if (count > 8) {
            throw new IllegalArgumentException("Fehler das Spieler ist schon längst verloren!.");
        }
        addWhiteShadow(anzahlAusbrueche);
    }

    /**
     * Zeigt an, welcher Spieler gerade dran ist.
     * - Spielername blibkt
     *
     * @param spielerNummer - int - dem Spieler zugeordnete Nummer
     */
    public Timeline spielerAnzeigen(int spielerNummer) {
        // Abfrage welcher Spieler am Zug ist; zur Kennzeichnung wird die jeweilige Spielerbox durch einen farblichen Rand gekennzeichnet
        Label label;
        if (spielerNummer == 1) {
            label = name1;
        } else if (spielerNummer == 2) {
            label = name2;
        } else if (spielerNummer == 3) {
            label = name3;
        } else {
            label = name4;
        }
        Timeline blinkEffekt = new Timeline(new KeyFrame(Duration.seconds(0.5), evt -> label.setTextFill(Color.BLACK)),
                new KeyFrame(Duration.seconds(0.1), evt -> label.setTextFill(Color.YELLOWGREEN)));
        blinkEffekt.setCycleCount(Animation.INDEFINITE);
        blinkEffekt.play();
        return blinkEffekt;
    }

    /**
     * Setzt die Anzahl der Seuchenwürfel des jeweiligen ErregerTyps
     *
     * @param erregerTyp Krankheitserreger
     * @param wuerfel    Seuchenwürfel (Rot = Viren; Blau = Parasiten; Gelb = Bakterien; Schwarz = Pilze)
     */
    public void setDurchseuchungsCounter(ErregerTyp erregerTyp, Integer wuerfel) {
        Effect glow = new Glow(1.0);
        // wuerfel wird in ein String umgewandelt und der String wuerfel wird als Text für das jeweilige Label "anzahlWuerfel" gesetzt
        // Abfrage nach dem ErregerTyp und setzen der Textfarbe
        if (erregerTyp == ErregerTyp.ROT) {
            anzahlWuerfel1.setText(Integer.toString(wuerfel));
            anzahlWuerfel1.setTextFill(erregerTyp.getFarbe());
            addWhiteShadow(anzahlWuerfel1);
            //anzahlWuerfel1.setStyle("-fx-stroke: white; -fx-stroke-width: 1;");
        } else if (erregerTyp == ErregerTyp.BLAU) {
            anzahlWuerfel2.setText(Integer.toString(wuerfel));
            anzahlWuerfel2.setTextFill(erregerTyp.getFarbe());
            addWhiteShadow(anzahlWuerfel2);
        } else if (erregerTyp == ErregerTyp.GELB) {
            anzahlWuerfel3.setText(Integer.toString(wuerfel));
            anzahlWuerfel3.setTextFill(erregerTyp.getFarbe());
            addWhiteShadow(anzahlWuerfel3);
        } else if (erregerTyp == ErregerTyp.SCHWARZ) {
            anzahlWuerfel4.setTextFill(erregerTyp.getFarbe());
            anzahlWuerfel4.setText(" " + Integer.toString(wuerfel) + " ");
            addWhiteShadow(anzahlWuerfel4);
        }
    }

    public static void addWhiteShadow(Label label) {
        DropShadow dropShadow = new DropShadow();
        dropShadow.setRadius(3.0);
        dropShadow.setOffsetX(0.);
        dropShadow.setOffsetY(0.);
        dropShadow.setSpread(.9);
        dropShadow.setColor(Color.color(1., 1., 1.));
        label.setEffect(dropShadow);
    }

    /**
     * setzt die Infektionsrate
     *
     * @param infektionsRate
     */
    public void setInfektionsRate(Integer infektionsRate) {
        // infektionen wird in ein String umgewandelt und der String infektionen wird als Text für das Label "anzahlInfektionen" gesetzt
        anzahlInfektionen.setTextFill(Color.BLACK);
        anzahlInfektionen.setText(infektionsRate + " / 7");
        addWhiteShadow(anzahlInfektionen);
    }

    /**
     * setzt die einzelnen Runden, die in diesem Fall als Tage dargestellt werden.
     *
     * @param tag Runde X
     */
    public void setLabelTage(Integer tag) {
        // tag wird in ein String umgewandelt und der String tag wird als Text für das Label "anzahlTage" gesetzt
        anzahlTage.setTextFill(Color.WHITE);
        anzahlTage.setText(Integer.toString(tag));
        addWhiteShadow(anzahlTage);
    }

    public void incrementLabelTage() {
        int neuerTag = 1 + Integer.parseInt(anzahlTage.getText());
        setLabelTage(neuerTag);
    }

    /**
     * setzt die einzelnen Rollen für die Spieler. Je nachdem welcher Spieler welche Rolle erhalten hat,
     * wird der Text in der jeweiligen Farbe der Rolle dargestellt.
     *
     * @param spielerNummer der jeweilige Spieler (spieler1vBox, spieler2, spieler3, spieler4)
     * @param rolle         die jeweilige Rolle des Spielers (Logistiker, Wissenschaftler, Forscher, Betriebsexperte, Arzt)
     */
    public void setLabelRolle(int spielerNummer, Rolle rolle) {
        // Überprüfung welche Rolle Spieler 1 hat und setzen der Rolle als Text sowie setzen der Textfarbe
        if (spielerNummer == 1) {
            rolle1.setText(rolle.getRollenname());
            rolle1.setTextFill(rolle.getFarbe());
            addWhiteShadow(rolle1);
        }
        // Überprüfung welche Rolle Spieler 2 hat und setzen der Rolle als Text sowie setzen der Textfarbe
        if (spielerNummer == 2) {
            rolle2.setText(rolle.getRollenname());
            rolle2.setTextFill(rolle.getFarbe());
            addWhiteShadow(rolle2);
        }
        //Überprüfung welche Rolle Spieler 3 hat und setzen der Rolle als Text sowie setzen der Textfarbe
        if (spielerNummer == 3) {
            rolle3.setText(rolle.getRollenname());
            rolle3.setTextFill(rolle.getFarbe());
            addWhiteShadow(rolle3);
        }

        // Überprüfung welche Rolle Spieler 4 hat und setzen der Rolle als Text sowie setzen der Textfarbe
        if (spielerNummer == 4) {
            rolle4.setText(rolle.getRollenname());
            rolle4.setTextFill(rolle.getFarbe());
            addWhiteShadow(rolle4);
        }
    }

    /**
     * Je nach dem wo sich der einzelne Spieler auf der Weltkarte befindet, soll der jeweilige Stadtname als Position gesetzt werden.
     *
     * @param spielerNummer - die Nummer des Spielers
     * @param stadt         - die Stadt, in der sich der jeweilige Spieler aufhält
     */
    public void setLabelPosition(int spielerNummer, Stadt stadt) {
        // Überprüfung, ob sich Spieler1 in einer Stadt befindet, die einen bestimmten ErregerTyp aufweist und setzen der jeweiligen Stadt als Position sowie setzen der ihm zugeordneten Textfarbe
        if (spielerNummer == 1) {
            pos1.setText(stadt.getName());
            pos1.setTextFill(stadt.getErreger().getFarbe());
            addWhiteShadow(pos1);
            // Überprüfung, ob sich Spieler2 in einer Stadt befindet, die einen bestimmten ErregerTyp aufweist und setzen der jeweiligen Stadt als Position sowie setzen der ihm zugeordneten Textfarbe
        } else if (spielerNummer == 2) {
            pos2.setText(stadt.getName());
            pos2.setTextFill(stadt.getErreger().getFarbe());
            addWhiteShadow(pos2);
            // Überprüfung, ob sich Spieler3 in einer Stadt befindet, die einen bestimmten ErregerTyp aufweist und setzen der jeweiligen Stadt als Position sowie setzen der ihm zugeordneten Textfarbe
        } else if (spielerNummer == 3) {
            pos3.setText(stadt.getName());
            pos3.setTextFill(stadt.getErreger().getFarbe());
            addWhiteShadow(pos3);
            // Überprüfung, ob sich Spieler4 in einer Stadt befindet, die einen bestimmten ErregerTyp aufweist und setzen der jeweiligen Stadt als Position sowie setzen der ihm zugeordneten Textfarbe
        } else if (spielerNummer == 4) {
            pos4.setText(stadt.getName());
            pos4.setTextFill(stadt.getErreger().getFarbe());
            addWhiteShadow(pos4);
        }
    }


    public void versteckeAktionen() {
        boxAktionen.setVisible(false);
    }


    public void aktualisiereKartenUndAktionsPanel(Spieler spieler) {
        loescheKartenUndAktionsPanel();
        aktualisiereKarten(spieler);
        aktualisiereAktionsPanel(spieler);
    }

    private void aktualisiereKarten(Spieler spieler) {
        // füge Überschrift hinzu
        setzeLabel(boxHandkarten, spieler.getName() + "'s Handkarten:");
        
        // füge Handkarten hinzu
        for (ISpielkarte karte : spieler.getHandkarten()) {
            Label lblKarte = new Label();
            addWhiteShadow(lblKarte);
            lblKarte.setTextFill(karte.getFarbe());
            lblKarte.setText(karte.getName());
            boxHandkarten.getChildren().add(lblKarte);
        }
        boxHandkarten.setVisible(true);
    }

    private void setzeLabel(VBox boxHandkarten, String string) {
        Label lblHandkarten = new Label();
        lblHandkarten.setText(string);
        lblHandkarten.setStyle(cssUeberSchrift);
        boxHandkarten.getChildren().add(lblHandkarten);
    }

    private void versteckeKarten(){
        // lösche Kartenpanel
        boxHandkarten.getChildren().removeAll(boxHandkarten.getChildren());
        // mache Panel unsichtbar
        boxHandkarten.setVisible(false);
    }

    private void ladeAktionsPanel(Spieler spieler){
        setzeLabel(boxAktionen, "Aktionspunkte: " +spieler.getAktionspunkte());

        if(spieler.getRolle().equals(Rolle.LOGISTIKER)){

        }
    };


    /**
    private void ladeAktionsPanelHandler(Spieler spieler) {
        untoggleToggles();

        btnAuto.setOnAction(event -> {

        });

        btnCharterflug.setOnAction(event -> {
            Stadt zielStadt = waehleBeliebigeStadt();
            spieler.selberOderAnderer(this).charterflug(zielStadt);
            checkZugBeendet(spieler);
        });
        btnDirektflug.setOnAction(event -> {
            System.out.println("Direktflug");
            zeigeDirektFlugMenue(spieler.selberOderAnderer(this));
        });
        btnZubringer.setOnAction(event -> {
            System.out.println("Zubringerflug");
            zeigeStadtMitLaborMenue(spieler.selberOderAnderer(this));
        });

        btnLabor.setOnAction(event -> {
            spieler.forschungsLaborErrichten();
            checkZugBeendet(spieler);
        });
        btnMittel.setOnAction(event -> {
            ErregerTyp gegenMittelErreger = waehleErreger();
            LinkedList<Stadt> gegegenMittelKarten = waehleStadtkarten(
                    gegenMittelErreger,
                    spieler.getRolle().equals(Rolle.FORSCHER) ? 4 : 5);
            spieler.gegenmittelEntdecken(gegenMittelErreger);
            checkZugBeendet();
        });
        btnSeucheBehandeln.setOnAction(event -> {
            if (spieler.getStadt().getZahlErregerAktiv() > 1) {
                ErregerTyp erregerZuBehandeln = waehleErreger();
            } else {
                ErregerTyp erregerZuBehandeln = spieler.getStadt().getAktiveErreger().get(0);
            }
            spieler.seucheBehandeln(erregerZuBehandeln);
            checkZugBeendet();
        });
        btnWissenTeilen.setOnAction(event -> {
            Stadt stadt = spieler.getStadt();
            if (nehmenGewaehlt()) {
                Spieler nehmenderSpieler = spieler;
                Spieler gebenderSpieler = waehleSpielerAus(stadt.getSpielerInStadt().remove(nehmenderSpieler));
            } else {
                Spieler gebenderSpieler = spieler;
                Spieler nehmenderSpieler = waehleSpielerAus(stadt.getSpielerInStadt().remove(gebenderSpieler));
            }
            Spieler nehmenderSpieler = waehleNehmendenSpielerAus(stadt.getSpielerInStadt());
            Spieler gebenderSpieler = waehleGebendenSpielerAus(stadt.getSpielerInStadt().remove(nehmenderSpieler));
            Stadt spielKarte = waehleTauschkarte(gebenderSpieler);
            gebenderSpieler.wissenTeilen(nehmenderSpieler, spielKarte, true);
            checkZugBeendet();
        });


    }
     **/
    private void checkZugBeendet(Spieler spieler) {
        if(spieler.getAktionspunkte()==0){
            for(Spieler spielerI: spiel.getSpielerListe()){spieler.setAktionspunkte(4);}
            beendeZug();
            System.out.println("checkZugBeendet ausgelöst");
        } else{
            aktualisiereKartenUndAktionsPanel(spieler);
        }
    }

    private void loescheKartenUndAktionsPanel() {
        boxHandkarten.getChildren().removeAll(boxHandkarten.getChildren());
        boxAktionen.getChildren().removeAll(boxAktionen.getChildren());
    }

    private void zeigeDirektFlugMenue(Spieler spieler) {
        int buttonSize = 150;
        versteckeKarten();
        setzeLabel(boxHandkarten, "Wählen Sie eine Stadt:");
        for(Stadt stadt: spieler.getStadtkarten()){
            Button btn = new Button(stadt.getName());
            btn.setPrefWidth(buttonSize);
            btn.setOnAction(event -> {
                spieler.direktflug(stadt);
                checkZugBeendet(spieler);
            });
            boxHandkarten.getChildren().add(btn);
        }
        boxHandkarten.setVisible(true);
    }

    private void zeigeStadtMitLaborMenue(Spieler spieler) {
        int buttonSize = 150;
        versteckeKarten();
        setzeLabel(boxHandkarten, "Wählen Sie ein Ziel mit Forschungslabor:");
        for(Stadt moeglicheZielStadt: Stadt.values()){
            if(moeglicheZielStadt.isHatForschungsLabor()){
                Stadt zielStadt = moeglicheZielStadt;
                Button btn = new Button(zielStadt.getName());
                btn.setPrefWidth(buttonSize);
                btn.setOnAction(event -> {
                    spieler.charterflug(zielStadt);
                    checkZugBeendet(spieler);
                });
                boxHandkarten.getChildren().add(btn);
            }
        }
    }

    private void zeigeNachbarStadtMenue(Spieler spieler){
        versteckeKarten();
        setzeLabel(boxHandkarten, "Wählen Sie eine");
        setzeLabel(boxHandkarten, "Nachbarstadt:");
        for(Stadt nachbarStadt: spieler.getStadt().getNachbarstaedte()){
            Button btn = new Button(nachbarStadt.getName());
            btn.setOnAction(event -> {
                System.out.println("Ziel: "+nachbarStadt);
                spieler.autofahrt(nachbarStadt);
                checkZugBeendet(spieler);
            });
            boxHandkarten.getChildren().add(btn);
        }
        boxHandkarten.setVisible(true);
    }

    private void aktualisiereAktionsPanel(Spieler spieler) {
        int buttonWidth = 150;

        boxAktionen.setVisible(true);
        setzeLabel(boxAktionen, "Aktionspunkte: "+spieler.getAktionspunkte());
        // Aktion Auto oder Fähre
        Button btnAuto = new Button();
        btnAuto.setText("Auto (oder Fähre)");
        btnAuto.setPrefWidth(buttonWidth);
        btnAuto.setOnAction(actionEvent -> {
            zeigeNachbarStadtMenue(spieler);
            System.out.println("Auto");
        });
        boxAktionen.getChildren().add(btnAuto);
        // Aktion Direktflug
        if(spieler.getStadtkarten().size()>0){
            Button btnDirekt = new Button();
            btnDirekt.setText("Direktflug");
            btnDirekt.setPrefWidth(buttonWidth);
            btnDirekt.setOnAction(actionEvent -> {
                zeigeDirektFlugMenue(spieler);
                System.out.println("Direktflug");
            });
            boxAktionen.getChildren().add(btnDirekt);
        }
        // Aktion Charterflug
        if(spieler.getStadtkarten().contains(spieler.getStadt())){
            Button btnCharter = new Button();
            btnCharter.setText("Charterflug");
            btnCharter.setPrefWidth(buttonWidth);
            btnCharter.setOnAction(actionEvent -> {
                //macheAlleStaedteAusserDerEigenenAnklickbar(spieler.getStadt());
                System.out.println("Charterflug");
            });
            boxAktionen.getChildren().add(btnCharter);
        }
        // Aktion Zubringerflug
        if(Stadt.getZahlForschungslabore() > 1 && spieler.getStadt().isHatForschungsLabor()){
            Button btnZubringer = new Button();
            btnZubringer.setText("Zubringerflug");
            btnZubringer.setPrefWidth(buttonWidth);
            btnZubringer.setOnAction(actionEvent -> {
                zeigeStadtMitLaborMenue(spieler);
                System.out.println("Zubringerflug");
            });
            boxAktionen.getChildren().add(btnZubringer);
        }
        //Aktion Forschungslabor errichten
        if(!spieler.getStadt().isHatForschungsLabor() && spieler.getStadtkarten().contains(spieler.getStadt())){
            Button btnLabErrichten = new Button();
            btnLabErrichten.setText("Forschungslabor errichten");
            btnLabErrichten.setPrefWidth(buttonWidth);
            btnLabErrichten.setOnAction(actionEvent -> {
                System.out.println("Forschungslabor errichten");
            });
            boxAktionen.getChildren().add(btnLabErrichten);
        }
        //Gegenmittel Entdecken
        if(spieler.getStadt().isHatForschungsLabor()
                && (spieler.hatMindKartenVon(ErregerTyp.BLAU)
                    || spieler.hatMindKartenVon(ErregerTyp.ROT)
                    || spieler.hatMindKartenVon(ErregerTyp.GELB)
                    || spieler.hatMindKartenVon(ErregerTyp.SCHWARZ))){
            Button btnGegenmittel = new Button();
            btnGegenmittel.setText("Gegenmittel entdecken");
            btnGegenmittel.setPrefWidth(buttonWidth);
            btnGegenmittel.setOnAction(actionEvent -> {
                System.out.println("Gegenmittel entdecken");
            });
            boxAktionen.getChildren().add(btnGegenmittel);
        }
        //Seuche behandeln
        if(spieler.getStadt().getZahlAktiveErreger()>0){
            Button btnSeucheBeh = new Button();
            btnSeucheBeh.setText("Seuche behandeln");
            btnSeucheBeh.setPrefWidth(buttonWidth);
            btnSeucheBeh.setOnAction(actionEvent -> {
                System.out.println("Seuche behandeln");
            });
            boxAktionen.getChildren().add(btnSeucheBeh);
        }
        //Wissen teilen
        if(spieler.isAndererMitspielerDa(spiel.getSpielerListe())
                && existiertSpielerMitPassenderKarte(spieler, spiel.getSpielerListe())){
            Button btnWissenTeilen = new Button();
            btnWissenTeilen.setText("Wissen teilen");
            btnWissenTeilen.setPrefWidth(buttonWidth);
            btnWissenTeilen.setOnAction(actionEvent -> {
                System.out.println("Wissen teilen");
            });
            boxAktionen.getChildren().add(btnWissenTeilen);
        }
    }

    private boolean existiertSpielerMitPassenderKarte(Spieler spieler, ArrayList<Spieler> spielerListe) {
        boolean result = false;
        // result = true zuweisen, wenn irgendein spieler in der Stadt die Spielkarte dieser Stadt hat
        for(Spieler spieler2: spielerListe)
            if(spieler2.getStadt().equals(spieler.getStadt())){
                if(spieler2.getHandkarten().contains(spieler.getStadt())){
                    result = true;
                }
            }

        // result = true zuweisen, wenn ein Forscher da ist und Karten auf der Hand hat
        for(Spieler spieler2: spielerListe){
            if(spieler2.getStadt().equals(spieler.getStadt())
                    && spieler2.getRolle().equals(Rolle.FORSCHER)
                    && spieler2.getStadtkarten().size()>0){
                result = true;
            }
        }

        return result;
    }

    public Spieler getLogistikerAuswahl() {
        Object toggleAuswahl = toggleGroup.getUserData();
        if (toggleAuswahl == null) {
            return getSpieler(Rolle.LOGISTIKER);
        } else {
            return getSpieler((Rolle) toggleAuswahl);
        }
    }

    private Spieler getSpieler(Rolle rolle) {
        Spieler result = null;
        for (Spieler spieler : spiel.getSpielerListe()) {
            if (spieler.getRolle().equals(rolle)) {
                result = spieler;
            }
        }
        if (result==null){
            System.err.println("Fehler in SpielplanController .getSpieler(), Rolle nicht vorhanden.");
        }
        return result;
    }
}
