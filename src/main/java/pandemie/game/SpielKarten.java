package pandemie.game;

import java.util.LinkedList;
import java.util.Stack;

public class SpielKarten extends KartenStapel {

    boolean ereignisKarte;
    boolean pandemieKarte;

    /**
     * @param stapel
     * @param ablageStapel
     * @param ereignisKarte
     * @param pandemieKarte
     */
    public SpielKarten(LinkedList<KartenStapel> stapel, Stack<KartenStapel> ablageStapel, boolean ereignisKarte, boolean pandemieKarte) {

        super(stapel, ablageStapel);
        this.ereignisKarte = ereignisKarte;
        this.pandemieKarte = pandemieKarte;
    }

    /**
     * @return
     */
    public boolean isEreignisKarte() {
        return ereignisKarte;
    }

    /**
     * @param ereignisKarte
     */
    public void setEreignisKarte(boolean ereignisKarte) {

        this.ereignisKarte = ereignisKarte;

        if(ereignisKarte){

            this.pandemieKarte = !ereignisKarte;
        }
    }

    /**
     * @return
     */
    public boolean isPandemieKarte() {
        return pandemieKarte;
    }

    /**
     * @param pandemieKarte
     */
    public void setPandemieKarte(boolean pandemieKarte) {

        this.pandemieKarte = pandemieKarte;
    }
}
