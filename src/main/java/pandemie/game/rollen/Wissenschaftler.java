package pandemie.game.rollen;

import pandemie.game.spielablauf.Spieler;
import pandemie.game.spielkarten.Stadt;

/**
 * Wissenschaftler: Bei der Aktion Gegenmittel entdecken benötigt er nur 4 statt 5 Karten einer Farbe.
 */
public class Wissenschaftler implements IRollenFaehigkeit {

    private final String NAME = "Wissenschaftler";
    private static boolean instanzErzeugt = false;

    /**
     *
     */
    private Wissenschaftler() { }


    /**
     * Kontrolle, ob eine instanz schon erzeugt wurde. Denn 1 Beruf kann nur 1-mal vorkommen.
     * @return gibt ein erzeugtes Objekt vom typ Wissenschaftler zurueck
     */
    static public Wissenschaftler erzeugeInstanz() {

        Wissenschaftler neuerWissenschaftler = new Wissenschaftler();

        if (instanzErzeugt) {
            neuerWissenschaftler = null; //Zerstörung des Objektes
            System.err.println("Zu viele Wissenschafler");
        } else {
            instanzErzeugt = true; //
        }
        return neuerWissenschaftler;
    }
}




