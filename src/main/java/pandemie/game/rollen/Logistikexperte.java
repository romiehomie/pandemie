package pandemie.game.rollen;

import pandemie.game.spielablauf.Spieler;
import pandemie.game.spielkarten.Stadt;

/**
 * Logistikexperte: Sie können einfache Aktionen in Ihrem Zug auch mit Figuren der anderen durchführen &
 * Sie können für eine Aktion eine beliebige Spielfigur in eine Stadt bewegen,
 * in der sich bereits eine Spielfigur befindet
 */
public class Logistikexperte implements IRollenFaehigkeit {

    private final String NAME = "Logistiker";
    private static boolean instanzErzeugt = false;

    /**
     *
     */
    private Logistikexperte() { }


    /**
     * Kontrolle, ob eine instanz schon erzeugt wurde. Denn 1 Beruf kann nur 1-mal vorkommen.
     * @return gibt ein erzeugtes Objekt vom typ Logistikexperte zurueck
     */
    static public Logistikexperte erzeugeInstanz() {

        Logistikexperte neuerLogistikexperte = new Logistikexperte();

        if (instanzErzeugt) {
            neuerLogistikexperte = null; //Zerstörung des Objektes
            System.err.println("Zu viele Logistikexperten.");
        } else {
            instanzErzeugt = true; //
        }
        return neuerLogistikexperte;
    }
}
