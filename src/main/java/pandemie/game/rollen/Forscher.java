package pandemie.game.rollen;

import pandemie.game.spielablauf.Spieler;
import pandemie.game.spielkarten.Stadt;

/**
 * Forscherin: Bei der Aktion Wissen teilen kann die Karte einer beliebigen Stadt weitergegeben werden
 * (auch im Zug eines Mitspielers).
 */
public class Forscher implements IRollenFaehigkeit {

    private final String NAME = "Forscher";
    private static boolean instanzErzeugt = false;

    /**
     *
     */
    private Forscher() { }

    /**
     * Kontrolle, ob eine instanz schon erzeugt wurde. Denn 1 Beruf kann nur 1-mal vorkommen.
     * @return gibt ein erzeugtes Objekt vom typ Forscher zurueck
     */
    static public Forscher erzeugeInstanz() {

        Forscher neuerForscher = new Forscher();

        if (instanzErzeugt) {
            neuerForscher = null; //Zerstörung des Objektes
            System.err.println("Zu viele Forscher.");
        } else {
            instanzErzeugt = true; //
        }
        return neuerForscher;
    }
}
