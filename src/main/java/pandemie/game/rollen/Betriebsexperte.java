package pandemie.game.rollen;

import pandemie.game.spielablauf.Spieler;
import pandemie.game.spielkarten.Stadt;

/**
 * Betriebsexperte: muss bei Forschungslabor errichten keine Karte ablegen
 */
public class Betriebsexperte implements IRollenFaehigkeit {

    private final String NAME = "Betriebsexperte";
    private static boolean instanzErzeugt = false;

    /**
     *
     */
    private Betriebsexperte() {
    }


    /**
     * Kontrolle, ob eine instanz schon erzeugt wurde. Denn 1 Beruf kann nur 1-mal vorkommen.
     * @return gibt ein erzeugtes Objekt vom typ Betriebsexperte zurueck
     */
    static public Betriebsexperte erzeugeInstanz() {

        Betriebsexperte neuerBetriebsexperte = new Betriebsexperte();

        if (instanzErzeugt) {
            neuerBetriebsexperte = null; //Zerstörung des Objektes
            System.err.println("Zu viele Betriebsexperten.");
        } else {
            instanzErzeugt = true; //
        }
        return neuerBetriebsexperte;
    }
}
