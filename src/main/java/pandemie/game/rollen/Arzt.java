package pandemie.game.rollen;

import pandemie.game.spielablauf.Spieler;
import pandemie.game.spielkarten.Stadt;

/**
 * Arzt: Bei der Aktion Seuche behandeln entfernt er alle Würfel einer Farbe,
 * wurde das Gegenmittel bereits entdeckt, entfernt er alle Würfel ohne Aktion dafür ausgeben zu müssen
 * (auch wenn er durch Logistikexperte bewegt wird)
 */
public class Arzt implements IRollenFaehigkeit {

    private String name = "Arzt";
    private static boolean instanzErzeugt = false;

    /**
     * Standartkonstruktor von der Klasse "Arzt"
     */
    private Arzt() {
    }


    /**
     * Kontrolle, ob eine instanz schon erzeugt wurde. Denn 1 Beruf kann nur 1-mal vorkommen.
     * @return gibt ein erzeugtes Objekt vom typ Arzt zurueck
     */
    static public Arzt erzeugeInstanz() {

        Arzt neuerArzt = new Arzt();

        if (instanzErzeugt) {
            neuerArzt = null; //Zerstörung des Objektes
            System.err.println("Zu viele Ärzte.");
        } else {
            instanzErzeugt = true; //
        }
        return neuerArzt;
    }

}
