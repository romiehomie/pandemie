package pandemie.game.rollen;

import javafx.scene.paint.Color;
import pandemie.game.rollen.*;

public enum Rolle {


    LOGISTIKER("(Logistikexperte)", Color.PURPLE),
    WISSENSCHAFTLER("(Wissenschaftler)", Color.grayRgb(55)),
    FORSCHER("(Forscher)", Color.BROWN),
    BETRIEBSEXPERTE("(Betriebsexperte)", Color.DARKGREEN),
    ARZT("(Arzt)", Color.rgb(155,100,0));

    private String rollenname; // Rolle = beruf
    private Color farbe;
    private IRollenFaehigkeit rollenFaehigkeit;

    /**
     * @param rollenname
     * @param farbe
     */
    private Rolle(String rollenname, Color farbe) {
        if(rollenname.equals("(Logistikexperte)")){
            this.rollenFaehigkeit = Logistikexperte.erzeugeInstanz();
        } else if(rollenname.equals("(Wissenschaftler)")){
            this.rollenFaehigkeit = Wissenschaftler.erzeugeInstanz();
        } else if(rollenname.equals("(Forscher)")){
            this.rollenFaehigkeit = Forscher.erzeugeInstanz();
        } else if(rollenname.equals("(Betriebsexperte)")){
            this.rollenFaehigkeit = Betriebsexperte.erzeugeInstanz();
        } else if(rollenname.equals("(Arzt)")){
            this.rollenFaehigkeit = Arzt.erzeugeInstanz();
        }
        this.farbe = farbe;
        this.rollenname = rollenname;
    }

    /**
     * @return
     */
    public Color getFarbe() {
        return farbe;
    }


    /**
     * @return
     */
    public String getRollenname() {
        return rollenname;
    }

}