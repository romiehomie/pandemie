package pandemie.game;

import java.util.*;

/**
 * Es ist eine Klasse, die ein Kartenstapel und ein Ablagestapel im Spiel als LinkedList und Stack initialisiert
 */
public class KartenStapel {

    private LinkedList<KartenStapel> stapel = new LinkedList<KartenStapel>(); // ein Kartenstapel im Spiel

    private Stack<KartenStapel> ablageStapel = new Stack<KartenStapel>(); // ein Ablagestapel fuer Karten im Spiel


    /**
     * Ist der Konstruktor von der Klasse "KartenStapel"
     * @param stapel ist ein Container (LinkedList, vom Typ "Kartenstapel"), der als ein Kartenstapel im Spiel dient
     * @param ablageStapel ist ein Container (Stack, vom Typ "Kartenstapel"), der als ein Ablagestapel im Spiel dient
     */
    public KartenStapel(LinkedList<KartenStapel> stapel, Stack<KartenStapel> ablageStapel) {

        this.stapel = stapel;
        this.ablageStapel = ablageStapel;
    }

    /**
     * Getter Metode fuer die Instanzvariable "stapel"
     * @return ist ein erzeugter Kartenstapel als Rueckgabewert
     */
    public LinkedList<KartenStapel> getStapel() {
        return stapel;
    }

    /**
     * Setter Metode fuer die Instanzvariable "stapel"
     * @param stapel ist ein Kartenstapel im Spiel (LinkedList, vom Typ "KartenStapel"), den die Instanzvariable "stapel" symbolisiert
     */
    public void setStapel(LinkedList<KartenStapel> stapel) {
        this.stapel = stapel;
    }

    /**
     * Getter Metode fuer die Instanzvariable "ablageStapel"
     * @return ist ein Ablagestapel (Stack, vom Typ "KartenStapel")
     */
    public Stack<KartenStapel> getAblageStapel() {
        return ablageStapel;
    }

    /**
     * Setter Metode fuer die Instanzvariable "ablageStapel"
     * @param ablageStapel ist ein Ablagestapelim Spiel (Stack, vom Typ "KartenStapel"), den die Instanzvariable "ablageStapel" symbolissiert
     */
    public void setAblageStapel(Stack<KartenStapel> ablageStapel) {
        this.ablageStapel = ablageStapel;
    }

    /**
     * Getter Metode fuer die erste Karte aus einem Kartenstapel
     * @return ist eine aus sem Kartenstapel (Instanzvariable "stapel") von unten gezogene Karte
     */
    public KartenStapel eineKarteVonUntenziehen(){

        return stapel.pollFirst();
    }

    /**
     * Getter Metode fuer die letzte Karte aus einem Kartenstapel
     * @return ist eine aus sem Kartenstapel (Instanzvariable "stapel") von oben gezogene Karte
     */
    public KartenStapel eineKarteVonObenZiehen(){

        return stapel.pollLast();
    }

}
