package pandemie.game.spielkarten;

import javafx.scene.paint.Color;
import pandemie.game.ErregerTyp;
import pandemie.game.spielablauf.Spieler;

import java.util.*;


/**
 * Enem Stadt entspricht den 48 Stadtkarten, bzw. Infektionskarten
 */
public enum Stadt implements ISpielkarte {


    /**
     * Städte mit dem Erregertyp "BLAU"
     */
    SAN_FRANCISCO("San Francisco", false, ErregerTyp.BLAU),
    CHICAGO("Chicago", false, ErregerTyp.BLAU),
    TORONTO("Toronto", false, ErregerTyp.BLAU),
    NEW_YORK("New York", false, ErregerTyp.BLAU),
    ATLANTA("Atlanta", true, ErregerTyp.BLAU), // Am Anfang des Spiels besitzt ein Forschungslabor
    WASHINGTON("Washington", false, ErregerTyp.BLAU),
    LONDON("London", false, ErregerTyp.BLAU),
    ESSEN("Essen", false, ErregerTyp.BLAU),
    PARIS("Paris", false, ErregerTyp.BLAU),
    MADRID("Madrid", false, ErregerTyp.BLAU),
    MAILAND("Mailand", false, ErregerTyp.BLAU),
    ST_PETERSBURG("St. Petersburg", false, ErregerTyp.BLAU),


    /**
     * Städte mit dem Erregertyp "GELB"
     */
    LOS_ANGELES("Los Angeles", false, ErregerTyp.GELB),
    MEXICO_STADT("Mexico-Stadt", false, ErregerTyp.GELB),
    MIAMI("Miami", false, ErregerTyp.GELB),
    BOGOTA("Bogota", false, ErregerTyp.GELB),
    LIMA("Lima", false, ErregerTyp.GELB),
    SANTIAGO("Santiago", false, ErregerTyp.GELB),
    SAN_PAULO("São Paulo", false, ErregerTyp.GELB),
    BUENOS_AIRES("Buones Aires", false, ErregerTyp.GELB),
    LAGOS("Lagos", false, ErregerTyp.GELB),
    KHARTUM("Khartum", false, ErregerTyp.GELB),
    KINSHASA("Kinshasa", false, ErregerTyp.GELB),
    JOHANNESBURG("Johannesburg", false, ErregerTyp.GELB),


    /**
     * Städte mit dem Erregertyp "SCHWARZ"
     */
    MOSKAU("Moskau", false, ErregerTyp.SCHWARZ),
    ISTANBUL("Istanbul", false, ErregerTyp.SCHWARZ),
    ALGIER("Algier", false, ErregerTyp.SCHWARZ),
    KAIRO("Kairo", false, ErregerTyp.SCHWARZ),
    RIAD("Riad", false, ErregerTyp.SCHWARZ),
    BAGDAD("Bagdad", false, ErregerTyp.SCHWARZ),
    TEHERAN("Teheran", false, ErregerTyp.SCHWARZ),
    KARATSCHI("Karatschi", false, ErregerTyp.SCHWARZ),
    DELHI("Delhi", false, ErregerTyp.SCHWARZ),
    MUMBAI("Mumbai", false, ErregerTyp.SCHWARZ),
    KALKUTTA("Kalkutta", false, ErregerTyp.SCHWARZ),
    CHENNAI("Chennai", false, ErregerTyp.SCHWARZ),


    /**
     * Städte mit dem Erregertyp "ROT"
     */
    PEKING("Peking", false, ErregerTyp.ROT),
    SEOUL("Seoul", false, ErregerTyp.ROT),
    SHANGHAI("Shanghai", false, ErregerTyp.ROT),
    TOKIO("Tokio", false, ErregerTyp.ROT),
    OSAKA("Osaka", false, ErregerTyp.ROT),
    HONGKONG("Hongkong", false, ErregerTyp.ROT),
    TAIPEH("Taipeh", false, ErregerTyp.ROT),
    BANGKOK("Bangkok", false, ErregerTyp.ROT),
    HO_CHI_MINH_STADT("Ho-Chi-Minh-Stadt", false, ErregerTyp.ROT),
    JAKARTA("Jakarta", false, ErregerTyp.ROT),
    SYDNEY("Sydney", false, ErregerTyp.ROT),
    MANILA("Manila", false, ErregerTyp.ROT);


    private String name; // Instanzvariable, der neme einer Stadt
    private boolean hatForschungsLabor; // Instanzvariable, bei "true" ist ein forschungslabor in der Stadt vorhanden, bei "false" nicht
    private ErregerTyp erreger; // hashtable erregertyp, Erregertyp, der einer Stadt zugewiesen wird.
    private double posY; // Instanzvariable
    private double posX; // Instanzvariable,
    private double symbolX; // Instanzvariable, X-Koordinaten des Symbols der Stadt
    private double symbolY; // Instanzvariable
    private ArrayList<Stadt> nachbarstaedte; // Instanzvariable, Liste von nacbarstaedten einer Stadt
    private HashMap<ErregerTyp, Integer> infektionsSkalen; // Instanzvariable


    /**
     * @param name
     * @param hatForschungsLabor
     * @param erreger
     */
    Stadt(String name, boolean hatForschungsLabor, ErregerTyp erreger) {
        this.name = name;
        this.hatForschungsLabor = hatForschungsLabor;
        this.erreger = erreger;
        infektionsSkalen = new HashMap<>();
        infektionsSkalen.put(ErregerTyp.ROT, 0);
        infektionsSkalen.put(ErregerTyp.GELB, 0);
        infektionsSkalen.put(ErregerTyp.SCHWARZ, 0);
        infektionsSkalen.put(ErregerTyp.BLAU, 0);
        nachbarstaedte = new ArrayList<>(); // Arraylist mit Nacbarstaedten
    }

    public static int getZahlForschungslabore() {
        int result=0;
        for(Stadt stadt: Stadt.values()){
            if(stadt.isHatForschungsLabor()){
                result++;
            }
        }
        return result;
    }

    /**
     * Setermetode fue die Instanzvariable "name"
     *
     * @param name ist der Name einer Stadt
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return
     */
    public String getName() {
        return name;
    }

    @Override
    public Color getFarbe() {
        return getErreger().getFarbe();
    }


    /**
     * Setermetode fue die Instanzvariable "hatForschungsLabor"
     *
     * @param hatForschungsLabor bei "true" ist ein Forschungslabor vorhanden, bei "false" nicht
     */
    public void setHatForschungsLabor(boolean hatForschungsLabor) {
        this.hatForschungsLabor = hatForschungsLabor;
    }

    /**
     * Getermetode fuer die Instanzvariable "hatForschungsLabor"
     *
     * @return ist der Rueckgabewert fuer hatForschungsLabor: bei "true" ist ein Forschungslabor vorhanden, bei "false" nicht
     */
    public boolean isHatForschungsLabor() {
        return hatForschungsLabor;
    }


    /**
     * Getermetode fuer die Instanzvariable "erreger"
     *
     * @return ist der Rueckgabewert fuer die Instanzvariable erreger (Erregertyp):
     */
    public ErregerTyp getErreger() {
        return erreger;
    }

    /**
     * Setermetode fue die Instanzvariable "erreger"
     *
     * @param erreger
     */
    public void setErreger(ErregerTyp erreger) {
        this.erreger = erreger;
    }

    /**
     * Getermetode fuer die Instanzvariable "nachbarstaedte"
     *
     * @return ist der Rueckgabewert fuer die Instanzvariable nachbarstaedte (die Liste der Nachbarstaedte einer Stadt):
     */
    public ArrayList<Stadt> getNachbarstaedte() {
        return nachbarstaedte;
    }

    /**
     * Setermetode fue die Instanzvariable "nachbarstaedte"
     *
     * @param nachbarstaedte
     */
    public void setNachbarstaedte(ArrayList<Stadt> nachbarstaedte) {
        this.nachbarstaedte = nachbarstaedte;
    }

    /**
     * Getermetode fuer die Instanzvariable "infektionsSkalen"
     *
     * @return ist der Rueckgabewert fuer die Instanzvariable infektionsSkalen (Durchseuchungsgrad, Anzahl der Wuerfel eines Erregetueps):
     */
    public HashMap<ErregerTyp, Integer> getInfektionsSkalen() {
        return infektionsSkalen;
    }


    /**
     * Getermetode fuer die Instanzvariable "posX"
     *
     * @return ist der Rueckgabewert fuer die Instanzvariable posX ()
     */
    public double getPosX() {
        return posX;
    }

    /**
     * Setermetode fue die Instanzvariable "posX"
     *
     * @param posX
     */
    public void setPosX(double posX) {
        this.posX = posX;
    }

    /**
     * Getermetode fuer die Instanzvariable "posY"
     *
     * @return ist der Rueckgabewert fuer die Instanzvariable posY ()
     */
    public double getPosY() {
        return posY;
    }

    /**
     * Setermetode fue die Instanzvariable "posY"
     *
     * @param posY
     */
    public void setPosY(double posY) {
        this.posY = posY;
    }

    /**
     * Getermetode fuer die Instanzvariable "symbolX"
     *
     * @return ist der Rueckgabewert fuer die Instanzvariable symbolX ()
     */
    public double getSymbolX() {
        return symbolX;
    }

    /**
     * Setermetode fue die Instanzvariable "symbolX"
     *
     * @param symbolX
     */
    public void setSymbolX(double symbolX) {
        this.symbolX = symbolX;
    }

    /**
     * Getermetode fuer die Instanzvariable "symbolY"
     *
     * @return ist der Rueckgabewert fuer die Instanzvariable symbolY ()
     */
    public double getSymbolY() {
        return symbolY;
    }

    /**
     * Setermetode fue die Instanzvariable "symbolY"
     *
     * @param symbolY
     */
    public void setSymbolY(double symbolY) {
        this.symbolY = symbolY;
    }

    /**
     * @param stadt2
     * @return
     */
    public double distanceTo(Stadt stadt2) {

        double deltaX = (this.getSymbolX() - stadt2.getSymbolX());
        double deltaY = (this.getSymbolY() - stadt2.getSymbolY());
        return Math.sqrt(deltaX * deltaX + deltaY * deltaY);
    }

    /**
     * @param erreger       ist die Instanzvariable vom Typ "Erreger" (also ein Erregertyp)
     * @param wuerfelanzahl ist die Infektionsskala () eines Erregertyps
     */
    public void setSkala(ErregerTyp erreger, int wuerfelanzahl) {

        this.infektionsSkalen.put(erreger, wuerfelanzahl);
    }

    /**
     * @param erreger ist die Instanzvariable vom Typ "Erreger" (also ein Erregertyp)
     * @return ist der Rueckgabewert fuer die Instanzvariable wuerfelanzahl (Anzahl der Wuerfel eines bestimmten Erregertyps)
     */
    public int getSkala(ErregerTyp erreger) {

        int wuerfelanzahl = this.infektionsSkalen.get(erreger);
        return wuerfelanzahl;
    }


    public int getZahlAktiveErreger() {
        int result = 0;
        for(ErregerTyp erreger: this.getInfektionsSkalen().keySet()){
            if(infektionsSkalen.get(erreger)>0) result++;
        }
        return result;
    }
}
