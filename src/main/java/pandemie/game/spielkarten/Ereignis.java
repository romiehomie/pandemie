package pandemie.game.spielkarten;

import pandemie.game.spielablauf.Spiel;
import pandemie.game.spielablauf.Spieler;

/**
 * Die Klasse Ereignis ist ststisch und enthaelt funktionen, die die Ereignisse beschreiben, die
 * waehrend des Spiels auftreten koennen. Die Ereignisskarten sind in dem enum "Ereigniskarte enthalten".
 */
public class Ereignis {

    /**
     * Luftbrücke: Ziehen Sie eine beliebige Spielfigur in eine beliebige Stadt.
     *
     * @param sp ist ein Objekt der Klasse Spieler
     * @param stadt ist ein Objekt der Klasse Stadt
     * Spieelfigur spfg in die Stadt stadt verschoben
     */
    public static void luftbruecke (Spieler sp, Stadt stadt){

        sp.setStadt(stadt);
    }

    public static void prognose (){

    }

    public static void eineRuhigeNacht (){

    }

    /**
     * Regierungssubventionen: Errichten sie in einer beliebigen Stadt ein Forschungslabor
     *
     * @param stadt ist ein Objekt der Klasse Stadt
     * in der Stadt stadt wird ein Forschungslabor errichtet (setHatForschungsLabor = true)
     */
    public static void regierungsSubvention (Stadt stadt){

        stadt.setHatForschungsLabor(true);
    }

    public static void zaeheBevoelkerung (){

    }

    public static void epidemieEreignis(Spiel spiel){
        spiel.inkrementInfektionsRate();
        spiel.inkrementNummerSpielerDran();
    }

}
