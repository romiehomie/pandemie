package pandemie.game.spielkarten;

import javafx.scene.paint.Color;

public enum Ereigniskarte implements ISpielkarte {

    LUFTBRUECKE("Luftbruecke"),
    PROGNOSE("Prognose"),
    EINE_RUHIGE_NACHT("Eine ruhige Nacht"),
    REGIERUNGSSUBVENTIONEN("Regierungssubventionen"),
    ZAEHE_BEVOELKERUNG("Zähe Bevoelkerung");

    private String ereignisname;

    /**
     * @param ereignisname Bezeichnung des aktuellen Ereignisses
     */
    private Ereigniskarte(String ereignisname) {
        this.ereignisname = ereignisname;
    }

    @Override
    public String getName() {
        return ereignisname;
    }

    @Override
    public Color getFarbe() {
        return Color.ORANGE;
    }
}
