package pandemie.game.spielkarten;

import javafx.scene.paint.Color;

public interface ISpielkarte {
    String getName();

    Color getFarbe();
}
