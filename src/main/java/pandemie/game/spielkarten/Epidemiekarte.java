package pandemie.game.spielkarten;

import javafx.scene.paint.Color;

public class Epidemiekarte implements ISpielkarte {


    private static int anzahlDerEpidemiekarten = 0;


    /**
     * 4 Epidemiekarten -> Anfängerspiel
     * 5 Epidemiekarten -> Normales Spiel
     * 6 Epidemiekarten -> Heldenspiel
     */
    public static int maxEpidemieKartenzahl = 6; // =5; / =4;

    /**
     *
     */
    private Epidemiekarte() {
    }


    /**
     * @return gibt ein erzeugtes Objekt vom typ Arzt zurueck
     */
    public static Epidemiekarte erzeugeInstanz() {

        Epidemiekarte neueEpidemiekarte = new Epidemiekarte();

        if (anzahlDerEpidemiekarten > maxEpidemieKartenzahl) {
            neueEpidemiekarte = null; //Zerstörung des Objektes
            System.err.println("Zu viele User");
        } else {
            System.out.println("Neuer Benutzer angelegt.");
            anzahlDerEpidemiekarten = anzahlDerEpidemiekarten + 1; //Hochzählen der Variablen
        }
        return neueEpidemiekarte;
    }

    @Override
    public String getName() {
        return "Pandemie!";
    }

    @Override
    public Color getFarbe() {
        return Color.YELLOWGREEN;
    }

}
