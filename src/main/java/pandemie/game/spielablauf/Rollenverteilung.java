package pandemie.game.spielablauf;

import pandemie.game.rollen.Rolle;

import java.util.*;

public class Rollenverteilung {
    /**
     * @param spieleranzahl

     */
    public static ArrayList<Spieler> loadRandom(int spieleranzahl, String[] spielerNamen) {

        List<Rolle> rollenList = Arrays.asList(Rolle.values());
        Collections.shuffle(rollenList);

        ArrayList<Spieler> spielerArrayList = new ArrayList<>();
        for(int i=0; i< spieleranzahl; i++) {
            spielerArrayList.add(new Spieler(rollenList.get(i), spielerNamen[i]));
         }
        /**
        ArrayList<Spieler> spielerArrayList = new ArrayList<>();

        int anzahlDerRollen = Rolle.values().length;

        Random rnd = new Random();

        int[] zahlenarray = new int[spieleranzahl];

        for (int ii = 0; ii < spieleranzahl; ii++) {
            zahlenarray[ii] = 0;
        }

        boolean istEnthalten;
        int zufallsZahl;
        int jj = 0;
        while (jj < spieleranzahl) {

            zufallsZahl = rnd.nextInt(anzahlDerRollen) + 1;
            istEnthalten = false;

            for (int ii = 0; ii < spieleranzahl; ii++) {

                if (zufallsZahl == zahlenarray[ii]) {

                    istEnthalten = true;
                }
            }

            if (!istEnthalten) {

                zahlenarray[jj] = zufallsZahl;
                jj++;
            }
        }

        for (int ii = 0; ii < spieleranzahl; ii++) {

            switch (zahlenarray[ii]) {

                case 1:
                    Spieler spielerArzt = new Spieler(Rolle.ARZT);
                    spielerArrayList.add(spielerArzt);
                    break;

                case 2:
                    Spieler spielerForscher = new Spieler(Rolle.FORSCHER);
                    spielerArrayList.add(spielerForscher);
                    break;

                case 3:
                    Spieler spielerBetriebsexperte = new Spieler(Rolle.BETRIEBSEXPERTE);
                    spielerArrayList.add(spielerBetriebsexperte);
                    break;

                case 4:
                    Spieler spielerLogistikexperte = new Spieler(Rolle.LOGISTIKER);
                    spielerArrayList.add(spielerLogistikexperte);
                    break;

                case 5:
                    Spieler spielerWissenschaftler = new Spieler(Rolle.WISSENSCHAFTLER);
                    spielerArrayList.add(spielerWissenschaftler);
                    break;

                default:
                    spielerArrayList = null;
            }

        }
        **/
        return spielerArrayList;
    }
}
