package pandemie.game.spielablauf;

import pandemie.game.spielkarten.Stadt;

import java.util.ArrayList;

public class Nachbarstaedte {

    /**
     * Fuer jede einzelne Stadt eine Liste Nachbarstädten erzeugen und initialisisren:
     */

    public static void load() {

        //----------------------------------------------------------------------------
        ArrayList<Stadt> nachbarstaedteSanFrancisco = new ArrayList<Stadt>();

        nachbarstaedteSanFrancisco.add(Stadt.TOKIO);
        nachbarstaedteSanFrancisco.add(Stadt.MANILA);
        nachbarstaedteSanFrancisco.add(Stadt.CHICAGO);
        nachbarstaedteSanFrancisco.add(Stadt.LOS_ANGELES);

        Stadt.SAN_FRANCISCO.setNachbarstaedte(nachbarstaedteSanFrancisco);

        //----------------------------------------------------------------------------

        ArrayList<Stadt> nachbarstaedteChikago = new ArrayList<Stadt>();

        nachbarstaedteChikago.add(Stadt.SAN_FRANCISCO);
        nachbarstaedteChikago.add(Stadt.LOS_ANGELES);
        nachbarstaedteChikago.add(Stadt.MEXICO_STADT);
        nachbarstaedteChikago.add(Stadt.TORONTO);
        nachbarstaedteChikago.add(Stadt.ATLANTA);

        Stadt.CHICAGO.setNachbarstaedte(nachbarstaedteChikago);

        //----------------------------------------------------------------------------


        ArrayList<Stadt> nachbarstaedteToronto = new ArrayList<Stadt>();

        nachbarstaedteToronto.add(Stadt.CHICAGO);
        nachbarstaedteToronto.add(Stadt.WASHINGTON);
        nachbarstaedteToronto.add(Stadt.NEW_YORK);

        Stadt.TORONTO.setNachbarstaedte(nachbarstaedteToronto);

        //----------------------------------------------------------------------------

        ArrayList<Stadt> nachbarstaedteNewYork = new ArrayList<Stadt>();

        nachbarstaedteNewYork.add(Stadt.TORONTO);
        nachbarstaedteNewYork.add(Stadt.WASHINGTON);
        nachbarstaedteNewYork.add(Stadt.LONDON);
        nachbarstaedteNewYork.add(Stadt.MADRID);

        Stadt.NEW_YORK.setNachbarstaedte(nachbarstaedteNewYork);

        //----------------------------------------------------------------------------

        ArrayList<Stadt> nachbarstaedteAtlanta = new ArrayList<Stadt>();

        nachbarstaedteAtlanta.add(Stadt.CHICAGO);
        nachbarstaedteAtlanta.add(Stadt.WASHINGTON);
        nachbarstaedteAtlanta.add(Stadt.MIAMI);

        Stadt.ATLANTA.setNachbarstaedte(nachbarstaedteAtlanta);

        //----------------------------------------------------------------------------

        ArrayList<Stadt> nachbarstaedteWashington = new ArrayList<Stadt>();

        nachbarstaedteWashington.add(Stadt.NEW_YORK);
        nachbarstaedteWashington.add(Stadt.ATLANTA);
        nachbarstaedteWashington.add(Stadt.MIAMI);

        Stadt.WASHINGTON.setNachbarstaedte(nachbarstaedteWashington);

        //----------------------------------------------------------------------------

        ArrayList<Stadt> nachbarstaedteLondon = new ArrayList<Stadt>();

        nachbarstaedteLondon.add(Stadt.NEW_YORK);
        nachbarstaedteLondon.add(Stadt.MADRID);
        nachbarstaedteLondon.add(Stadt.PARIS);
        nachbarstaedteLondon.add(Stadt.ESSEN);

        Stadt.LONDON.setNachbarstaedte(nachbarstaedteLondon);

        //----------------------------------------------------------------------------

        ArrayList<Stadt> nachbarstaedteEssen = new ArrayList<Stadt>();

        nachbarstaedteEssen.add(Stadt.LONDON);
        nachbarstaedteEssen.add(Stadt.PARIS);
        nachbarstaedteEssen.add(Stadt.MAILAND);
        nachbarstaedteEssen.add(Stadt.ST_PETERSBURG);

        Stadt.ESSEN.setNachbarstaedte(nachbarstaedteEssen);

        //----------------------------------------------------------------------------

        ArrayList<Stadt> nachbarstaedteParis = new ArrayList<Stadt>();

        nachbarstaedteParis.add(Stadt.MADRID);
        nachbarstaedteParis.add(Stadt.LONDON);
        nachbarstaedteParis.add(Stadt.ESSEN);
        nachbarstaedteParis.add(Stadt.MAILAND);
        nachbarstaedteParis.add(Stadt.ALGIER);

        Stadt.PARIS.setNachbarstaedte(nachbarstaedteParis);

        //----------------------------------------------------------------------------

        ArrayList<Stadt> nachbarstaedteMadrid = new ArrayList<Stadt>();

        nachbarstaedteMadrid.add(Stadt.NEW_YORK);
        nachbarstaedteMadrid.add(Stadt.SAN_PAULO);
        nachbarstaedteMadrid.add(Stadt.PARIS);
        nachbarstaedteMadrid.add(Stadt.ALGIER);
        nachbarstaedteMadrid.add(Stadt.LONDON);

        Stadt.MADRID.setNachbarstaedte(nachbarstaedteMadrid);

        //----------------------------------------------------------------------------

        ArrayList<Stadt> nachbarstaedteMailand = new ArrayList<Stadt>();

        nachbarstaedteMailand.add(Stadt.PARIS);
        nachbarstaedteMailand.add(Stadt.ESSEN);
        nachbarstaedteMailand.add(Stadt.ST_PETERSBURG);

        Stadt.MAILAND.setNachbarstaedte(nachbarstaedteMailand);

        //----------------------------------------------------------------------------

        ArrayList<Stadt> nachbarstaedteStPetersburg = new ArrayList<Stadt>();

        nachbarstaedteStPetersburg.add(Stadt.ESSEN);
        nachbarstaedteStPetersburg.add(Stadt.MOSKAU);
        nachbarstaedteStPetersburg.add(Stadt.ISTANBUL);

        Stadt.ST_PETERSBURG.setNachbarstaedte(nachbarstaedteStPetersburg);

        //----------------------------------------------------------------------------

        ArrayList<Stadt> nachbarstaedteLosAngeles = new ArrayList<Stadt>();

        nachbarstaedteLosAngeles.add(Stadt.SAN_FRANCISCO);
        nachbarstaedteLosAngeles.add(Stadt.CHICAGO);
        nachbarstaedteLosAngeles.add(Stadt.MEXICO_STADT);
        nachbarstaedteLosAngeles.add(Stadt.SYDNEY);

        Stadt.LOS_ANGELES.setNachbarstaedte(nachbarstaedteLosAngeles);

        //----------------------------------------------------------------------------

        ArrayList<Stadt> nachbarstaedteMexicoStadt = new ArrayList<Stadt>();

        nachbarstaedteMexicoStadt.add(Stadt.LOS_ANGELES);
        nachbarstaedteMexicoStadt.add(Stadt.CHICAGO);
        nachbarstaedteMexicoStadt.add(Stadt.MIAMI);
        nachbarstaedteMexicoStadt.add(Stadt.BOGOTA);
        nachbarstaedteMexicoStadt.add(Stadt.LIMA);

        Stadt.MEXICO_STADT.setNachbarstaedte(nachbarstaedteMexicoStadt);

        //----------------------------------------------------------------------------

        ArrayList<Stadt> nachbarstaedteMiami = new ArrayList<Stadt>();

        nachbarstaedteMiami.add(Stadt.ATLANTA);
        nachbarstaedteMiami.add(Stadt.WASHINGTON);
        nachbarstaedteMiami.add(Stadt.MEXICO_STADT);
        nachbarstaedteMiami.add(Stadt.BOGOTA);

        Stadt.MIAMI.setNachbarstaedte(nachbarstaedteMiami);

        //----------------------------------------------------------------------------

        ArrayList<Stadt> nachbarstaedteBogota = new ArrayList<Stadt>();

        nachbarstaedteBogota.add(Stadt.MIAMI);
        nachbarstaedteBogota.add(Stadt.MEXICO_STADT);
        nachbarstaedteBogota.add(Stadt.LIMA);
        nachbarstaedteBogota.add(Stadt.SAN_PAULO);
        nachbarstaedteBogota.add(Stadt.BUENOS_AIRES);

        Stadt.BOGOTA.setNachbarstaedte(nachbarstaedteBogota);

        //----------------------------------------------------------------------------

        ArrayList<Stadt> nachbarstaedteLima = new ArrayList<Stadt>();

        nachbarstaedteLima.add(Stadt.BOGOTA);
        nachbarstaedteLima.add(Stadt.SANTIAGO);
        nachbarstaedteLima.add(Stadt.MEXICO_STADT);

        Stadt.LIMA.setNachbarstaedte(nachbarstaedteLima);

        //----------------------------------------------------------------------------

        ArrayList<Stadt> nachbarstaedteSantiago = new ArrayList<Stadt>();

        nachbarstaedteSantiago.add(Stadt.LIMA);

        Stadt.SANTIAGO.setNachbarstaedte(nachbarstaedteSantiago);

        //----------------------------------------------------------------------------

        ArrayList<Stadt> nachbarstaedteSanpPulo = new ArrayList<Stadt>();

        nachbarstaedteSanpPulo.add(Stadt.BOGOTA);
        nachbarstaedteSanpPulo.add(Stadt.BUENOS_AIRES);
        nachbarstaedteSanpPulo.add(Stadt.LAGOS);
        nachbarstaedteSanpPulo.add(Stadt.MADRID);

        Stadt.SAN_PAULO.setNachbarstaedte(nachbarstaedteSanpPulo);

        //----------------------------------------------------------------------------

        ArrayList<Stadt> nachbarstaedteBuanosAires = new ArrayList<Stadt>();

        nachbarstaedteBuanosAires.add(Stadt.BOGOTA);
        nachbarstaedteBuanosAires.add(Stadt.SAN_PAULO);

        Stadt.BUENOS_AIRES.setNachbarstaedte(nachbarstaedteBuanosAires);

        //----------------------------------------------------------------------------

        ArrayList<Stadt> nachbarstaedteLagos = new ArrayList<Stadt>();

        nachbarstaedteLagos.add(Stadt.SAN_PAULO);
        nachbarstaedteLagos.add(Stadt.KINSHASA);
        nachbarstaedteLagos.add(Stadt.KHARTUM);

        Stadt.LAGOS.setNachbarstaedte(nachbarstaedteLagos);

        //----------------------------------------------------------------------------

        ArrayList<Stadt> nachbarstaedteKhartum = new ArrayList<Stadt>();

        nachbarstaedteKhartum.add(Stadt.KAIRO);
        nachbarstaedteKhartum.add(Stadt.LAGOS);
        nachbarstaedteKhartum.add(Stadt.KINSHASA);
        nachbarstaedteKhartum.add(Stadt.JOHANNESBURG);

        Stadt.KHARTUM.setNachbarstaedte(nachbarstaedteKhartum);

        //----------------------------------------------------------------------------

        ArrayList<Stadt> nachbarstaedteKinshasa = new ArrayList<Stadt>();

        nachbarstaedteKinshasa.add(Stadt.LAGOS);
        nachbarstaedteKinshasa.add(Stadt.KHARTUM);
        nachbarstaedteKinshasa.add(Stadt.JOHANNESBURG);

        Stadt.KINSHASA.setNachbarstaedte(nachbarstaedteKinshasa);

        //----------------------------------------------------------------------------

        ArrayList<Stadt> nachbarstaedteJohannesburg = new ArrayList<Stadt>();

        nachbarstaedteJohannesburg.add(Stadt.KINSHASA);
        nachbarstaedteJohannesburg.add(Stadt.KHARTUM);

        Stadt.JOHANNESBURG.setNachbarstaedte(nachbarstaedteJohannesburg);

        //----------------------------------------------------------------------------

        ArrayList<Stadt> nachbarstaedteMoskau = new ArrayList<Stadt>();

        nachbarstaedteMoskau.add(Stadt.ST_PETERSBURG);
        nachbarstaedteMoskau.add(Stadt.ISTANBUL);
        nachbarstaedteMoskau.add(Stadt.TEHERAN);

        Stadt.MOSKAU.setNachbarstaedte(nachbarstaedteMoskau);

        //----------------------------------------------------------------------------

        ArrayList<Stadt> nachbarstaedteIstanbul = new ArrayList<Stadt>();

        nachbarstaedteIstanbul.add(Stadt.ALGIER);
        nachbarstaedteIstanbul.add(Stadt.MAILAND);
        nachbarstaedteIstanbul.add(Stadt.ST_PETERSBURG);
        nachbarstaedteIstanbul.add(Stadt.MOSKAU);
        nachbarstaedteIstanbul.add(Stadt.BAGDAD);

        Stadt.ISTANBUL.setNachbarstaedte(nachbarstaedteIstanbul);

        //----------------------------------------------------------------------------

        ArrayList<Stadt> nachbarstaedteAlgier = new ArrayList<Stadt>();

        nachbarstaedteAlgier.add(Stadt.MADRID);
        nachbarstaedteAlgier.add(Stadt.PARIS);
        nachbarstaedteAlgier.add(Stadt.ISTANBUL);
        nachbarstaedteAlgier.add(Stadt.KAIRO);

        Stadt.ALGIER.setNachbarstaedte(nachbarstaedteAlgier);

        //----------------------------------------------------------------------------

        ArrayList<Stadt> nachbarstaedteKairo = new ArrayList<Stadt>();

        nachbarstaedteKairo.add(Stadt.ALGIER);
        nachbarstaedteKairo.add(Stadt.ISTANBUL);
        nachbarstaedteKairo.add(Stadt.BAGDAD);
        nachbarstaedteKairo.add(Stadt.RIAD);
        nachbarstaedteKairo.add(Stadt.KHARTUM);

        Stadt.KAIRO.setNachbarstaedte(nachbarstaedteKairo);

        //----------------------------------------------------------------------------

        ArrayList<Stadt> nachbarstaedteRiad = new ArrayList<Stadt>();

        nachbarstaedteRiad.add(Stadt.KAIRO);
        nachbarstaedteRiad.add(Stadt.BAGDAD);
        nachbarstaedteRiad.add(Stadt.KARATSCHI);

        Stadt.RIAD.setNachbarstaedte(nachbarstaedteRiad);

        //----------------------------------------------------------------------------

        ArrayList<Stadt> nachbarstaedteBagdad = new ArrayList<Stadt>();

        nachbarstaedteBagdad.add(Stadt.KAIRO);
        nachbarstaedteBagdad.add(Stadt.ISTANBUL);
        nachbarstaedteBagdad.add(Stadt.TEHERAN);
        nachbarstaedteBagdad.add(Stadt.KARATSCHI);
        nachbarstaedteBagdad.add(Stadt.RIAD);

        Stadt.BAGDAD.setNachbarstaedte(nachbarstaedteBagdad);

        //----------------------------------------------------------------------------

        ArrayList<Stadt> nachbarstaedteTeheran = new ArrayList<Stadt>();

        nachbarstaedteTeheran.add(Stadt.MOSKAU);
        nachbarstaedteTeheran.add(Stadt.BAGDAD);
        nachbarstaedteTeheran.add(Stadt.KARATSCHI);
        nachbarstaedteTeheran.add(Stadt.DELHI);

        Stadt.TEHERAN.setNachbarstaedte(nachbarstaedteTeheran);

        //----------------------------------------------------------------------------

        ArrayList<Stadt> nachbarstaedteKaratschi = new ArrayList<Stadt>();

        nachbarstaedteKaratschi.add(Stadt.RIAD);
        nachbarstaedteKaratschi.add(Stadt.BAGDAD);
        nachbarstaedteKaratschi.add(Stadt.TEHERAN);
        nachbarstaedteKaratschi.add(Stadt.DELHI);
        nachbarstaedteKaratschi.add(Stadt.MUMBAI);

        Stadt.KARATSCHI.setNachbarstaedte(nachbarstaedteKaratschi);

        //----------------------------------------------------------------------------

        ArrayList<Stadt> nachbarstaedteDelhi = new ArrayList<Stadt>();

        nachbarstaedteDelhi.add(Stadt.TEHERAN);
        nachbarstaedteDelhi.add(Stadt.KARATSCHI);
        nachbarstaedteDelhi.add(Stadt.MUMBAI);
        nachbarstaedteDelhi.add(Stadt.CHENNAI);
        nachbarstaedteDelhi.add(Stadt.KALKUTTA);

        Stadt.DELHI.setNachbarstaedte(nachbarstaedteDelhi);

        //----------------------------------------------------------------------------

        ArrayList<Stadt> nachbarstaedteMumbai = new ArrayList<Stadt>();

        nachbarstaedteMumbai.add(Stadt.KARATSCHI);
        nachbarstaedteMumbai.add(Stadt.DELHI);
        nachbarstaedteMumbai.add(Stadt.CHENNAI);

        Stadt.MUMBAI.setNachbarstaedte(nachbarstaedteMumbai);

        //----------------------------------------------------------------------------

        ArrayList<Stadt> nachbarstaedteKalkutta = new ArrayList<Stadt>();

        nachbarstaedteKalkutta.add(Stadt.DELHI);
        nachbarstaedteKalkutta.add(Stadt.CHENNAI);
        nachbarstaedteKalkutta.add(Stadt.BANGKOK);
        nachbarstaedteKalkutta.add(Stadt.HONGKONG);

        Stadt.KALKUTTA.setNachbarstaedte(nachbarstaedteKalkutta);

        //----------------------------------------------------------------------------

        ArrayList<Stadt> nachbarstaedteChennai = new ArrayList<Stadt>();

        nachbarstaedteChennai.add(Stadt.MUMBAI);
        nachbarstaedteChennai.add(Stadt.DELHI);
        nachbarstaedteChennai.add(Stadt.KALKUTTA);
        nachbarstaedteChennai.add(Stadt.BANGKOK);
        nachbarstaedteChennai.add(Stadt.JAKARTA);

        Stadt.CHENNAI.setNachbarstaedte(nachbarstaedteChennai);

        //----------------------------------------------------------------------------

        ArrayList<Stadt> nachbarstaedtePeking = new ArrayList<Stadt>();

        nachbarstaedtePeking.add(Stadt.SEOUL);
        nachbarstaedtePeking.add(Stadt.SHANGHAI);

        Stadt.PEKING.setNachbarstaedte(nachbarstaedtePeking);

        //----------------------------------------------------------------------------

        ArrayList<Stadt> nachbarstaedteSeoul = new ArrayList<Stadt>();

        nachbarstaedteSeoul.add(Stadt.PEKING);
        nachbarstaedteSeoul.add(Stadt.SHANGHAI);
        nachbarstaedteSeoul.add(Stadt.TOKIO);

        Stadt.SEOUL.setNachbarstaedte(nachbarstaedteSeoul);

        //----------------------------------------------------------------------------

        ArrayList<Stadt> nachbarstaedteShanghai = new ArrayList<Stadt>();

        nachbarstaedteShanghai.add(Stadt.PEKING);
        nachbarstaedteShanghai.add(Stadt.SEOUL);
        nachbarstaedteShanghai.add(Stadt.TOKIO);
        nachbarstaedteShanghai.add(Stadt.HONGKONG);
        nachbarstaedteShanghai.add(Stadt.TAIPEH);

        Stadt.SHANGHAI.setNachbarstaedte(nachbarstaedteShanghai);

        //----------------------------------------------------------------------------

        ArrayList<Stadt> nachbarstaedteTokio = new ArrayList<Stadt>();

        nachbarstaedteTokio.add(Stadt.SEOUL);
        nachbarstaedteTokio.add(Stadt.SHANGHAI);
        nachbarstaedteTokio.add(Stadt.OSAKA);
        nachbarstaedteTokio.add(Stadt.SAN_FRANCISCO);

        Stadt.TOKIO.setNachbarstaedte(nachbarstaedteTokio);

        //----------------------------------------------------------------------------

        ArrayList<Stadt> nachbarstaedteOsaka = new ArrayList<Stadt>();

        nachbarstaedteOsaka.add(Stadt.TOKIO);
        nachbarstaedteOsaka.add(Stadt.TAIPEH);

        Stadt.OSAKA.setNachbarstaedte(nachbarstaedteOsaka);

        //----------------------------------------------------------------------------

        ArrayList<Stadt> nachbarstaedteHongkong = new ArrayList<Stadt>();

        nachbarstaedteHongkong.add(Stadt.KALKUTTA);
        nachbarstaedteHongkong.add(Stadt.SHANGHAI);
        nachbarstaedteHongkong.add(Stadt.TAIPEH);
        nachbarstaedteHongkong.add(Stadt.MANILA);
        nachbarstaedteHongkong.add(Stadt.HO_CHI_MINH_STADT);
        nachbarstaedteHongkong.add(Stadt.BANGKOK);

        Stadt.HONGKONG.setNachbarstaedte(nachbarstaedteHongkong);

        //----------------------------------------------------------------------------

        ArrayList<Stadt> nachbarstaedteTaipeh = new ArrayList<Stadt>();

        nachbarstaedteTaipeh.add(Stadt.SHANGHAI);
        nachbarstaedteTaipeh.add(Stadt.OSAKA);
        nachbarstaedteTaipeh.add(Stadt.HONGKONG);
        nachbarstaedteTaipeh.add(Stadt.MANILA);

        Stadt.TAIPEH.setNachbarstaedte(nachbarstaedteTaipeh);

        //----------------------------------------------------------------------------

        ArrayList<Stadt> nachbarstaedteBangkok = new ArrayList<Stadt>();

        nachbarstaedteBangkok.add(Stadt.KALKUTTA);
        nachbarstaedteBangkok.add(Stadt.HONGKONG);
        nachbarstaedteBangkok.add(Stadt.HO_CHI_MINH_STADT);
        nachbarstaedteBangkok.add(Stadt.JAKARTA);
        nachbarstaedteBangkok.add(Stadt.CHENNAI);

        Stadt.BANGKOK.setNachbarstaedte(nachbarstaedteBangkok);

        //----------------------------------------------------------------------------

        ArrayList<Stadt> nachbarstaedteHoChiMinStadt = new ArrayList<Stadt>();

        nachbarstaedteHoChiMinStadt.add(Stadt.JAKARTA);
        nachbarstaedteHoChiMinStadt.add(Stadt.BANGKOK);
        nachbarstaedteHoChiMinStadt.add(Stadt.HONGKONG);
        nachbarstaedteHoChiMinStadt.add(Stadt.MANILA);

        Stadt.HO_CHI_MINH_STADT.setNachbarstaedte(nachbarstaedteHoChiMinStadt);

        //----------------------------------------------------------------------------

        ArrayList<Stadt> nachbarstaedteKakarta = new ArrayList<Stadt>();

        nachbarstaedteKakarta.add(Stadt.CHENNAI);
        nachbarstaedteKakarta.add(Stadt.BANGKOK);
        nachbarstaedteKakarta.add(Stadt.HO_CHI_MINH_STADT);
        nachbarstaedteKakarta.add(Stadt.SYDNEY);

        Stadt.JAKARTA.setNachbarstaedte(nachbarstaedteKakarta);

        //----------------------------------------------------------------------------

        ArrayList<Stadt> nachbarstaedteSydney = new ArrayList<Stadt>();

        nachbarstaedteSydney.add(Stadt.JAKARTA);
        nachbarstaedteSydney.add(Stadt.MANILA);
        nachbarstaedteSydney.add(Stadt.LOS_ANGELES);

        Stadt.SYDNEY.setNachbarstaedte(nachbarstaedteSydney);

        //----------------------------------------------------------------------------

        ArrayList<Stadt> nachbarstaedteMANILA = new ArrayList<Stadt>();

        nachbarstaedteMANILA.add(Stadt.HO_CHI_MINH_STADT);
        nachbarstaedteMANILA.add(Stadt.HONGKONG);
        nachbarstaedteMANILA.add(Stadt.TAIPEH);
        nachbarstaedteMANILA.add(Stadt.SAN_FRANCISCO);
        nachbarstaedteMANILA.add(Stadt.SYDNEY);

        Stadt.MANILA.setNachbarstaedte(nachbarstaedteMANILA);


    }
}
