package pandemie.game.spielablauf;

import pandemie.draws.Positionen;
import pandemie.game.spielkarten.ISpielkarte;
import pandemie.game.spielkarten.Stadt;
import pandemie.view.SpielplanController;

import java.util.*;


public class Spiel {

    ArrayList<Spieler> spielerListe;
    LinkedList<ISpielkarte> spielkartenstapel;
    LinkedList<Stadt> infektionsKartenStapel;
    LinkedList<Stadt> infektionsAblagestapel;
    SpielplanController controller;
    private int infektionsRate;
    private int infektionenProZug;
    private Integer ausbruchsCounter;
    private int nummerSpielerDran = 1;
    private int schwierigkeitsGrad = 1;

    public Spiel(int spielerAnzahl, int schwierigkeitsGrad, SpielplanController controller){
        // Setze Ausbruchscounter
        ausbruchsCounter = 0;
        infektionsRate = 1;
        aktualisiereInfektionenProZug(infektionsRate);
        
        // Uebergebe controller
        this.controller = controller;

        // Lade Stadtpositionen
        Positionen.load();

        // Lade Nachbarstädte
        Nachbarstaedte.load();

        // spielerListe erzeugen
        spielerListe = Rollenverteilung.loadRandom(spielerAnzahl, controller.getSpielerNamen());

        // alle Spieler mit Handkarten versorgen, spielkarten sortieren und Spielkartenstapel erzeugen:
        Spielkartenstapel spielKartenStapel = new Spielkartenstapel(this);

        //spielkartenstapel = Spielkartenstapel.getSpielkartenstapel(schwierigkeitsGrad, spielerListe);

        // Infektionskarten sortieren und in den Infektionskartenstapel erzeugen
        infektionsKartenStapel = Spielkartenstapel.getinfektionskartenStapel();

        // Infektionsablagestapel erzeugen (am Anfang naoch leer):
        infektionsAblagestapel = new LinkedList<Stadt>();
    }

    public void beginneErstenZug(){
        initialInfektion();
        controller.aktualisiereKartenUndAktionsPanel(spielerListe.get(nummerSpielerDran));
    }

    private void aktualisiereInfektionenProZug(int infektionsRate) {
        if(infektionsRate<1){
            System.err.println("Infektionsrate kann nicht kleiner sein als 2.");
        } else if(infektionsRate<4){
            infektionenProZug = 2;
        } else if(infektionsRate<6){
            infektionenProZug = 3;
        } else if(infektionsRate<8){
            infektionenProZug = 4;
        } else{
            System.err.println("Infektionsrate kann nicht größer als 7 sein. Zuviele Epedemien?");
        }
    }

    public ArrayList<Spieler> getSpielerListe() {
        return spielerListe;
    }

    private void initialInfektion(){
        //Lösche Nachrichtenlog in der Anzeige
        controller.createBtnClearLog();

        // Zum Spielbeginn verseuchte Staedte bestimmen:
        // 1. 3 Karten vom Infektionskartenstapel ziehen und mit 3 Seuchen (3 Seuchenwuerfel) verseuchen
        // 2. 3 Karten vom Infektionskartenstapel ziehen und mit 2 Seuchen (2 Seuchenwuerfel) verseuchen
        // 3. 3 Karten vom Infektionskartenstapel ziehen und mit 1 Seuche (1 Seuchenwuerfel) verseuchen
        int wuerfelanzahl = 3;
        for(int jj = 1; jj < 4; jj ++) {
            for (int ii = 1; ii < 4; ii++) {
                // oberste karte aus dem Infektionskartenstapel nehmen:
                Stadt infektionskarte = (Stadt) infektionsKarteZiehen();

                //die Stadt mit entsprechender Wuerfelanzahl (Infektionsskala) des entsprechenden Erregertyps infizieren
                infektionskarte.setSkala(infektionskarte.getErreger(), wuerfelanzahl);

                // zeige die Infektion an
                controller.wurdeInfiziert(infektionskarte, infektionskarte.getErreger(), wuerfelanzahl);
            }
            wuerfelanzahl--;
        }
    }

    public LinkedList<ISpielkarte> getSpielkartenstapel() {
        return spielkartenstapel;
    }

    public void setSpielkartenstapel(LinkedList<ISpielkarte> spielkartenstapel) {
        this.spielkartenstapel = spielkartenstapel;
    }

    public LinkedList<Stadt> getInfektionsKartenStapel() {
        return infektionsKartenStapel;
    }

    public void setInfektionsKartenStapel(LinkedList<Stadt> infektionskartenstapel) {
        this.infektionsKartenStapel = infektionskartenstapel;
    }

    public LinkedList<Stadt> getInfektionsAblagestapel() {
        return infektionsAblagestapel;
    }

    public void setInfektionsAblagestapel(LinkedList<Stadt> infektionsAblagestapel) {
        this.infektionsAblagestapel = infektionsAblagestapel;
    }


    /**
     * @param spieler
     * @return
     */
    public ISpielkarte spielKarteZiehen(Spieler spieler) {

        if (spieler.getHandkarten().size() >= spieler.MAX_KARTENZAHL) {

            System.err.println(spieler.getName() + "hat schon " + spieler.getHandkarten().size() + "Handkarten!");
            return null;
        }

        LinkedList<ISpielkarte> spielkartenStapel = this.getSpielkartenstapel();

        if (spielkartenStapel.size() < 1) { // wenn im Spielkartenstapel kene Karten vorhanden sind

            System.err.println("Im Spielkartenstapel sind keine Karten mehr vorhanden!");
            return null;
        }

        // die letzte Karte vom Spielkartenstapel entfernen
        ISpielkarte eineSpielkarte = spielkartenStapel.getLast();
        spielkartenStapel.removeLast();

        // die Spielkarte zu den Spielerhandkarten hinzufuegen:
        LinkedList<ISpielkarte> handkarten = spieler.getHandkarten();
        handkarten.add(eineSpielkarte);
        spieler.setHandkarten(handkarten);

        // Spielkartenstapel updaten:
        this.setSpielkartenstapel(spielkartenStapel);

        return eineSpielkarte;
    }

    public LinkedList<Stadt> zieheInfektionsKarten(int i){
        LinkedList<Stadt> gezogeneKarten = new LinkedList<>();
        gezogeneKarten.add(infektionsKarteZiehen());
        return gezogeneKarten;
    }

    /**
     * Zieht eine Karte aus dem InfektionsKartenStapel, gibt sie zurück.
     * Die Infektionskarte wird auf den InfektionsKartenAblageStapel gelegt.
     *
     * @return
     */
    public Stadt infektionsKarteZiehen() {

        if (this.getInfektionsKartenStapel().size() < 1) { // wenn im Spielkartenstapel kene Karten vorhanden sind
            System.err.println("Im Infektionskartenstapel sind keine Karten mehr vorhanden!");
            return null;
        }

        Stadt eineInfektionsKarte = infektionsKartenStapel.pollLast();
        infektionsAblagestapel.add(eineInfektionsKarte);

        return eineInfektionsKarte;
    }

    /**
     * @param eineInfektionskarte
     */
    public void infektionskartenAblegen(Stadt eineInfektionskarte) {

        LinkedList<Stadt> infektionskartenAblagestapel = this.getInfektionsAblagestapel();
        infektionskartenAblagestapel.add(eineInfektionskarte);

        this.setInfektionsAblagestapel(infektionskartenAblagestapel);
    }

    public void inkrementAusbrueche(){
        ausbruchsCounter++;
        controller.setAusbruchsCounter(ausbruchsCounter);
    }
    
    public void inkrementInfektionsRate() {
        infektionsRate++;
        aktualisiereInfektionenProZug(infektionsRate);
        controller.setInfektionsRate(infektionsRate);
    }

    public void mussHandkarteAblegen(Spieler spieler) {
        System.err.println("die Funktion mussHandkarteAblegen(Spieler) ist noch nicht implementiert.");
    }

    public void erregerSpielen() {

    }

    public int getNummerSpielerDran() {
        return nummerSpielerDran;
    }

    public void inkrementNummerSpielerDran() {
        nummerSpielerDran++;
        nummerSpielerDran = nummerSpielerDran%spielerListe.size();
    }

    public int getSchwierigkeitsGrad() {
        return schwierigkeitsGrad;
    }

    public void setSchwierigkeitsGrad(int schwierigkeitsGrad) {
        this.schwierigkeitsGrad = schwierigkeitsGrad;
    }
}
