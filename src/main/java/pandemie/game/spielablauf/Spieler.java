package pandemie.game.spielablauf;


import pandemie.game.ErregerTyp;
import pandemie.game.rollen.Rolle;
import pandemie.game.spielkarten.Ereignis;
import pandemie.game.spielkarten.Ereigniskarte;
import pandemie.game.spielkarten.ISpielkarte;
import pandemie.game.spielkarten.Stadt;
import pandemie.view.SpielplanController;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;

/**
 * Die Klasse "Spieler".
 * Es werden 2-5 Spieler erzeugt mit den zufaelliig gewaehlten Rollen (Berufen) sowie und einfachen und besonderen Aktionsfunktionen
 */
public class Spieler {

    public static final int MAX_KARTENZAHL = 7; // Klassenvariable, Maximal erlaubte Anzahl von Handkarten

    private int aktionspunkte; // Instanzvariable, Aktionspunkte (max 4)
    private Spielphase spielphase; // Instanzvariable, Aktuelle Spielphase im Spielablauf, in der sich der Spieler befindet
    private String name; // Instanzvariable, Der Name des Spielers
    private Rolle rolle; // Instanzvariable, Die Rolle (Beruf) des Spielers
    private Stadt stadt; // Instanzvariable, Die Stadt, in der sich der Spieler befindet

    private LinkedList<ISpielkarte> handkarten;


    /**
     * @param rolle ist ein Objekt der Klasse "Rolle": Arzt, Betriebsexperte, Forscher, Logistikexperte, Wissenschaftler
     */
    public Spieler(Rolle rolle, String name) {
        aktionspunkte = 4;
        this.rolle = rolle;
        spielphase = Spielphase.AKTION;
        handkarten = new LinkedList<>();
        this.name = name;
        stadt = Stadt.ATLANTA;
    }



    /**
     * Setermetode fuer die Instanzvariable "spielphase"
     * @return ist der Rueckgabewert der Instanzvariable "spielphase"
     */
    public Spielphase getSpielphase() {
        return spielphase;
    }

    /**
     * Setermetode fuer die Instanzvariable "spielphase"
     * @param spielphase ist der Initialisierungswert fuer die Instanzvariable "spielphase"
     */
    public void setSpielphase(Spielphase spielphase) {
        this.spielphase = spielphase;
    }

    /**
     * Getermetode fuer die Instanzvariable "handkarten"
     * @return @return Gibt die Handkarten eines Spielers zurueck
     */
    public LinkedList<ISpielkarte> getHandkarten() {
        return handkarten;
    }

    public LinkedList<Stadt> getStadtkarten() {
        LinkedList<Stadt> result = new LinkedList<>();
        for (ISpielkarte handkarte : handkarten) {
            if (handkarte instanceof Stadt) {
                result.add((Stadt) handkarte);
            }
        }
        return result;
    }



    public LinkedList<ISpielkarte> getEreigniskarten() {
        LinkedList<ISpielkarte> result = new LinkedList<>();
        for (ISpielkarte handkarte : handkarten) {
            if (handkarte instanceof Ereigniskarte) {
                result.add(handkarte);
            }
        }
        return result;
    }

    /**
     * Setermetode fuer die Instanzvariable "handkarten"
     * @param handkarten ist der Initialisierungswert fuer die Instanzvariable "handkarten"
     */
    public void setHandkarten(LinkedList<ISpielkarte> handkarten) {
        this.handkarten = handkarten;
    }

    /**
     * Getermetode fuer die Instanzvariable "spielername"
     * @return Gibt den Namen eines Spielers zurueck
     */
    public String getName() {
        return name;
    }


    /**
     * Setermetode fuer die Instanzvariable "spielername"
     * @param name ist der Initialisierungswert fuer die Instanzvariable "spielername"
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Getermetode fuer die Instanzvariable "aktionspunkte"
     * @return Gibt die Anzahl der Aktionspunkte eines Spielers zurueck
     */
    public int getAktionspunkte() { return aktionspunkte; }

    /**
     * Setermetode fuer die Instanzvariable "aktionspunkte"
     * @param aktionspunkte ist der Initialisierungswert fuer die Instanzvariable "aktionspunkte"
     */
    public void setAktionspunkte(int aktionspunkte) {
        this.aktionspunkte = aktionspunkte;
    }

    /**
     * Getermetode fuer die Instanzvariable "stadt"
     * @return Gibt die aktuelle Stadt eines Spielers zurueck
     */
    public Stadt getStadt() {
        return stadt;
    }


    /**
     * Setermetode fuer die Instanzvariable "stadt" (die Klasse Stadt)
     * @param stadt ist der Initialisierungswert fuer die Instanzvariable "stadt"
     */
    public void setStadt(Stadt stadt) {
        this.stadt = stadt;
    }

    /**
     * Getermetode fuer die Instanzvariable "rolle"
     * @return Gibt daie Rolle (Beruf) eines Spielers zurueck
     */
    public Rolle getRolle() {
        return rolle;
    }
    // Einfache Aktionen:

    /**
     * Auto(oder Fähre) Ziehe Spielfigur eine benachbarte Stadt weiter
     *
     * @param zielstadt ist die Zielstadt, in die sich der Spieler begeben moechte
     */
    public void autofahrt(Stadt zielstadt) {

        if (this.aktionspunkte < 1) {
            System.err.println("Sie haben keine Aktionspunkte mehr!");
            return;
        }

        Stadt aktuelleStadt = this.getStadt();
        ArrayList<Stadt> nachberstaedte = aktuelleStadt.getNachbarstaedte();

        if (nachberstaedte.contains(zielstadt)) {

            this.setStadt(zielstadt);

            //Anzahl der Aktionspunkte um 1 reduziert:
            int aktionspunkte = this.getAktionspunkte();
            aktionspunkte = aktionspunkte - 1;
            this.setAktionspunkte(aktionspunkte);

        } else {

            System.err.println("Die Zielstadt ist keine Nachbarstadt von Ihrem Ort!");
            return;
        }
    }


    /**
     * Direktflug (Lege Handkarte ab und bewege dich direkt in die Stadt auf der Karte)
     *
     * @param eineHandkarte ist eine Handkarte, auf der die Stadt abgebildet ist, in die sich der
     *                      Spieler begeben moechte. Danach wird die Handkarte abgelegt.
     */
    public void direktflug(ISpielkarte eineHandkarte) {

        if (this.aktionspunkte < 1) {
            System.err.println("Sie haben keine Aktionspunkte mehr!");
            return;
        }

        if (eineHandkarte instanceof Stadt) {

            // "eineHandkarte" in das Objekt der Klasse "Stadt" casten:
            Stadt stadtKarte = (Stadt) eineHandkarte;

            LinkedList<ISpielkarte> handkarten = this.getHandkarten();

            // uberpruefen, ob eine solche Handkarte bei dem Speler tatsaechlich vorhanden ist:
            if (handkarten.contains(eineHandkarte)) {

                // den Spieler in die uebergebene Stadtkarte "eineHandkarte" versetzen
                this.setStadt(stadtKarte);

                // die Karte "eineHandkarte" wird aus den Handkarten des Spieler entfernt:
                this.handkarten.remove(eineHandkarte);

                //Anzahl der Aktionspunkte um 1 reduziert:
                int aktionspunkte = this.getAktionspunkte();
                aktionspunkte = aktionspunkte - 1;
                this.setAktionspunkte(aktionspunkte);

            } else {

                System.err.println("Sie besitzen keine Handkarte mit der Stadt, in die Sie sich begeben moechten!");
                return;
            }

        } else {

            System.err.println("Die aktuelle Handkarte ist keine Instanz der Klasse Stadt");
            return;
        }
    }


    /**
     * Legen Sie die Karte ab, auf der Ihr jetziger Standort abgebildet ist, um sich zu einer beliebigen Stadt zu bewegen
     *
     * @param zielstadt ist die Zielstadt, in die sich der Spieler begeben moechte.
     */
    public void charterflug(Stadt zielstadt) {

        if (this.aktionspunkte < 1) {
            System.err.println("Sie haben keine Aktionspunkte mehr!");
            return;
        }

        Stadt aktStadt = this.getStadt();

        // uberpruefen, ob eine Handkarte mit der Stadt, in der sich der Spieler befindet, bei dem Speler tatsaechlich vorhanden ist:

        if (this.handkarten.contains(aktStadt)) {

            // der Spieler wird in die Zielstadt versetzt:
            this.setStadt(zielstadt);

            // die Karte mit der aktuellen Stadt wird aus den Handkarten des Spieler entfernt:
            this.handkarten.remove(aktStadt);

            //Anzahl der Aktionspunkte um 1 reduziert:
            int aktionspunkte = this.getAktionspunkte();
            aktionspunkte = aktionspunkte - 1;
            this.setAktionspunkte(aktionspunkte);

        } else {

            System.err.println("Sie besitzen keine Handkarte mit der Stadt, in der Sie sich gerade befinden!");
            return;
        }
    }

    /**
     * Wenn Sie in einer Stadt mit einem Forschungslabor sind, können Sie sich in eine andere Stadt
     * mit einem Forschungslabor bewegen
     *
     * @param zielstadt ist die Zielstadt, in die sich der Spieler begeben moechte.
     */
    public void zubringerFlug(Stadt zielstadt) {

        if (this.aktionspunkte < 1) {
            System.err.println("Sie haben keine Aktionspunkte mehr!");
            return;
        }

        Stadt actStadt = this.getStadt();

        if ((zielstadt.isHatForschungsLabor()) && (actStadt.isHatForschungsLabor())) {

            //Anzahl der Aktionspunkte um 1 reduziert:
            int aktionspunkte = this.getAktionspunkte();
            aktionspunkte = aktionspunkte - 1;
            this.setAktionspunkte(aktionspunkte);

            // den Spieler in die Zielstadt versetzen:
            this.setStadt(zielstadt);

        } else {

            System.err.println("In Der Start- oder Zielstadt ist kein Forschunglabor vorhanden!");
            return;
        }
    }

    // Besondere Aktionen:

    /**
     * Forschungslabor einrichten (lege die Handkarte ab auf der ihr jetziger Standord abgebildet ist)
     */
    public void forschungsLaborErrichten() {

        if (this.aktionspunkte < 1) {
            System.err.println("Sie haben keine Aktionspunkte mehr!");
            return;
        }

        Stadt aktStadt = this.getStadt();

        if (aktStadt.isHatForschungsLabor()) {

            System.err.println("Ein Forschungslabor ist hier schon vorhanden");
            return;
        }

        if (this.handkarten.contains(aktStadt)) {

        } else {
            System.err.println("Sie besitzen keine Handkarte mit der Stadt, in der Sie sich gerade befinden!");
            return;
        }

        // Betriebsexperte: muss bei Forschungslabor errichten keine Karte ablegen
        if (!this.getRolle().equals(Rolle.BETRIEBSEXPERTE)) { // Der Spieler ist KEIN Betriebsexperte

            // Die benutzte Handkarte wird abgelegt:
            this.handkarten.remove(aktStadt);
        }

        // Forschunglabor errichten:
        aktStadt.setHatForschungsLabor(true);

        //Anzahl der Aktionspunkte um 1 reduziert:
        int aktionspunkte = this.getAktionspunkte();
        aktionspunkte = aktionspunkte - 1;
        this.setAktionspunkte(aktionspunkte);
    }

    /**
     * Gegenmittel entdecken (man muss in einer Stadt mit Forschungslabor sein ...
     * Legen Sie 5 Handkarten der selben Farbe ab um die Seuche mit dieser Farbe zu heilen.)
     *
     * @param einErreger
     */
    public void gegenmittelEntdecken(ErregerTyp einErreger) {

        if (this.aktionspunkte < 1) {
            System.err.println("Sie haben keine Aktionspunkte mehr!");
            return;
        }


        // Ueberpruefen, ob in der Stadt ein Forschungslabor vorganden ist:
        if (!(this.getStadt().isHatForschungsLabor())) {

            System.err.println("In Ihrer Stadt ist kein Forschungslabor vorhanden!");
            return;

        } else {

            Stadt actStadt = this.getStadt();

            LinkedList<ISpielkarte> handkarten = this.getHandkarten();

            int notwendigeKartenanzahl = 5;
            int aktuelleKartenzahl = 0;

            // Wissenschaftler: Bei der Aktion Gegenmittel entdecken benötigt er nur 4 statt 5 Karten einer Farbe
            if (this.getRolle().equals(Rolle.WISSENSCHAFTLER)) {
                notwendigeKartenanzahl = 4;
            } else {
                notwendigeKartenanzahl = 5;
            }

            // Karten der Farben des uebergebenen Erregers in Handkarten zaehlen:
            for (int ii = 0; ii < handkarten.size(); ii++) {

                if (handkarten.get(ii) instanceof Stadt) { // Wenn eine Karte eine Stadtkarte ist,

                    ErregerTyp err = ((Stadt) handkarten.get(ii)).getErreger(); // Erregertyp ermitteln

                    if (err.equals(einErreger)) {
                        aktuelleKartenzahl++;
                    }
                }
            }

            // wenn nicht genug Karten der Farbe des uebergebenen Erregers in Handkarten vorhanden ist:
            if (aktuelleKartenzahl < notwendigeKartenanzahl) {

                System.err.println("Sie verfuegen uber nicht genug Karten notwendiger Farbe, um ein Gegenmittel zu bekommen!");
                return;

            } else {

                einErreger.setGegenmittelEntdeckt(true);

                for (int ii = 0; ii < handkarten.size(); ii++) {

                    if (notwendigeKartenanzahl == 0) {
                        break;
                    }

                    if (einErreger.equals(((Stadt) handkarten.get(ii)).getErreger())) {

                        handkarten.remove(handkarten.get(ii));
                        notwendigeKartenanzahl--;
                    }
                }
            }

            //Anzahl der Aktionspunkte um 1 reduziert:
            int aktionspunkte = this.getAktionspunkte();
            aktionspunkte = aktionspunkte - 1;
            this.setAktionspunkte(aktionspunkte);
        }
    }


    /**
     * Seuche behandeln (Reduzieren Sie in der Stadt in der sich Ihre Spielfigur befindet, eine Infektionsskala um einen Punkt.
     * Der Arzt entfernt alle Punkte dieser Skala)
     *
     * @param erreger ist der Erregetyp, desen Infektionsskala reduziert werden soll.
     */
    public void seucheBehandeln(ErregerTyp erreger) {

        if (this.aktionspunkte < 1) {
            System.err.println("Sie haben keine Aktionspunkte mehr!");
            return;
        }

        if (this.getStadt().getSkala(erreger) == 0) {
            System.err.println("Die Krankheit ist nicht vorhanden!");
            return;
        }


        // Arzt: Bei der Aktion Seuche behandeln entfernt er alle Würfel einer Farbe,
        // wurde das Gegenmittel bereits entdeckt, entfernt er alle Würfel,
        // ohne Aktion dafür ausgeben zu müssen (auch wenn er durch Logistikexperte bewegt wird)

        if (this.getRolle().equals(Rolle.ARZT)) {

            this.getStadt().setSkala(erreger, 0);

            if (!erreger.isGegenmittelEntdeckt()) {

                //Anzahl der Aktionspunkte um 1 reduziert:
                int aktionspunkte = this.getAktionspunkte();
                aktionspunkte = aktionspunkte - 1;
                this.setAktionspunkte(aktionspunkte);
            }
        } else {

            int actSckla = this.getStadt().getSkala(erreger);
            actSckla = actSckla - 1;
            this.getStadt().setSkala(erreger, actSckla);

            //Anzahl der Aktionspunkte um 1 reduziert:
            int aktionspunkte = this.getAktionspunkte();
            aktionspunkte = aktionspunkte - 1;
            this.setAktionspunkte(aktionspunkte);
        }

    }

    public boolean isAndererMitspielerDa(ArrayList<Spieler> spielerListe){
        boolean result = false;
        for(Spieler spieler : spielerListe){
            if(!spieler.equals(this) && spieler.getStadt().equals(this.getStadt())){
                result = true;
            }
        }
        return result;
    }

    /**
     * Wissen teilen (Wenn zwei Spieler in der selben Stadt sind, kann die Karte, auf der diese Stadt abgebildet ist,
     * weitergegeben werden...Befinden sich beispielsweise beide Spieler in Moskau, dann kann nur die Moskaukarte
     * weitergegeben werden, die Forscherin kann in einer beliebigen Stadt eine Karte weitergeben)
     *
     * @param mitspieler    der gewuenschte Spielpartner fuer den Kartenaustausch
     * @param karteAbgeben: // true = die Karte uebergeben (vom Spieler zum Mitspieler), // false = die Karte erhalten (vom Mitspieler)
     */
    public void wissenTeilen(Spieler mitspieler, ISpielkarte eineSpielerkarte, boolean karteAbgeben) {

        if (this.aktionspunkte < 1) {
            System.err.println("Sie haben keine Aktionspunkte mehr!");
            return;
        }

        LinkedList<ISpielkarte> handkartenSpieler = this.getHandkarten();
        Stadt spielerStadt = this.getStadt();

        LinkedList<ISpielkarte> handkartenMitspieler = mitspieler.getHandkarten();
        Stadt mitspielerStadt = mitspieler.getStadt();

        if (!spielerStadt.equals(mitspielerStadt)) {

            System.err.println("Der Spieler und der Mitspieler befinden sich nicht in derselben Stadt !");
            return;
        }

        if (karteAbgeben) { // true = die Karte uebergeben (vom Spieler zum Mitspieler)

            // ueberpruefen, ob der Spieler zumindest 1 Karte besitzt:
            if (this.handkarten.size() == 0) {

                System.err.println("Sie koennen keine Karten uebergeben, da Sie gar keine beistzen!");
                return;
            }

            // ueberpruefen, ob der Mitspieler NICHT die maximale Kartenanzahl besitzt:
            if (mitspieler.getHandkarten().size() >= MAX_KARTENZAHL) { // wenn der Mitspieler maximale kartenanzahl besitzt

                System.err.println("Der Mitspieler, der die karte bekommen soll, besitzt die maximale Kartenanzahl!");
                return;
            }

            if (!(this.handkarten.contains(eineSpielerkarte))) { // Wenn der Spieler die gewaehlte Karte nicht besitzt

                System.err.println("Die Karte, die Sie uebergeben moechten, ist bei Ihnen nicht vorhanden!");
                return;
            }

            if (this.getRolle() == Rolle.FORSCHER) { // Wenn der Spieler Forscher ist

                this.handkarten.remove(eineSpielerkarte);
                mitspieler.getHandkarten().add(eineSpielerkarte);

            } else { // Wenn der Spieler KEIN Forscher ist

                if (eineSpielerkarte instanceof Stadt) { // Wenn die Karte ene Stadtkarte ist

                    this.handkarten.remove(eineSpielerkarte);
                    mitspieler.getHandkarten().add(eineSpielerkarte);

                } else { // Wenn die Karte KEINE Stadtkarte ist

                    System.err.println("Die an Sie uebergeben Karte ist keine Stadtkarte!");
                    return;
                }
            }

            //Anzahl der Aktionspunkte um 1 reduziert:
            int aktionspunkte = this.getAktionspunkte();
            aktionspunkte = aktionspunkte - 1;
            this.setAktionspunkte(aktionspunkte);

        } else { // false = die Karte erhalten (vom Mitspieler)

            // ueberpruefen, ob der Spieler NICHT die maximale Kartenanzahl besitzt:
            if (this.getHandkarten().size() >= MAX_KARTENZAHL) {

                System.err.println("Sie besitzen die maximale Kartenanzahl und koennen keine karten mehr annehmen!");
                return;
            }

            if (mitspieler.getRolle().equals(Rolle.FORSCHER)) { // Wenn der Mitspieler Forscher ist:

                mitspieler.handkarten.remove(eineSpielerkarte);
                this.getHandkarten().add(eineSpielerkarte);

            } else { // Wenn der Mitspieler KEIN Forscher ist:

                // ueberpruefen, ob die uebergebene Karte eine Stadtkarte ist:
                if (eineSpielerkarte instanceof Stadt) {

                    mitspieler.handkarten.remove(eineSpielerkarte);
                    this.getHandkarten().add(eineSpielerkarte);

                } else { // wenn die uebergebene Karte KEINE Stadtkarte ist:

                    System.err.println("Die an Sie uebergebene Karte ist keine Stadtkarte!");
                    return;
                }
            }

            //Anzahl der Aktionspunkte um 1 reduziert:
            int aktionspunkte = mitspieler.getAktionspunkte();
            aktionspunkte = aktionspunkte - 1;
            mitspieler.setAktionspunkte(aktionspunkte);
        }
    }

    // Zug beenden und Aktionspunkte verwerfen:

    public void zugBeenden() {

        this.setAktionspunkte(4);
    }

    public void nehmeHandkarte(ISpielkarte karte) {
        this.handkarten.add(karte);
    }

    public boolean hatMindKartenVon(ErregerTyp erreger){
        int aktuelleKartenzahl = 0;
        int notwendigeKartenzahl = (this.getRolle().equals(Rolle.WISSENSCHAFTLER) ? 4 : 5);

        // Karten der Farben des uebergebenen Erregers in Handkarten zaehlen:
        for (int ii = 0; ii < handkarten.size(); ii++) {
            if (handkarten.get(ii) instanceof Stadt) { // Wenn eine Karte eine Stadtkarte ist,
                ErregerTyp err = ((Stadt) handkarten.get(ii)).getErreger(); // Erregertyp ermitteln
                if (err.equals(erreger)) {
                    aktuelleKartenzahl++;
                }
            }
        }
        return notwendigeKartenzahl < aktuelleKartenzahl;
    }

    public Spieler selberOderAnderer(SpielplanController controller) {
        if(rolle.equals(Rolle.LOGISTIKER)){
            Spieler bewegteFigur = controller.getLogistikerAuswahl();
            bewegteFigur.setAktionspunkte(1);
            return bewegteFigur;
        }
        else return this;
    }
}
