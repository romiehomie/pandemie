package pandemie.game.spielablauf;

import pandemie.game.spielkarten.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.Random;

public class Spielkartenstapel {
    LinkedList<ISpielkarte> spielKartenStapel;

    /**
     * Fügt alle Karten dem Stapel hinzu und mischt
     */
    public Spielkartenstapel(Spiel spiel){
        spielKartenStapel = new LinkedList<>();

        // füge alle Städte hinzu
        for(Stadt stadtKarte : Stadt.values()){
            spielKartenStapel.add(stadtKarte);
        }
        // füge alle Ereigniskarten hinzu
        for(Ereigniskarte ereignisKarte : Ereigniskarte.values()){
            spielKartenStapel.add(ereignisKarte);
        }
        // mische diese Karten
        Collections.shuffle(spielKartenStapel);

        // verteile Anfangskarten
        verteileAnfangskartenWlado(spiel);

        mischeEpedemiekartenEin(spiel);

    }

    private void mischeEpedemiekartenEin(Spiel spiel) {
        int schwierigkeitsGrad = spiel.getSchwierigkeitsGrad();
    }

    public void zieheKarte(Spieler spieler, Spiel spiel){
        if(spieler.getHandkarten().size()>7){
            spiel.mussHandkarteAblegen(spieler);
        }

        ISpielkarte karte = spielKartenStapel.pollLast();

        if(karte.getName().equals("Epedemie!")){
            Ereignis.epidemieEreignis(spiel);
        } else{
            spieler.nehmeHandkarte(karte);
        }
    }

    public void verteileAnfangskartenRomsen(Spiel spiel){
        int zahlAnfangskarten = 6 - spiel.getSpielerListe().size();
        for(Spieler spieler: spiel.getSpielerListe()){
            for(int i=0; i< zahlAnfangskarten; i++){
                zieheKarte(spieler, spiel);
            }
        }
    }
    
    public void verteileAnfangskartenWlado(Spiel spiel) {
        ArrayList<Spieler> spielerliste = spiel.getSpielerListe();

        switch (spielerliste.size()) {

            case 2: // 2 Spieler

                // Spieler 1 bekommt 4 Handkarten:

                LinkedList<ISpielkarte> handkarten21 = new LinkedList<ISpielkarte>();
                handkarten21.add(spielKartenStapel.getLast()); // Karte 1
                spielKartenStapel.removeLast();

                handkarten21.add(spielKartenStapel.getLast()); // Karte 2
                spielKartenStapel.removeLast();

                handkarten21.add(spielKartenStapel.getLast()); // Karte 3
                spielKartenStapel.removeLast();

                handkarten21.add(spielKartenStapel.getLast()); // Karte 4
                spielKartenStapel.removeLast();

                System.out.println(spielKartenStapel);

                (spielerliste.get(0)).setHandkarten(handkarten21); // ISpielkarte


                // Spieler 2 bekommt 4 Handkarten:
                LinkedList<ISpielkarte> handkarten22 = new LinkedList<ISpielkarte>();
                handkarten22.add(spielKartenStapel.getLast()); // Karte 1
                spielKartenStapel.removeLast();

                handkarten22.add(spielKartenStapel.getLast()); // Karte 2
                spielKartenStapel.removeLast();

                handkarten22.add(spielKartenStapel.getLast()); // Karte 3
                spielKartenStapel.removeLast();

                handkarten22.add(spielKartenStapel.getLast()); // Karte 4
                spielKartenStapel.removeLast();

                (spielerliste.get(1)).setHandkarten(handkarten22); // ISpielkarte

                break;

            case 3: // 3 Spieler

                // Spieler 1 bekommt 3 Handkarten:
                LinkedList<ISpielkarte> handkarten31 = new LinkedList<ISpielkarte>();
                handkarten31.add(spielKartenStapel.getLast()); // Karte 1
                spielKartenStapel.removeLast();

                handkarten31.add(spielKartenStapel.getLast()); // Karte 2
                spielKartenStapel.removeLast();

                handkarten31.add(spielKartenStapel.getLast()); // Karte 3
                spielKartenStapel.removeLast();

                (spielerliste.get(0)).setHandkarten(handkarten31); // ISpielkarte

                // Spieler 2 bekommt 3 Handkarten:
                LinkedList<ISpielkarte> handkarten32 = new LinkedList<ISpielkarte>();
                handkarten32.add(spielKartenStapel.getLast()); // Karte 1
                spielKartenStapel.removeLast();

                handkarten32.add(spielKartenStapel.getLast()); // Karte 2
                spielKartenStapel.removeLast();

                handkarten32.add(spielKartenStapel.getLast()); // Karte 3
                spielKartenStapel.removeLast();

                (spielerliste.get(1)).setHandkarten(handkarten32); // ISpielkarte


                // Spieler 3 bekommt 3 Handkarten:
                LinkedList<ISpielkarte> handkarten33 = new LinkedList<ISpielkarte>();
                handkarten33.add(spielKartenStapel.getLast()); // Karte 1
                spielKartenStapel.removeLast();

                handkarten33.add(spielKartenStapel.getLast()); // Karte 2
                spielKartenStapel.removeLast();

                handkarten33.add(spielKartenStapel.getLast()); // Karte 3
                spielKartenStapel.removeLast();

                (spielerliste.get(2)).setHandkarten(handkarten33); // ISpielkarte

                break;

            case 4: // 4 Spieler

                // Spieler 1 bekommt 2 Handkarten:
                LinkedList<ISpielkarte> handkarten41 = new LinkedList<ISpielkarte>();
                handkarten41.add(spielKartenStapel.getLast()); // Karte 1
                spielKartenStapel.removeLast();

                handkarten41.add(spielKartenStapel.getLast()); // Karte 2
                spielKartenStapel.removeLast();

                (spielerliste.get(0)).setHandkarten(handkarten41); // ISpielkarte


                // Spieler 2 bekommt 2 Handkarten:
                LinkedList<ISpielkarte> handkarten42 = new LinkedList<ISpielkarte>();
                handkarten42.add(spielKartenStapel.getLast()); // Karte 1
                spielKartenStapel.removeLast();

                handkarten42.add(spielKartenStapel.getLast()); // Karte 2
                spielKartenStapel.removeLast();

                (spielerliste.get(1)).setHandkarten(handkarten42); // ISpielkarte


                // Spieler 3 bekommt 2 Handkarten:
                LinkedList<ISpielkarte> handkarten43 = new LinkedList<ISpielkarte>();
                handkarten43.add(spielKartenStapel.getLast()); // Karte 1
                spielKartenStapel.removeLast();

                handkarten43.add(spielKartenStapel.getLast()); // Karte 2
                spielKartenStapel.removeLast();

                (spielerliste.get(2)).setHandkarten(handkarten43); // ISpielkarte


                // Spieler 4 bekommt 2 Handkarten:
                LinkedList<ISpielkarte> handkarten44 = new LinkedList<ISpielkarte>();
                handkarten44.add(spielKartenStapel.getLast()); // Karte 1
                spielKartenStapel.removeLast();

                handkarten44.add(spielKartenStapel.getLast()); // Karte 2
                spielKartenStapel.removeLast();

                (spielerliste.get(3)).setHandkarten(handkarten44); // ISpielkarte
        }
    }

    /**
     * Die Funktion versorgt alle Spieler mit Spielkarten und erzeugt den Spielkartenstapel, abhaengig von der Spieleranzahl und Schwierigkeitsgrad
     * @param schwierigkeitsgrad ist der Schwieriegkeitsgrad des Spiels (1 bis 3)
     * @param spielerliste ist eine Liste mit den erzeugten Spielern (als Objekte, 2 bis 4 Spieler)
     * @return
     */
    public static LinkedList<ISpielkarte> getSpielkartenstapel(int schwierigkeitsgrad, ArrayList<Spieler> spielerliste) {

        int anzahlDerStaedte = 48, anzahlDerEreigniskarten = 5, anzahlDerEpidemiekarten = 0;

        // Spielkartenstapel als Array, nicht gemischt:
        ISpielkarte[] spielkartenstapel = new ISpielkarte[anzahlDerStaedte + anzahlDerEreigniskarten];

        // Spielkartenstapel als Array, gemischt, noch nicht aufgefuellt:
        ISpielkarte[] spielkartenstapelGemischt = new ISpielkarte[spielkartenstapel.length];
        LinkedList<ISpielkarte> spielKartenStapel = new LinkedList<ISpielkarte>();

        // Alle Staedte werden zu dem Spielkartenstapel hinzugefuegt:
        int i = 0;
        for (Stadt stadt : Stadt.values()) {
            spielkartenstapel[i] = stadt;
            i++;
        }

        // Ereigniskarten werden zu dem Spielkartenstapel hinzugefuegt:
        for (Ereigniskarte ereigniskarte : Ereigniskarte.values()) {
            spielkartenstapel[i] = ereigniskarte;
            i++;
        }

        // mit Zufallsgenerator Kartenpositionen von Stadtkarten + Ereigniskarten, ohne Epidemiekarten zufaellig setzen (vermischen):
        int kartenanzahl = spielkartenstapel.length;
        Random rnd = new Random();
        boolean istEnthalten = false;
        int zufallsZahl = 0, jj = 0;


        int[] zahlenarray = new int[kartenanzahl]; // neue, durch Zufallszahlgenerator ermittelte Positionen der Karten im Spielkartenstapel

        for (int ii = 0; ii < kartenanzahl; ii++) {
            zahlenarray[ii] = -1;
            spielkartenstapelGemischt[ii] = null;
        }


        while (jj < kartenanzahl) {

            zufallsZahl = rnd.nextInt(kartenanzahl) + 1; // Zufallszahl zw. 1 und Anzahl der Karten im Spielkartenstapel
            istEnthalten = false;

            for (int ii = 0; ii < kartenanzahl; ii++) {

                if (zufallsZahl == zahlenarray[ii]) {

                    istEnthalten = true;
                }
            }

            if (!istEnthalten) {

                zahlenarray[jj] = zufallsZahl;
                jj++;
            }
        }

        // an die mit Zufallszahlgenerator ermittelte Positionen Karten setzen:
        for (int ii = 0; ii < kartenanzahl; ii++) {
            spielkartenstapelGemischt[ii] = spielkartenstapel[(zahlenarray[ii] - 1)];
            spielKartenStapel.add(spielkartenstapel[(zahlenarray[ii] - 1)]);
        }


        int anzahlDerSpieler = spielerliste.size(); // Anzahl der Spieler ermitteln

        // aus dem gemischten Stapel eine Karte an den aktuellen Spieler uebergeben:
        LinkedList<ISpielkarte> handkarten = new LinkedList<ISpielkarte>();

        switch (anzahlDerSpieler) {

            case 2: // 2 Spieler

                // Spieler 1 bekommt 4 Handkarten:

                handkarten.add(spielKartenStapel.getLast()); // Karte 1
                spielKartenStapel.removeLast();

                handkarten.add(spielKartenStapel.getLast()); // Karte 2
                spielKartenStapel.removeLast();

                handkarten.add(spielKartenStapel.getLast()); // Karte 3
                spielKartenStapel.removeLast();

                handkarten.add(spielKartenStapel.getLast()); // Karte 4
                spielKartenStapel.removeLast();

                (spielerliste.get(0)).setHandkarten(handkarten); // ISpielkarte


                // Spieler 2 bekommt 4 Handkarten:
                handkarten.add(spielKartenStapel.getLast()); // Karte 1
                spielKartenStapel.removeLast();

                handkarten.add(spielKartenStapel.getLast()); // Karte 2
                spielKartenStapel.removeLast();

                handkarten.add(spielKartenStapel.getLast()); // Karte 3
                spielKartenStapel.removeLast();

                handkarten.add(spielKartenStapel.getLast()); // Karte 4
                spielKartenStapel.removeLast();

                (spielerliste.get(1)).setHandkarten(handkarten); // ISpielkarte

                break;

            case 3: // 3 Spieler

                // Spieler 1 bekommt 3 Handkarten:
                handkarten.add(spielKartenStapel.getLast()); // Karte 1
                spielKartenStapel.removeLast();

                handkarten.add(spielKartenStapel.getLast()); // Karte 2
                spielKartenStapel.removeLast();

                handkarten.add(spielKartenStapel.getLast()); // Karte 3
                spielKartenStapel.removeLast();

                (spielerliste.get(0)).setHandkarten(handkarten); // ISpielkarte

                // Spieler 2 bekommt 3 Handkarten:
                handkarten.add(spielKartenStapel.getLast()); // Karte 1
                spielKartenStapel.removeLast();

                handkarten.add(spielKartenStapel.getLast()); // Karte 2
                spielKartenStapel.removeLast();

                handkarten.add(spielKartenStapel.getLast()); // Karte 3
                spielKartenStapel.removeLast();

                (spielerliste.get(1)).setHandkarten(handkarten); // ISpielkarte


                // Spieler 3 bekommt 3 Handkarten:
                handkarten.add(spielKartenStapel.getLast()); // Karte 1
                spielKartenStapel.removeLast();

                handkarten.add(spielKartenStapel.getLast()); // Karte 2
                spielKartenStapel.removeLast();

                handkarten.add(spielKartenStapel.getLast()); // Karte 3
                spielKartenStapel.removeLast();

                (spielerliste.get(2)).setHandkarten(handkarten); // ISpielkarte


                break;

            case 4: // 4 Spieler

                // Spieler 1 bekommt 2 Handkarten:
                handkarten.add(spielKartenStapel.getLast()); // Karte 1
                spielKartenStapel.removeLast();

                handkarten.add(spielKartenStapel.getLast()); // Karte 2
                spielKartenStapel.removeLast();

                (spielerliste.get(0)).setHandkarten(handkarten); // ISpielkarte


                // Spieler 2 bekommt 2 Handkarten:
                handkarten.add(spielKartenStapel.getLast()); // Karte 1
                spielKartenStapel.removeLast();

                handkarten.add(spielKartenStapel.getLast()); // Karte 2
                spielKartenStapel.removeLast();

                (spielerliste.get(1)).setHandkarten(handkarten); // ISpielkarte


                // Spieler 3 bekommt 2 Handkarten:
                handkarten.add(spielKartenStapel.getLast()); // Karte 1
                spielKartenStapel.removeLast();

                handkarten.add(spielKartenStapel.getLast()); // Karte 2
                spielKartenStapel.removeLast();

                (spielerliste.get(2)).setHandkarten(handkarten); // ISpielkarte


                // Spieler 4 bekommt 2 Handkarten:
                handkarten.add(spielKartenStapel.getLast()); // Karte 1
                spielKartenStapel.removeLast();

                handkarten.add(spielKartenStapel.getLast()); // Karte 2
                spielKartenStapel.removeLast();

                (spielerliste.get(3)).setHandkarten(handkarten); // ISpielkarte
                break;

            default:
                break;
        }


        kartenanzahl = spielKartenStapel.size();

        // 6 Spielkartenstapel anlegen:
        LinkedList<ISpielkarte> spielkartenstapel1 = new LinkedList<ISpielkarte>();
        LinkedList<ISpielkarte> spielkartenstapel2 = new LinkedList<ISpielkarte>();
        LinkedList<ISpielkarte> spielkartenstapel3 = new LinkedList<ISpielkarte>();
        LinkedList<ISpielkarte> spielkartenstapel4 = new LinkedList<ISpielkarte>();
        LinkedList<ISpielkarte> spielkartenstapel5 = new LinkedList<ISpielkarte>();
        LinkedList<ISpielkarte> spielkartenstapel6 = new LinkedList<ISpielkarte>();

        // Kontainer für alle 4 bis 6 Stapel anlegen:
        LinkedList<ISpielkarte> alleSpielkartenStapel = new LinkedList<ISpielkarte>();

        switch (schwierigkeitsgrad) {

            case 1:  // 4 Epidemiekarten, 4 Stapel -> Anfängerspiel

                spielkartenstapel1.add(Epidemiekarte.erzeugeInstanz());
                spielkartenstapel2.add(Epidemiekarte.erzeugeInstanz());
                spielkartenstapel3.add(Epidemiekarte.erzeugeInstanz());
                spielkartenstapel4.add(Epidemiekarte.erzeugeInstanz());

                switch (anzahlDerSpieler) {

                    case 3: // 3 Spieler

                        // 4x11 Stapel:
                        for (int ii = 1; ii < 12; ii++) {

                            spielkartenstapel1.add(spielKartenStapel.getLast());
                            spielKartenStapel.removeLast();
                            Collections.shuffle(spielkartenstapel1);
                        }
                        for (int ii = 1; ii < 12; ii++) {

                            spielkartenstapel2.add(spielKartenStapel.getLast());
                            spielKartenStapel.removeLast();
                            Collections.shuffle(spielkartenstapel2);
                        }
                        for (int ii = 1; ii < 12; ii++) {

                            spielkartenstapel3.add(spielKartenStapel.getLast());
                            spielKartenStapel.removeLast();
                            Collections.shuffle(spielkartenstapel3);
                        }
                        for (int ii = 1; ii < 12; ii++) {

                            spielkartenstapel4.add(spielKartenStapel.getLast());
                            spielKartenStapel.removeLast();
                            Collections.shuffle(spielkartenstapel4);
                        }

                        break;

                    default:  // 2 & 4 Spieler

                        // 3x11 & 1x12 Stapel:
                        for (int ii = 1; ii < 12; ii++) {

                            spielkartenstapel1.add(spielKartenStapel.getLast());
                            spielKartenStapel.removeLast();
                            Collections.shuffle(spielkartenstapel1);
                        }
                        for (int ii = 1; ii < 12; ii++) {

                            spielkartenstapel2.add(spielKartenStapel.getLast());
                            spielKartenStapel.removeLast();
                            Collections.shuffle(spielkartenstapel2);
                        }
                        for (int ii = 1; ii < 12; ii++) {

                            spielkartenstapel3.add(spielKartenStapel.getLast());
                            spielKartenStapel.removeLast();
                            Collections.shuffle(spielkartenstapel3);
                        }
                        for (int ii = 1; ii < 13; ii++) {

                            spielkartenstapel4.add(spielKartenStapel.getLast());
                            spielKartenStapel.removeLast();
                            Collections.shuffle(spielkartenstapel4);
                        }

                        break;
                }

                for (int ii = 0; ii < spielkartenstapel1.size(); ii ++){

                    alleSpielkartenStapel.add(spielkartenstapel1.getLast());
                    spielkartenstapel1.removeLast();
                }

                for (int ii = 0; ii < spielkartenstapel2.size(); ii ++){

                    alleSpielkartenStapel.add(spielkartenstapel2.getLast());
                    spielkartenstapel2.removeLast();
                }

                for (int ii = 0; ii < spielkartenstapel3.size(); ii ++){

                    alleSpielkartenStapel.add(spielkartenstapel3.getLast());
                    spielkartenstapel3.removeLast();
                }

                for (int ii = 0; ii < spielkartenstapel4.size(); ii ++){

                    alleSpielkartenStapel.add(spielkartenstapel4.getLast());
                    spielkartenstapel4.removeLast();
                }

                Collections.shuffle(alleSpielkartenStapel);

                break;

            case 2: // 5 Epidemiekarten, 5 Stapel -> Normales Spiel

                spielkartenstapel1.add(Epidemiekarte.erzeugeInstanz());
                spielkartenstapel2.add(Epidemiekarte.erzeugeInstanz());
                spielkartenstapel3.add(Epidemiekarte.erzeugeInstanz());
                spielkartenstapel4.add(Epidemiekarte.erzeugeInstanz());
                spielkartenstapel5.add(Epidemiekarte.erzeugeInstanz());

                switch (anzahlDerSpieler) {

                    case 3: // 3 Spieler

                        // 4x9 & 1x8 Stapel:
                        for (int ii = 1; ii < 10; ii++) {

                            spielkartenstapel1.add(spielKartenStapel.getLast());
                            spielKartenStapel.removeLast();
                            Collections.shuffle(spielkartenstapel1);
                        }
                        for (int ii = 1; ii < 10; ii++) {

                            spielkartenstapel2.add(spielKartenStapel.getLast());
                            spielKartenStapel.removeLast();
                            Collections.shuffle(spielkartenstapel2);
                        }
                        for (int ii = 1; ii < 10; ii++) {

                            spielkartenstapel3.add(spielKartenStapel.getLast());
                            spielKartenStapel.removeLast();
                            Collections.shuffle(spielkartenstapel3);
                        }
                        for (int ii = 1; ii < 10; ii++) {

                            spielkartenstapel4.add(spielKartenStapel.getLast());
                            spielKartenStapel.removeLast();
                            Collections.shuffle(spielkartenstapel4);
                        }
                        for (int ii = 1; ii < 9; ii++) {

                            spielkartenstapel5.add(spielKartenStapel.getLast());
                            spielKartenStapel.removeLast();
                            Collections.shuffle(spielkartenstapel5);
                        }

                        break;

                    default: // 2 & 4 Spieler

                        // 5x9 Stapel:
                        for (int ii = 1; ii < 10; ii++) {

                            spielkartenstapel1.add(spielKartenStapel.getLast());
                            spielKartenStapel.removeLast();
                            Collections.shuffle(spielkartenstapel1);
                        }
                        for (int ii = 1; ii < 10; ii++) {

                            spielkartenstapel2.add(spielKartenStapel.getLast());
                            spielKartenStapel.removeLast();
                            Collections.shuffle(spielkartenstapel2);
                        }
                        for (int ii = 1; ii < 10; ii++) {

                            spielkartenstapel3.add(spielKartenStapel.getLast());
                            spielKartenStapel.removeLast();
                            Collections.shuffle(spielkartenstapel3);
                        }
                        for (int ii = 1; ii < 10; ii++) {

                            spielkartenstapel4.add(spielKartenStapel.getLast());
                            spielKartenStapel.removeLast();
                            Collections.shuffle(spielkartenstapel4);
                        }
                        for (int ii = 1; ii < 10; ii++) {

                            spielkartenstapel5.add(spielKartenStapel.getLast());
                            spielKartenStapel.removeLast();
                            Collections.shuffle(spielkartenstapel5);
                        }
                        break;
                }



                for (int ii = 0; ii < spielkartenstapel1.size(); ii ++){

                    alleSpielkartenStapel.add(spielkartenstapel1.getLast());
                    spielkartenstapel1.removeLast();
                }
                for (int ii = 0; ii < spielkartenstapel2.size(); ii ++){

                    alleSpielkartenStapel.add(spielkartenstapel2.getLast());
                    spielkartenstapel2.removeLast();
                }
                for (int ii = 0; ii < spielkartenstapel3.size(); ii ++){

                    alleSpielkartenStapel.add(spielkartenstapel3.getLast());
                    spielkartenstapel3.removeLast();
                }
                for (int ii = 0; ii < spielkartenstapel4.size(); ii ++){

                    alleSpielkartenStapel.add(spielkartenstapel4.getLast());
                    spielkartenstapel4.removeLast();
                }
                for (int ii = 0; ii < spielkartenstapel5.size(); ii ++){

                    alleSpielkartenStapel.add(spielkartenstapel5.getLast());
                    spielkartenstapel5.removeLast();
                }

                Collections.shuffle(alleSpielkartenStapel);

                break;

            case 3: // 6 Epidemiekarten, 6 Stapel -> Heldenspiel

                spielkartenstapel1.add(Epidemiekarte.erzeugeInstanz());
                spielkartenstapel2.add(Epidemiekarte.erzeugeInstanz());
                spielkartenstapel3.add(Epidemiekarte.erzeugeInstanz());
                spielkartenstapel4.add(Epidemiekarte.erzeugeInstanz());
                spielkartenstapel5.add(Epidemiekarte.erzeugeInstanz());
                spielkartenstapel6.add(Epidemiekarte.erzeugeInstanz());

                switch (anzahlDerSpieler) {

                    case 3: // 3 Spieler

                        // 4x7 & 2x8 Stapel
                        for (int ii = 1; ii < 8; ii++) {

                            spielkartenstapel1.add(spielKartenStapel.getLast());
                            spielKartenStapel.removeLast();
                            Collections.shuffle(spielkartenstapel1);
                        }
                        for (int ii = 1; ii < 8; ii++) {

                            spielkartenstapel2.add(spielKartenStapel.getLast());
                            spielKartenStapel.removeLast();
                            Collections.shuffle(spielkartenstapel2);
                        }
                        for (int ii = 1; ii < 8; ii++) {

                            spielkartenstapel3.add(spielKartenStapel.getLast());
                            spielKartenStapel.removeLast();
                            Collections.shuffle(spielkartenstapel3);
                        }
                        for (int ii = 1; ii < 8; ii++) {

                            spielkartenstapel4.add(spielKartenStapel.getLast());
                            spielKartenStapel.removeLast();
                            Collections.shuffle(spielkartenstapel4);
                        }
                        for (int ii = 1; ii < 9; ii++) {

                            spielkartenstapel5.add(spielKartenStapel.getLast());
                            spielKartenStapel.removeLast();
                            Collections.shuffle(spielkartenstapel5);
                        }
                        for (int ii = 1; ii < 9; ii++) {

                            spielkartenstapel6.add(spielKartenStapel.getLast());
                            spielKartenStapel.removeLast();
                            Collections.shuffle(spielkartenstapel6);
                        }

                        break;

                    default: // 2 & 4 Spieler

                        // 3x7 & 3x8 Stapel:
                        for (int ii = 1; ii < 8; ii++) {

                            spielkartenstapel1.add(spielKartenStapel.getLast());
                            spielKartenStapel.removeLast();
                            Collections.shuffle(spielkartenstapel1);
                        }
                        for (int ii = 1; ii < 8; ii++) {

                            spielkartenstapel2.add(spielKartenStapel.getLast());
                            spielKartenStapel.removeLast();
                            Collections.shuffle(spielkartenstapel2);
                        }
                        for (int ii = 1; ii < 8; ii++) {

                            spielkartenstapel3.add(spielKartenStapel.getLast());
                            spielKartenStapel.removeLast();
                            Collections.shuffle(spielkartenstapel3);
                        }
                        for (int ii = 1; ii < 9; ii++) {

                            spielkartenstapel4.add(spielKartenStapel.getLast());
                            spielKartenStapel.removeLast();
                            Collections.shuffle(spielkartenstapel4);
                        }
                        for (int ii = 1; ii < 9; ii++) {

                            spielkartenstapel5.add(spielKartenStapel.getLast());
                            spielKartenStapel.removeLast();
                            Collections.shuffle(spielkartenstapel5);
                        }
                        for (int ii = 1; ii < 9; ii++) {

                            spielkartenstapel6.add(spielKartenStapel.getLast());
                            spielKartenStapel.removeLast();
                            Collections.shuffle(spielkartenstapel6);
                        }
                        break;
                }

                for (int ii = 0; ii < spielkartenstapel1.size(); ii ++){

                    alleSpielkartenStapel.add(spielkartenstapel1.getLast());
                    spielkartenstapel1.removeLast();
                }
                for (int ii = 0; ii < spielkartenstapel2.size(); ii ++){

                    alleSpielkartenStapel.add(spielkartenstapel2.getLast());
                    spielkartenstapel2.removeLast();
                }
                for (int ii = 0; ii < spielkartenstapel3.size(); ii ++){

                    alleSpielkartenStapel.add(spielkartenstapel3.getLast());
                    spielkartenstapel3.removeLast();
                }
                for (int ii = 0; ii < spielkartenstapel4.size(); ii ++){

                    alleSpielkartenStapel.add(spielkartenstapel4.getLast());
                    spielkartenstapel4.removeLast();
                }
                for (int ii = 0; ii < spielkartenstapel5.size(); ii ++){

                    alleSpielkartenStapel.add(spielkartenstapel5.getLast());
                    spielkartenstapel5.removeLast();
                }
                for (int ii = 0; ii < spielkartenstapel6.size(); ii ++){

                    alleSpielkartenStapel.add(spielkartenstapel6.getLast());
                    spielkartenstapel6.removeLast();
                }
                Collections.shuffle(alleSpielkartenStapel);

                break;
        }


        return alleSpielkartenStapel; // alle Spielkarten im Spielkartenstapel, zufaellig gemischt
    }


    /**
     * Die Funktion erzeugt den Infektionskartenstapel (Staedte mit zugehoerigen Seuchen)
     * @return ist der erzeugte Infektionsstapel
     */
    public static LinkedList<Stadt> getinfektionskartenStapel(){

        LinkedList<Stadt> infektionskartenStapel = new LinkedList<Stadt>();

        for (Stadt stadt : Stadt.values()) {

            infektionskartenStapel.add(stadt);
        }

        Collections.shuffle(infektionskartenStapel);

        return infektionskartenStapel;
    }


    /**
     * Die Funktion simulert ablage Einer Karte in einen Ablagestapel
     * @param eineKarte ist eine Karte als Objekt, die in den Ablagestepel abgelegt werden soll
     * @param ablagestapel ist der Ablagestapel, in den die Karte gelegt werden soll
     * @return
     */
    public static LinkedList<ISpielkarte> karteAblegen(ISpielkarte eineKarte,  LinkedList<ISpielkarte> ablagestapel){

        ablagestapel.add(eineKarte);

        return ablagestapel;
    }
}
