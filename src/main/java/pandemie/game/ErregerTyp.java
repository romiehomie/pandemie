package pandemie.game;

import javafx.scene.paint.Color;

/**
 * In diesem Enum sind alle Erregetypen festgelegt
 */
public enum ErregerTyp {

    ROT("Viren", Color.rgb(150, 0, 0), 0),
    BLAU("Parasiten", Color.rgb(0, 0, 150), 0),
    GELB("Bakterien", Color.rgb(150, 125, 0), 0),
    SCHWARZ("Pilze", Color.rgb(30, 30, 30), 0);

    private String name;  // der Name eines Erregetyps
    private Color farbe; // die Farbe eines Erregetyps
    private int durchseuchungscounter; // Durchseuchungscounter (Wuerfelanzahl) eines Erregetyps: Wenn keine Wuerfel fuer einen Erregertuep mehr gibt, hat man das Spiel verloren
    private boolean gegenmittelEntdeckt; // Ob fuer einen bestimmten Erregetyp ein gegenmittel entdeckt wurde

    /**
     * Konstruktor für die Klasse "Erregertyp"
     *
     * @param name                  gewuenschter Name eines Erregetyps
     * @param farbe                 gewuenschte Farbe eines Erregetyps
     * @param durchseuchungscounter gewuenschter Durchseuchungscounter (Wuerfelanzahl) eines Erregetyps
     */
    private ErregerTyp(String name, Color farbe, int durchseuchungscounter) {

        this.name = name;
        this.farbe = farbe;
        this.durchseuchungscounter = durchseuchungscounter;
        this.gegenmittelEntdeckt = false;
    }

    /**
     * Gettermethode fuer die Instanzvariable "Name"
     * @return Rueckgabewert fuer die Instanzvariable "Name" (der Name eines Erregers im Spiel)
     */
    public String getName() {
        return name;
    }

    /**
     * Settermethode fuer die Instanzvariable "Name"
     * @param name gewuenschter Wert fuer die Instanzvariable "Name" (der Name eines Erregers im Spiel)
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gettermethode fuer die Instanzvariable "farbe"
     * @return Rueckgabewert fuer die Instanzvariable "farbe" (die Farbe eines Erregers im Spiel)
     */
    public Color getFarbe() {
        return farbe;
    }

    /**
     * Gettermethode fuer die Instanzvariable "durchseuchungscounter"
     * @return Rueckgabewert fuer die Instanzvariable "durchseuchungscounter" (Die Wuerfelanzahl eines Erregers im Spiel)
     */
    public int getDurchseuchungscounter() {
        return durchseuchungscounter;
    }

    /**
     * Settermthode fuer die Instanzvariable "durchseuchungscounter"
     * @param durchseuchungscounter gewuenschter Wert fuer die Instanzvariable "durchseuchungscounter" (Die Wuerfelanzahl eines Erregers im Spiel)
     */
    public void setDurchseuchungscounter(int durchseuchungscounter) {
        this.durchseuchungscounter = durchseuchungscounter;
    }

    /**
     * Gettermethode fuer die Instanzvariable "gegenmittelEntdeckt"
     * @return Rueckgabewert fuer die Instanzvariable "gegenmittelEntdeckt" (Ob fuer einem Erreger ein Gegenmittel entdeckt wurde im Spiel)
     */
    public boolean isGegenmittelEntdeckt() {
        return gegenmittelEntdeckt;
    }

    /**
     * Settermrthode fuer die Variable "gegenmittelEntdeckt"
     *
     * @param gegenmittelEntdeckt gewuenschter Wert fuer die Instanzvariable "gegenmittelEntdeckt" (Ob fuer einem Erreger ein Gegenmittel entdeckt wurde im Spiel)
     */
    public void setGegenmittelEntdeckt(boolean gegenmittelEntdeckt) {
        this.gegenmittelEntdeckt = gegenmittelEntdeckt;
    }
}
