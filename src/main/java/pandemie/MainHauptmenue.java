package pandemie;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class MainHauptmenue extends Application {
    private static Scene scene;
    private static Stage stage;

    @Override
    public void start(Stage hauptmenueStage) throws IOException {
        scene = new Scene(loadFXML("Hauptmenue"));
        hauptmenueStage.setScene(scene);
        hauptmenueStage.setResizable(false);
        stage = hauptmenueStage;
        hauptmenueStage.show();

    }

    public static Stage returnStage() {
        return stage;
    }

    public static void setRoot(String fxml) throws IOException {
        scene.setRoot(loadFXML(fxml));
        scene.getWindow().sizeToScene();
    }

    private static Parent loadFXML(String fxml) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(MainHauptmenue.class.getResource( "view/" + fxml + ".fxml"));
        return fxmlLoader.load();
    }

    public static void main(String[] args) {
        launch();
    }

}
