# Pandemie

The boardgame "Pamdemic" realized with JavaFX. Just an unfinished version of the game where almost all game mechanics is copied. This software is not for comercial use. Just a project for me to teach myself how to code java and javaFX GUI library.

## Dependencies

* Java 12
* JavaFX

## Build and Run

* Linux: open a terminal, navigate into your dirctory "pandemie" and type "./gradlew build" or "./gradlew run"
* Windows: open the command prompt, navigate into your directory "pandemie" and type "gradlew.bat build" or "gradlew.bat run"
